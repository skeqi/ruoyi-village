package com.ruoyi.toushuxinxi.mapper;

import java.util.List;

import com.ruoyi.domain.StaffComplaint;
import com.ruoyi.domain.StaffUser;

/**
 * 投诉管理Mapper接口
 * 
 * @author 郑东来
 * @date 2023-03-24
 */
public interface StaffComplaintMapper 
{
    /**
     * 查询投诉管理
     * 
     * @param complaintId 投诉管理主键
     * @return 投诉管理
     */
    public StaffComplaint selectStaffComplaintByComplaintId(Long complaintId);

    /**
     * 查询投诉管理列表
     * 
     * @param staffComplaint 投诉管理
     * @return 投诉管理集合
     */
    public List<StaffComplaint> selectStaffComplaintList(StaffComplaint staffComplaint);

    /**
     * 新增投诉管理
     * 
     * @param staffComplaint 投诉管理
     * @return 结果
     */
    public int insertStaffComplaint(StaffComplaint staffComplaint);

    /**
     * 修改投诉管理
     * 
     * @param staffComplaint 投诉管理
     * @return 结果
     */
    public int updateStaffComplaint(StaffComplaint staffComplaint);

    /**
     * 删除投诉管理
     * 
     * @param complaintId 投诉管理主键
     * @return 结果
     */
    public int deleteStaffComplaintByComplaintId(Long complaintId);

    /**
     * 批量删除投诉管理
     * 
     * @param complaintIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStaffComplaintByComplaintIds(Long[] complaintIds);

    public List<StaffUser> owner();
}
