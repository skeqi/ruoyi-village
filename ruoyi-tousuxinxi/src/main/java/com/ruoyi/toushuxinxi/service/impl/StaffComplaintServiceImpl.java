package com.ruoyi.toushuxinxi.service.impl;

import java.util.List;

import com.ruoyi.domain.StaffUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.toushuxinxi.mapper.StaffComplaintMapper;
import com.ruoyi.domain.StaffComplaint;
import com.ruoyi.toushuxinxi.service.IStaffComplaintService;

/**
 * 投诉管理Service业务层处理
 * 
 * @author 郑东来
 * @date 2023-03-24
 */
@Service
public class StaffComplaintServiceImpl implements IStaffComplaintService 
{
    @Autowired
    private StaffComplaintMapper staffComplaintMapper;

    /**
     * 查询投诉管理
     * 
     * @param complaintId 投诉管理主键
     * @return 投诉管理
     */
    @Override
    public StaffComplaint selectStaffComplaintByComplaintId(Long complaintId)
    {
        return staffComplaintMapper.selectStaffComplaintByComplaintId(complaintId);
    }

    /**
     * 查询投诉管理列表
     * 
     * @param staffComplaint 投诉管理
     * @return 投诉管理
     */
    @Override
    public List<StaffComplaint> selectStaffComplaintList(StaffComplaint staffComplaint)
    {
        return staffComplaintMapper.selectStaffComplaintList(staffComplaint);
    }

    /**
     * 新增投诉管理
     * 
     * @param staffComplaint 投诉管理
     * @return 结果
     */
    @Override
    public int insertStaffComplaint(StaffComplaint staffComplaint)
    {
        return staffComplaintMapper.insertStaffComplaint(staffComplaint);
    }

    /**
     * 修改投诉管理
     * 
     * @param staffComplaint 投诉管理
     * @return 结果
     */
    @Override
    public int updateStaffComplaint(StaffComplaint staffComplaint)
    {
        return staffComplaintMapper.updateStaffComplaint(staffComplaint);
    }

    /**
     * 批量删除投诉管理
     * 
     * @param complaintIds 需要删除的投诉管理主键
     * @return 结果
     */
    @Override
    public int deleteStaffComplaintByComplaintIds(Long[] complaintIds)
    {
        return staffComplaintMapper.deleteStaffComplaintByComplaintIds(complaintIds);
    }

    /**
     * 删除投诉管理信息
     * 
     * @param complaintId 投诉管理主键
     * @return 结果
     */
    @Override
    public int deleteStaffComplaintByComplaintId(Long complaintId)
    {
        return staffComplaintMapper.deleteStaffComplaintByComplaintId(complaintId);
    }

    @Override
    public List<StaffUser> owner() {
        return staffComplaintMapper.owner();
    }
}
