package com.ruoyi.toushuxinxi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.StaffUser;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.StaffComplaint;
import com.ruoyi.toushuxinxi.service.IStaffComplaintService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 投诉管理Controller
 * 
 * @author 郑东来
 * @date 2023-03-24
 */
@RestController
@RequestMapping("/toushuxinxi/toushuxinxi")
public class StaffComplaintController extends BaseController
{
    @Autowired
    private IStaffComplaintService staffComplaintService;

    /**
     * 查询投诉管理列表
     */
    @PreAuthorize("@ss.hasPermi('toushuxinxi:toushuxinxi:list')")
    @GetMapping("/list")
    public TableDataInfo list(StaffComplaint staffComplaint)
    {
        startPage();
        List<StaffComplaint> list = staffComplaintService.selectStaffComplaintList(staffComplaint);
        return getDataTable(list);
    }

    @GetMapping("/owner")
    public List<StaffUser> owner(){
        return staffComplaintService.owner();
    }

    /**
     * 导出投诉管理列表
     */
    @PreAuthorize("@ss.hasPermi('toushuxinxi:toushuxinxi:export')")
    @Log(title = "投诉管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StaffComplaint staffComplaint)
    {
        List<StaffComplaint> list = staffComplaintService.selectStaffComplaintList(staffComplaint);
        ExcelUtil<StaffComplaint> util = new ExcelUtil<StaffComplaint>(StaffComplaint.class);
        util.exportExcel(response, list, "投诉管理数据");
    }

    /**
     * 获取投诉管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('toushuxinxi:toushuxinxi:query')")
    @GetMapping(value = "/{complaintId}")
    public AjaxResult getInfo(@PathVariable("complaintId") Long complaintId)
    {
        return success(staffComplaintService.selectStaffComplaintByComplaintId(complaintId));
    }

    /**
     * 新增投诉管理
     */
    @PreAuthorize("@ss.hasPermi('toushuxinxi:toushuxinxi:add')")
    @Log(title = "投诉管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StaffComplaint staffComplaint)
    {
        return toAjax(staffComplaintService.insertStaffComplaint(staffComplaint));
    }

    /**
     * 修改投诉管理
     */
    @PreAuthorize("@ss.hasPermi('toushuxinxi:toushuxinxi:edit')")
    @Log(title = "投诉管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StaffComplaint staffComplaint)
    {
        return toAjax(staffComplaintService.updateStaffComplaint(staffComplaint));
    }

    /**
     * 删除投诉管理
     */
    @PreAuthorize("@ss.hasPermi('toushuxinxi:toushuxinxi:remove')")
    @Log(title = "投诉管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{complaintIds}")
    public AjaxResult remove(@PathVariable Long[] complaintIds)
    {
        return toAjax(staffComplaintService.deleteStaffComplaintByComplaintIds(complaintIds));
    }
}
