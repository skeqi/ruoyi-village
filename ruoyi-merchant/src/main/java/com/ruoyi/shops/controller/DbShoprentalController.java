package com.ruoyi.shops.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbShoprental;
import com.ruoyi.shops.service.IDbShoprentalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商铺出租Controller
 * 
 * @author wlx
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/shops/shoprental")
public class DbShoprentalController extends BaseController
{
    @Autowired
    private IDbShoprentalService dbShoprentalService;

    /**
     * 查询商铺出租列表
     */
    @PreAuthorize("@ss.hasPermi('shops:shoprental:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbShoprental dbShoprental)
    {
        startPage();
        List<DbShoprental> list = dbShoprentalService.selectDbShoprentalList(dbShoprental);
        return getDataTable(list);
    }

    /**
     * 导出商铺出租列表
     */
    @PreAuthorize("@ss.hasPermi('shops:shoprental:export')")
    @Log(title = "商铺出租", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbShoprental dbShoprental)
    {
        List<DbShoprental> list = dbShoprentalService.selectDbShoprentalList(dbShoprental);
        ExcelUtil<DbShoprental> util = new ExcelUtil<DbShoprental>(DbShoprental.class);
        util.exportExcel(response, list, "商铺出租数据");
    }

    /**
     * 获取商铺出租详细信息
     */
    @PreAuthorize("@ss.hasPermi('shops:shoprental:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(dbShoprentalService.selectDbShoprentalById(id));
    }

    /**
     * 新增商铺出租
     */
    @PreAuthorize("@ss.hasPermi('shops:shoprental:add')")
    @Log(title = "商铺出租", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbShoprental dbShoprental)
    {
        return toAjax(dbShoprentalService.insertDbShoprental(dbShoprental));
    }

    /**
     * 修改商铺出租
     */
    @PreAuthorize("@ss.hasPermi('shops:shoprental:edit')")
    @Log(title = "商铺出租", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbShoprental dbShoprental)
    {
        return toAjax(dbShoprentalService.updateDbShoprental(dbShoprental));
    }

    /**
     * 删除商铺出租
     */
    @PreAuthorize("@ss.hasPermi('shops:shoprental:remove')")
    @Log(title = "商铺出租", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(dbShoprentalService.deleteDbShoprentalByIds(ids));
    }
}
