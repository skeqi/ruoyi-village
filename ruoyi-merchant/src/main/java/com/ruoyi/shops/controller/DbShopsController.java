package com.ruoyi.shops.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbShops;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbShoprental;

import com.ruoyi.shops.service.IDbShopsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商铺信息Controller
 * 
 * @author wlx
 * @date 2023-03-07
 */
@RestController
@RequestMapping("/shops/shops")
public class DbShopsController extends BaseController
{
    @Autowired
    private IDbShopsService dbShopsService;

    /**
     * 查询商铺信息列表
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbShops dbShops)
    {
        startPage();
        List<DbShops> list = dbShopsService.selectDbShopsList(dbShops);
        return getDataTable(list);
    }

    /**
     * 导出商铺信息列表
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:export')")
    @Log(title = "商铺信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbShops dbShops)
    {
        List<DbShops> list = dbShopsService.selectDbShopsList(dbShops);
        ExcelUtil<DbShops> util = new ExcelUtil<DbShops>(DbShops.class);
        util.exportExcel(response, list, "商铺信息数据");
    }

    /**
     * 获取商铺信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(dbShopsService.selectDbShopsById(id));
    }

    /**
     * 新增商铺信息
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:add')")
    @Log(title = "商铺信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbShops dbShops)
    {
        return toAjax(dbShopsService.insertDbShops(dbShops));
    }

    /**
     * 修改商铺信息
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:edit')")
    @Log(title = "商铺信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbShops dbShops)
    {
        return toAjax(dbShopsService.updateDbShops(dbShops));
    }

    /**
     * 删除商铺信息
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:remove')")
    @Log(title = "商铺信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(dbShopsService.deleteDbShopsByIds(ids));
    }
}
