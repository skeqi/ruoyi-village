package com.ruoyi.shops.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.shops.mapper.DbShopsMapper;
import com.ruoyi.domain.DbShops;
import com.ruoyi.shops.service.IDbShopsService;

/**
 * 商铺信息Service业务层处理
 * 
 * @author wlx
 * @date 2023-03-07
 */
@Service
public class DbShopsServiceImpl implements IDbShopsService 
{
    @Autowired
    private DbShopsMapper dbShopsMapper;

    /**
     * 查询商铺信息
     * 
     * @param id 商铺信息主键
     * @return 商铺信息
     */
    @Override
    public DbShops selectDbShopsById(Integer id)
    {
        return dbShopsMapper.selectDbShopsById(id);
    }

    /**
     * 查询商铺信息列表
     * 
     * @param dbShops 商铺信息
     * @return 商铺信息
     */
    @Override
    public List<DbShops> selectDbShopsList(DbShops dbShops)
    {
        return dbShopsMapper.selectDbShopsList(dbShops);
    }

    /**
     * 新增商铺信息
     * 
     * @param dbShops 商铺信息
     * @return 结果
     */
    @Override
    public int insertDbShops(DbShops dbShops)
    {
        return dbShopsMapper.insertDbShops(dbShops);
    }

    /**
     * 修改商铺信息
     * 
     * @param dbShops 商铺信息
     * @return 结果
     */
    @Override
    public int updateDbShops(DbShops dbShops)
    {
        return dbShopsMapper.updateDbShops(dbShops);
    }

    /**
     * 批量删除商铺信息
     * 
     * @param ids 需要删除的商铺信息主键
     * @return 结果
     */
    @Override
    public int deleteDbShopsByIds(Integer[] ids)
    {
        return dbShopsMapper.deleteDbShopsByIds(ids);
    }

    /**
     * 删除商铺信息信息
     * 
     * @param id 商铺信息主键
     * @return 结果
     */
    @Override
    public int deleteDbShopsById(Integer id)
    {
        return dbShopsMapper.deleteDbShopsById(id);
    }
}
