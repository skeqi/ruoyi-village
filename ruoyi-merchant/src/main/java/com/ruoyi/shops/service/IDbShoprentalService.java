package com.ruoyi.shops.service;

import java.util.List;
import com.ruoyi.domain.DbShoprental;

/**
 * 商铺出租Service接口
 * 
 * @author wlx
 * @date 2023-03-17
 */
public interface IDbShoprentalService 
{
    /**
     * 查询商铺出租
     * 
     * @param id 商铺出租主键
     * @return 商铺出租
     */
    public DbShoprental selectDbShoprentalById(Integer id);

    /**
     * 查询商铺出租列表
     * 
     * @param dbShoprental 商铺出租
     * @return 商铺出租集合
     */
    public List<DbShoprental> selectDbShoprentalList(DbShoprental dbShoprental);

    /**
     * 新增商铺出租
     * 
     * @param dbShoprental 商铺出租
     * @return 结果
     */
    public int insertDbShoprental(DbShoprental dbShoprental);

    /**
     * 修改商铺出租
     * 
     * @param dbShoprental 商铺出租
     * @return 结果
     */
    public int updateDbShoprental(DbShoprental dbShoprental);

    /**
     * 批量删除商铺出租
     * 
     * @param ids 需要删除的商铺出租主键集合
     * @return 结果
     */
    public int deleteDbShoprentalByIds(Integer[] ids);

    /**
     * 删除商铺出租信息
     * 
     * @param id 商铺出租主键
     * @return 结果
     */
    public int deleteDbShoprentalById(Integer id);
}
