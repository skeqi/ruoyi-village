package com.ruoyi.shops.service.impl;

import java.util.List;

import com.ruoyi.domain.DbShops;
import com.ruoyi.shops.mapper.DbShopsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.shops.mapper.DbShoprentalMapper;
import com.ruoyi.domain.DbShoprental;
import com.ruoyi.shops.service.IDbShoprentalService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商铺出租Service业务层处理
 * 
 * @author wlx
 * @date 2023-03-17
 */
@Service
public class DbShoprentalServiceImpl implements IDbShoprentalService 
{
    @Autowired
    private DbShoprentalMapper dbShoprentalMapper;

    @Autowired
    private DbShopsMapper dbShopsMapper;

    /**
     * 查询商铺出租
     * 
     * @param id 商铺出租主键
     * @return 商铺出租
     */
    @Override
    public DbShoprental selectDbShoprentalById(Integer id)
    {
        return dbShoprentalMapper.selectDbShoprentalById(id);
    }

    /**
     * 查询商铺出租列表
     * 
     * @param dbShoprental 商铺出租
     * @return 商铺出租
     */
    @Override
    public List<DbShoprental> selectDbShoprentalList(DbShoprental dbShoprental)
    {
        return dbShoprentalMapper.selectDbShoprentalList(dbShoprental);
    }

    /**
     * 新增商铺出租,修改商铺状态
     * 
     * @param dbShoprental 商铺出租
     * @return 结果
     */
    @Override
    @Transactional
    public int insertDbShoprental(DbShoprental dbShoprental)
    {
        DbShops shops = new DbShops();
        shops.setId(dbShoprental.getShopsId());
        shops.setStatus(dbShoprental.getShops().getStatus());
        dbShopsMapper.updateDbShops(shops);
        return dbShoprentalMapper.insertDbShoprental(dbShoprental);
    }

    /**
     * 修改商铺出租
     * 
     * @param dbShoprental 商铺出租
     * @return 结果
     */
    @Override
    public int updateDbShoprental(DbShoprental dbShoprental)
    {
        return dbShoprentalMapper.updateDbShoprental(dbShoprental);
    }

    /**
     * 批量删除商铺出租
     * 
     * @param ids 需要删除的商铺出租主键
     * @return 结果
     */
    @Override
    public int deleteDbShoprentalByIds(Integer[] ids)
    {
        return dbShoprentalMapper.deleteDbShoprentalByIds(ids);
    }

    /**
     * 删除商铺出租信息
     * 
     * @param id 商铺出租主键
     * @return 结果
     */
    @Override
    public int deleteDbShoprentalById(Integer id)
    {
        return dbShoprentalMapper.deleteDbShoprentalById(id);
    }
}
