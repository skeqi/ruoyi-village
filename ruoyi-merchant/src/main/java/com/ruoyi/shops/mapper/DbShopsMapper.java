package com.ruoyi.shops.mapper;

import java.util.List;
import com.ruoyi.domain.DbShoprental;
import com.ruoyi.domain.DbShops;

/**
 * 商铺信息Mapper接口
 * 
 * @author wlx
 * @date 2023-03-07
 */
public interface DbShopsMapper 
{
    /**
     * 查询商铺信息
     * 
     * @param id 商铺信息主键
     * @return 商铺信息
     */
    public DbShops selectDbShopsById(Integer id);

    /**
     * 查询商铺信息列表
     * 
     * @param dbShops 商铺信息
     * @return 商铺信息集合
     */
    public List<DbShops> selectDbShopsList(DbShops dbShops);

    /**
     * 新增商铺信息
     * 
     * @param dbShops 商铺信息
     * @return 结果
     */
    public int insertDbShops(DbShops dbShops);

    /**
     * 修改商铺信息
     * 
     * @param dbShops 商铺信息
     * @return 结果
     */
    public int updateDbShops(DbShops dbShops);

    /**
     * 删除商铺信息
     * 
     * @param id 商铺信息主键
     * @return 结果
     */
    public int deleteDbShopsById(Integer id);

    /**
     * 批量删除商铺信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbShopsByIds(Integer[] ids);
}
