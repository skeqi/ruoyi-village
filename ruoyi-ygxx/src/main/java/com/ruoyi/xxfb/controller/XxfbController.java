package com.ruoyi.xxfb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.Xxfb;
import com.ruoyi.xxfb.service.IXxfbService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 信息发布Controller
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/xxfb/xxfb")
public class XxfbController extends BaseController
{
    @Autowired
    private IXxfbService xxfbService;

    /**
     * 查询信息发布列表
     */
    @PreAuthorize("@ss.hasPermi('xxfb:xxfb:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xxfb xxfb)
    {
        startPage();
        List<Xxfb> list = xxfbService.selectXxfbList(xxfb);
        return getDataTable(list);
    }

    /**
     * 导出信息发布列表
     */
    @PreAuthorize("@ss.hasPermi('xxfb:xxfb:export')")
    @Log(title = "信息发布", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Xxfb xxfb)
    {
        List<Xxfb> list = xxfbService.selectXxfbList(xxfb);
        ExcelUtil<Xxfb> util = new ExcelUtil<Xxfb>(Xxfb.class);
        util.exportExcel(response, list, "信息发布数据");
    }

    /**
     * 获取信息发布详细信息
     */
    @PreAuthorize("@ss.hasPermi('xxfb:xxfb:query')")
    @GetMapping(value = "/{xxfbId}")
    public AjaxResult getInfo(@PathVariable("xxfbId") Long xxfbId)
    {
        return success(xxfbService.selectXxfbByXxfbId(xxfbId));
    }

    /**
     * 新增信息发布
     */
    @PreAuthorize("@ss.hasPermi('xxfb:xxfb:add')")
    @Log(title = "信息发布", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xxfb xxfb)
    {
        return toAjax(xxfbService.insertXxfb(xxfb));
    }

    /**
     * 修改信息发布
     */
    @PreAuthorize("@ss.hasPermi('xxfb:xxfb:edit')")
    @Log(title = "信息发布", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xxfb xxfb)
    {
        return toAjax(xxfbService.updateXxfb(xxfb));
    }

    /**
     * 删除信息发布
     */
    @PreAuthorize("@ss.hasPermi('xxfb:xxfb:remove')")
    @Log(title = "信息发布", businessType = BusinessType.DELETE)
	@DeleteMapping("/{xxfbIds}")
    public AjaxResult remove(@PathVariable Long[] xxfbIds)
    {
        return toAjax(xxfbService.deleteXxfbByXxfbIds(xxfbIds));
    }
}
