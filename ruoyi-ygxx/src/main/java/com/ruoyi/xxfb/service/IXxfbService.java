package com.ruoyi.xxfb.service;

import java.util.List;
import com.ruoyi.domain.Xxfb;

/**
 * 信息发布Service接口
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
public interface IXxfbService 
{
    /**
     * 查询信息发布
     * 
     * @param xxfbId 信息发布主键
     * @return 信息发布
     */
    public Xxfb selectXxfbByXxfbId(Long xxfbId);

    /**
     * 查询信息发布列表
     * 
     * @param xxfb 信息发布
     * @return 信息发布集合
     */
    public List<Xxfb> selectXxfbList(Xxfb xxfb);

    /**
     * 新增信息发布
     * 
     * @param xxfb 信息发布
     * @return 结果
     */
    public int insertXxfb(Xxfb xxfb);

    /**
     * 修改信息发布
     * 
     * @param xxfb 信息发布
     * @return 结果
     */
    public int updateXxfb(Xxfb xxfb);

    /**
     * 批量删除信息发布
     * 
     * @param xxfbIds 需要删除的信息发布主键集合
     * @return 结果
     */
    public int deleteXxfbByXxfbIds(Long[] xxfbIds);

    /**
     * 删除信息发布信息
     * 
     * @param xxfbId 信息发布主键
     * @return 结果
     */
    public int deleteXxfbByXxfbId(Long xxfbId);
}
