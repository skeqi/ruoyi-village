package com.ruoyi.xxfb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xxfb.mapper.XxfbMapper;
import com.ruoyi.domain.Xxfb;
import com.ruoyi.xxfb.service.IXxfbService;

/**
 * 信息发布Service业务层处理
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
@Service
public class XxfbServiceImpl implements IXxfbService 
{
    @Autowired
    private XxfbMapper xxfbMapper;

    /**
     * 查询信息发布
     * 
     * @param xxfbId 信息发布主键
     * @return 信息发布
     */
    @Override
    public Xxfb selectXxfbByXxfbId(Long xxfbId)
    {
        return xxfbMapper.selectXxfbByXxfbId(xxfbId);
    }

    /**
     * 查询信息发布列表
     * 
     * @param xxfb 信息发布
     * @return 信息发布
     */
    @Override
    public List<Xxfb> selectXxfbList(Xxfb xxfb)
    {
        return xxfbMapper.selectXxfbList(xxfb);
    }

    /**
     * 新增信息发布
     * 
     * @param xxfb 信息发布
     * @return 结果
     */
    @Override
    public int insertXxfb(Xxfb xxfb)
    {
        return xxfbMapper.insertXxfb(xxfb);
    }

    /**
     * 修改信息发布
     * 
     * @param xxfb 信息发布
     * @return 结果
     */
    @Override
    public int updateXxfb(Xxfb xxfb)
    {
        return xxfbMapper.updateXxfb(xxfb);
    }

    /**
     * 批量删除信息发布
     * 
     * @param xxfbIds 需要删除的信息发布主键
     * @return 结果
     */
    @Override
    public int deleteXxfbByXxfbIds(Long[] xxfbIds)
    {
        return xxfbMapper.deleteXxfbByXxfbIds(xxfbIds);
    }

    /**
     * 删除信息发布信息
     * 
     * @param xxfbId 信息发布主键
     * @return 结果
     */
    @Override
    public int deleteXxfbByXxfbId(Long xxfbId)
    {
        return xxfbMapper.deleteXxfbByXxfbId(xxfbId);
    }
}
