package com.ruoyi.parkinglot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.*;
import com.ruoyi.parkinglot.service.IDbParkinglotService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 停车场Controller
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@RestController
@RequestMapping("/parkinglot/parkinglot")
public class DbParkinglotController extends BaseController
{
    @Autowired
    private IDbParkinglotService dbParkinglotService;

    /**
     * 查询停车场列表
     */
    @PreAuthorize("@ss.hasPermi('parkinglot:parkinglot:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbParkinglot dbParkinglot)
    {
        startPage();
        List<DbParkinglot> list = dbParkinglotService.selectDbParkinglotList(dbParkinglot);
        return getDataTable(list);
    }

    /**
     * 导出停车场列表
     */
    @PreAuthorize("@ss.hasPermi('parkinglot:parkinglot:export')")
    @Log(title = "停车场", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbParkinglot dbParkinglot)
    {
        List<DbParkinglot> list = dbParkinglotService.selectDbParkinglotList(dbParkinglot);
        ExcelUtil<DbParkinglot> util = new ExcelUtil<DbParkinglot>(DbParkinglot.class);
        util.exportExcel(response, list, "停车场数据");
    }

    /**
     * 获取停车场详细信息
     */
    @PreAuthorize("@ss.hasPermi('parkinglot:parkinglot:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbParkinglotService.selectDbParkinglotById(id));
    }

    /**
     * 新增停车场
     */
    @PreAuthorize("@ss.hasPermi('parkinglot:parkinglot:add')")
    @Log(title = "停车场", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbParkinglot dbParkinglot)
    {
        return toAjax(dbParkinglotService.insertDbParkinglot(dbParkinglot));
    }

    /**
     * 修改停车场
     */
    @PreAuthorize("@ss.hasPermi('parkinglot:parkinglot:edit')")
    @Log(title = "停车场", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbParkinglot dbParkinglot)
    {
        return toAjax(dbParkinglotService.updateDbParkinglot(dbParkinglot));
    }

    /**
     * 删除停车场
     */
    @PreAuthorize("@ss.hasPermi('parkinglot:parkinglot:remove')")
    @Log(title = "停车场", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbParkinglotService.deleteDbParkinglotByIds(ids));
    }
}
