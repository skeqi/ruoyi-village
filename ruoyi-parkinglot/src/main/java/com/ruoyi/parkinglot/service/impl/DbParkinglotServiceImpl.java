package com.ruoyi.parkinglot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.parkinglot.mapper.DbParkinglotMapper;
import com.ruoyi.domain.*;
import com.ruoyi.parkinglot.service.IDbParkinglotService;

/**
 * 停车场Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@Service
public class DbParkinglotServiceImpl implements IDbParkinglotService 
{
    @Autowired
    private DbParkinglotMapper dbParkinglotMapper;

    /**
     * 查询停车场
     * 
     * @param id 停车场主键
     * @return 停车场
     */
    @Override
    public DbParkinglot selectDbParkinglotById(Long id)
    {
        return dbParkinglotMapper.selectDbParkinglotById(id);
    }

    /**
     * 查询停车场列表
     * 
     * @param dbParkinglot 停车场
     * @return 停车场
     */
    @Override
    public List<DbParkinglot> selectDbParkinglotList(DbParkinglot dbParkinglot)
    {
        return dbParkinglotMapper.selectDbParkinglotList(dbParkinglot);
    }

    /**
     * 新增停车场
     * 
     * @param dbParkinglot 停车场
     * @return 结果
     */
    @Override
    public int insertDbParkinglot(DbParkinglot dbParkinglot)
    {
        return dbParkinglotMapper.insertDbParkinglot(dbParkinglot);
    }

    /**
     * 修改停车场
     * 
     * @param dbParkinglot 停车场
     * @return 结果
     */
    @Override
    public int updateDbParkinglot(DbParkinglot dbParkinglot)
    {
        return dbParkinglotMapper.updateDbParkinglot(dbParkinglot);
    }

    /**
     * 批量删除停车场
     * 
     * @param ids 需要删除的停车场主键
     * @return 结果
     */
    @Override
    public int deleteDbParkinglotByIds(Long[] ids)
    {
        return dbParkinglotMapper.deleteDbParkinglotByIds(ids);
    }

    /**
     * 删除停车场信息
     * 
     * @param id 停车场主键
     * @return 结果
     */
    @Override
    public int deleteDbParkinglotById(Long id)
    {
        return dbParkinglotMapper.deleteDbParkinglotById(id);
    }
}
