package com.ruoyi.parkinglot.service;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 停车场Service接口
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
public interface IDbParkinglotService 
{
    /**
     * 查询停车场
     * 
     * @param id 停车场主键
     * @return 停车场
     */
    public DbParkinglot selectDbParkinglotById(Long id);

    /**
     * 查询停车场列表
     * 
     * @param dbParkinglot 停车场
     * @return 停车场集合
     */
    public List<DbParkinglot> selectDbParkinglotList(DbParkinglot dbParkinglot);

    /**
     * 新增停车场
     * 
     * @param dbParkinglot 停车场
     * @return 结果
     */
    public int insertDbParkinglot(DbParkinglot dbParkinglot);

    /**
     * 修改停车场
     * 
     * @param dbParkinglot 停车场
     * @return 结果
     */
    public int updateDbParkinglot(DbParkinglot dbParkinglot);

    /**
     * 批量删除停车场
     * 
     * @param ids 需要删除的停车场主键集合
     * @return 结果
     */
    public int deleteDbParkinglotByIds(Long[] ids);

    /**
     * 删除停车场信息
     * 
     * @param id 停车场主键
     * @return 结果
     */
    public int deleteDbParkinglotById(Long id);
}
