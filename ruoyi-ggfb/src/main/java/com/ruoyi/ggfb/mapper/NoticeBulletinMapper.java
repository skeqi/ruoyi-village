package com.ruoyi.ggfb.mapper;

import java.util.List;
import com.ruoyi.domain.NoticeBulletin;

/**
 * 公告发布Mapper接口
 * 
 * @author koilty
 * @date 2023-03-07
 */
public interface NoticeBulletinMapper 
{
    /**
     * 查询公告发布
     * 
     * @param bulletinId 公告发布主键
     * @return 公告发布
     */
    public NoticeBulletin selectNoticeBulletinByBulletinId(Long bulletinId);

    /**
     * 查询公告发布列表
     * 
     * @param noticeBulletin 公告发布
     * @return 公告发布集合
     */
    public List<NoticeBulletin> selectNoticeBulletinList(NoticeBulletin noticeBulletin);

    /**
     * 新增公告发布
     * 
     * @param noticeBulletin 公告发布
     * @return 结果
     */
    public int insertNoticeBulletin(NoticeBulletin noticeBulletin);

    /**
     * 修改公告发布
     * 
     * @param noticeBulletin 公告发布
     * @return 结果
     */
    public int updateNoticeBulletin(NoticeBulletin noticeBulletin);

    /**
     * 删除公告发布
     * 
     * @param bulletinId 公告发布主键
     * @return 结果
     */
    public int deleteNoticeBulletinByBulletinId(Long bulletinId);

    /**
     * 批量删除公告发布
     * 
     * @param bulletinIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNoticeBulletinByBulletinIds(Long[] bulletinIds);
}
