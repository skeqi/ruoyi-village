package com.ruoyi.ggfb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ggfb.mapper.NoticeBulletinMapper;
import com.ruoyi.domain.NoticeBulletin;
import com.ruoyi.ggfb.service.INoticeBulletinService;

/**
 * 公告发布Service业务层处理
 * 
 * @author koilty
 * @date 2023-03-07
 */
@Service
public class NoticeBulletinServiceImpl implements INoticeBulletinService 
{
    @Autowired
    private NoticeBulletinMapper noticeBulletinMapper;

    /**
     * 查询公告发布
     * 
     * @param bulletinId 公告发布主键
     * @return 公告发布
     */
    @Override
    public NoticeBulletin selectNoticeBulletinByBulletinId(Long bulletinId)
    {
        return noticeBulletinMapper.selectNoticeBulletinByBulletinId(bulletinId);
    }

    /**
     * 查询公告发布列表
     * 
     * @param noticeBulletin 公告发布
     * @return 公告发布
     */
    @Override
    public List<NoticeBulletin> selectNoticeBulletinList(NoticeBulletin noticeBulletin)
    {
        return noticeBulletinMapper.selectNoticeBulletinList(noticeBulletin);
    }

    /**
     * 新增公告发布
     * 
     * @param noticeBulletin 公告发布
     * @return 结果
     */
    @Override
    public int insertNoticeBulletin(NoticeBulletin noticeBulletin)
    {
        return noticeBulletinMapper.insertNoticeBulletin(noticeBulletin);
    }

    /**
     * 修改公告发布
     * 
     * @param noticeBulletin 公告发布
     * @return 结果
     */
    @Override
    public int updateNoticeBulletin(NoticeBulletin noticeBulletin)
    {
        return noticeBulletinMapper.updateNoticeBulletin(noticeBulletin);
    }

    /**
     * 批量删除公告发布
     * 
     * @param bulletinIds 需要删除的公告发布主键
     * @return 结果
     */
    @Override
    public int deleteNoticeBulletinByBulletinIds(Long[] bulletinIds)
    {
        return noticeBulletinMapper.deleteNoticeBulletinByBulletinIds(bulletinIds);
    }

    /**
     * 删除公告发布信息
     * 
     * @param bulletinId 公告发布主键
     * @return 结果
     */
    @Override
    public int deleteNoticeBulletinByBulletinId(Long bulletinId)
    {
        return noticeBulletinMapper.deleteNoticeBulletinByBulletinId(bulletinId);
    }
}
