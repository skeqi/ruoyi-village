package com.ruoyi.ggfb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.NoticeBulletin;
import com.ruoyi.ggfb.service.INoticeBulletinService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公告发布Controller
 * 
 * @author koilty
 * @date 2023-03-07
 */
@RestController
@RequestMapping("/ggfb/ggfb")
public class NoticeBulletinController extends BaseController
{
    @Autowired
    private INoticeBulletinService noticeBulletinService;

    /**
     * 查询公告发布列表
     */
    @PreAuthorize("@ss.hasPermi('ggfb:ggfb:list')")
    @GetMapping("/list")
    public TableDataInfo list(NoticeBulletin noticeBulletin)
    {
        startPage();
        List<NoticeBulletin> list = noticeBulletinService.selectNoticeBulletinList(noticeBulletin);
        return getDataTable(list);
    }

    /**
     * 导出公告发布列表
     */
    @PreAuthorize("@ss.hasPermi('ggfb:ggfb:export')")
    @Log(title = "公告发布", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NoticeBulletin noticeBulletin)
    {
        List<NoticeBulletin> list = noticeBulletinService.selectNoticeBulletinList(noticeBulletin);
        ExcelUtil<NoticeBulletin> util = new ExcelUtil<NoticeBulletin>(NoticeBulletin.class);
        util.exportExcel(response, list, "公告发布数据");
    }

    /**
     * 获取公告发布详细信息
     */
    @PreAuthorize("@ss.hasPermi('ggfb:ggfb:query')")
    @GetMapping(value = "/{bulletinId}")
    public AjaxResult getInfo(@PathVariable("bulletinId") Long bulletinId)
    {
        return success(noticeBulletinService.selectNoticeBulletinByBulletinId(bulletinId));
    }

    /**
     * 新增公告发布
     */
    @PreAuthorize("@ss.hasPermi('ggfb:ggfb:add')")
    @Log(title = "公告发布", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NoticeBulletin noticeBulletin)
    {
        return toAjax(noticeBulletinService.insertNoticeBulletin(noticeBulletin));
    }

    /**
     * 修改公告发布
     */
    @PreAuthorize("@ss.hasPermi('ggfb:ggfb:edit')")
    @Log(title = "公告发布", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NoticeBulletin noticeBulletin)
    {
        return toAjax(noticeBulletinService.updateNoticeBulletin(noticeBulletin));
    }

    /**
     * 删除公告发布
     */
    @PreAuthorize("@ss.hasPermi('ggfb:ggfb:remove')")
    @Log(title = "公告发布", businessType = BusinessType.DELETE)
	@DeleteMapping("/{bulletinIds}")
    public AjaxResult remove(@PathVariable Long[] bulletinIds)
    {
        return toAjax(noticeBulletinService.deleteNoticeBulletinByBulletinIds(bulletinIds));
    }
}
