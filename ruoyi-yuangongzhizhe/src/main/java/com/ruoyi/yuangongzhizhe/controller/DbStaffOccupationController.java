package com.ruoyi.yuangongzhizhe.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbStaffOccupation;
import com.ruoyi.yuangongzhizhe.service.IDbStaffOccupationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 员工职业Controller
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/yuangongzhizhe/yuangongzhizhe")
public class DbStaffOccupationController extends BaseController
{
    @Autowired
    private IDbStaffOccupationService dbStaffOccupationService;

    /**
     * 查询员工职业列表
     */
    @PreAuthorize("@ss.hasPermi('yuangongzhizhe:yuangongzhizhe:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbStaffOccupation dbStaffOccupation)
    {
        startPage();
        List<DbStaffOccupation> list = dbStaffOccupationService.selectDbStaffOccupationList(dbStaffOccupation);
        return getDataTable(list);
    }

    /**
     * 导出员工职业列表
     */
    @PreAuthorize("@ss.hasPermi('yuangongzhizhe:yuangongzhizhe:export')")
    @Log(title = "员工职业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbStaffOccupation dbStaffOccupation)
    {
        List<DbStaffOccupation> list = dbStaffOccupationService.selectDbStaffOccupationList(dbStaffOccupation);
        ExcelUtil<DbStaffOccupation> util = new ExcelUtil<DbStaffOccupation>(DbStaffOccupation.class);
        util.exportExcel(response, list, "员工职业数据");
    }

    /**
     * 获取员工职业详细信息
     */
    @PreAuthorize("@ss.hasPermi('yuangongzhizhe:yuangongzhizhe:query')")
    @GetMapping(value = "/{staffId}")
    public AjaxResult getInfo(@PathVariable("staffId") Long staffId)
    {
        return success(dbStaffOccupationService.selectDbStaffOccupationByStaffId(staffId));
    }

    /**
     * 新增员工职业
     */
    @PreAuthorize("@ss.hasPermi('yuangongzhizhe:yuangongzhizhe:add')")
    @Log(title = "员工职业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbStaffOccupation dbStaffOccupation)
    {
        return toAjax(dbStaffOccupationService.insertDbStaffOccupation(dbStaffOccupation));
    }

    /**
     * 修改员工职业
     */
    @PreAuthorize("@ss.hasPermi('yuangongzhizhe:yuangongzhizhe:edit')")
    @Log(title = "员工职业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbStaffOccupation dbStaffOccupation)
    {
        return toAjax(dbStaffOccupationService.updateDbStaffOccupation(dbStaffOccupation));
    }

    /**
     * 删除员工职业
     */
    @PreAuthorize("@ss.hasPermi('yuangongzhizhe:yuangongzhizhe:remove')")
    @Log(title = "员工职业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{staffIds}")
    public AjaxResult remove(@PathVariable Long[] staffIds)
    {
        return toAjax(dbStaffOccupationService.deleteDbStaffOccupationByStaffIds(staffIds));
    }
}
