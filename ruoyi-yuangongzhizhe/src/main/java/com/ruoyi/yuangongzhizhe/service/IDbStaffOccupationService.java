package com.ruoyi.yuangongzhizhe.service;

import java.util.List;
import com.ruoyi.domain.DbStaffOccupation;

/**
 * 员工职业Service接口
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
public interface IDbStaffOccupationService 
{
    /**
     * 查询员工职业
     * 
     * @param staffId 员工职业主键
     * @return 员工职业
     */
    public DbStaffOccupation selectDbStaffOccupationByStaffId(Long staffId);

    /**
     * 查询员工职业列表
     * 
     * @param dbStaffOccupation 员工职业
     * @return 员工职业集合
     */
    public List<DbStaffOccupation> selectDbStaffOccupationList(DbStaffOccupation dbStaffOccupation);

    /**
     * 新增员工职业
     * 
     * @param dbStaffOccupation 员工职业
     * @return 结果
     */
    public int insertDbStaffOccupation(DbStaffOccupation dbStaffOccupation);

    /**
     * 修改员工职业
     * 
     * @param dbStaffOccupation 员工职业
     * @return 结果
     */
    public int updateDbStaffOccupation(DbStaffOccupation dbStaffOccupation);

    /**
     * 批量删除员工职业
     * 
     * @param staffIds 需要删除的员工职业主键集合
     * @return 结果
     */
    public int deleteDbStaffOccupationByStaffIds(Long[] staffIds);

    /**
     * 删除员工职业信息
     * 
     * @param staffId 员工职业主键
     * @return 结果
     */
    public int deleteDbStaffOccupationByStaffId(Long staffId);
}
