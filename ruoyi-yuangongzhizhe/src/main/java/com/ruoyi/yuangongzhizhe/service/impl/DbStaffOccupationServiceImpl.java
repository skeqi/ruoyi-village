package com.ruoyi.yuangongzhizhe.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.yuangongzhizhe.mapper.DbStaffOccupationMapper;
import com.ruoyi.domain.DbStaffOccupation;
import com.ruoyi.yuangongzhizhe.service.IDbStaffOccupationService;

/**
 * 员工职业Service业务层处理
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
@Service
public class DbStaffOccupationServiceImpl implements IDbStaffOccupationService 
{
    @Autowired
    private DbStaffOccupationMapper dbStaffOccupationMapper;

    /**
     * 查询员工职业
     * 
     * @param staffId 员工职业主键
     * @return 员工职业
     */
    @Override
    public DbStaffOccupation selectDbStaffOccupationByStaffId(Long staffId)
    {
        return dbStaffOccupationMapper.selectDbStaffOccupationByStaffId(staffId);
    }

    /**
     * 查询员工职业列表
     * 
     * @param dbStaffOccupation 员工职业
     * @return 员工职业
     */
    @Override
    public List<DbStaffOccupation> selectDbStaffOccupationList(DbStaffOccupation dbStaffOccupation)
    {
        return dbStaffOccupationMapper.selectDbStaffOccupationList(dbStaffOccupation);
    }

    /**
     * 新增员工职业
     * 
     * @param dbStaffOccupation 员工职业
     * @return 结果
     */
    @Override
    public int insertDbStaffOccupation(DbStaffOccupation dbStaffOccupation)
    {
        return dbStaffOccupationMapper.insertDbStaffOccupation(dbStaffOccupation);
    }

    /**
     * 修改员工职业
     * 
     * @param dbStaffOccupation 员工职业
     * @return 结果
     */
    @Override
    public int updateDbStaffOccupation(DbStaffOccupation dbStaffOccupation)
    {
        return dbStaffOccupationMapper.updateDbStaffOccupation(dbStaffOccupation);
    }

    /**
     * 批量删除员工职业
     * 
     * @param staffIds 需要删除的员工职业主键
     * @return 结果
     */
    @Override
    public int deleteDbStaffOccupationByStaffIds(Long[] staffIds)
    {
        return dbStaffOccupationMapper.deleteDbStaffOccupationByStaffIds(staffIds);
    }

    /**
     * 删除员工职业信息
     * 
     * @param staffId 员工职业主键
     * @return 结果
     */
    @Override
    public int deleteDbStaffOccupationByStaffId(Long staffId)
    {
        return dbStaffOccupationMapper.deleteDbStaffOccupationByStaffId(staffId);
    }
}
