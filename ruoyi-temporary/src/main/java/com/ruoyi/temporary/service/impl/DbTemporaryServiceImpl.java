package com.ruoyi.temporary.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.temporary.mapper.DbTemporaryMapper;
import com.ruoyi.domain.DbTemporary;
import com.ruoyi.temporary.service.IDbTemporaryService;

/**
 * 临时车管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class DbTemporaryServiceImpl implements IDbTemporaryService 
{
    @Autowired
    private DbTemporaryMapper dbTemporaryMapper;

    /**
     * 查询临时车管理
     * 
     * @param id 临时车管理主键
     * @return 临时车管理
     */
    @Override
    public DbTemporary selectDbTemporaryById(Long id)
    {
        return dbTemporaryMapper.selectDbTemporaryById(id);
    }

    /**
     * 查询临时车管理列表
     * 
     * @param dbTemporary 临时车管理
     * @return 临时车管理
     */
    @Override
    public List<DbTemporary> selectDbTemporaryList(DbTemporary dbTemporary)
    {
        return dbTemporaryMapper.selectDbTemporaryList(dbTemporary);
    }

    /**
     * 新增临时车管理
     * 
     * @param dbTemporary 临时车管理
     * @return 结果
     */
    @Override
    public int insertDbTemporary(DbTemporary dbTemporary)
    {
        return dbTemporaryMapper.insertDbTemporary(dbTemporary);
    }

    /**
     * 修改临时车管理
     * 
     * @param dbTemporary 临时车管理
     * @return 结果
     */
    @Override
    public int updateDbTemporary(DbTemporary dbTemporary)
    {
        return dbTemporaryMapper.updateDbTemporary(dbTemporary);
    }

    /**
     * 批量删除临时车管理
     * 
     * @param ids 需要删除的临时车管理主键
     * @return 结果
     */
    @Override
    public int deleteDbTemporaryByIds(Long[] ids)
    {
        return dbTemporaryMapper.deleteDbTemporaryByIds(ids);
    }

    /**
     * 删除临时车管理信息
     * 
     * @param id 临时车管理主键
     * @return 结果
     */
    @Override
    public int deleteDbTemporaryById(Long id)
    {
        return dbTemporaryMapper.deleteDbTemporaryById(id);
    }
}
