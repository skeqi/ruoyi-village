package com.ruoyi.temporary.mapper;

import java.util.List;
import com.ruoyi.domain.DbTemporary;

/**
 * 临时车管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
public interface DbTemporaryMapper 
{
    /**
     * 查询临时车管理
     * 
     * @param id 临时车管理主键
     * @return 临时车管理
     */
    public DbTemporary selectDbTemporaryById(Long id);

    /**
     * 查询临时车管理列表
     * 
     * @param dbTemporary 临时车管理
     * @return 临时车管理集合
     */
    public List<DbTemporary> selectDbTemporaryList(DbTemporary dbTemporary);

    /**
     * 新增临时车管理
     * 
     * @param dbTemporary 临时车管理
     * @return 结果
     */
    public int insertDbTemporary(DbTemporary dbTemporary);

    /**
     * 修改临时车管理
     * 
     * @param dbTemporary 临时车管理
     * @return 结果
     */
    public int updateDbTemporary(DbTemporary dbTemporary);

    /**
     * 删除临时车管理
     * 
     * @param id 临时车管理主键
     * @return 结果
     */
    public int deleteDbTemporaryById(Long id);

    /**
     * 批量删除临时车管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbTemporaryByIds(Long[] ids);
}
