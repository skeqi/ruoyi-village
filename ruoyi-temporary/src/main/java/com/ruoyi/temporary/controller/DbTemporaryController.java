package com.ruoyi.temporary.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbTemporary;
import com.ruoyi.temporary.service.IDbTemporaryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 临时车管理Controller
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@RestController
@RequestMapping("/temporary/temporary")
public class DbTemporaryController extends BaseController
{
    @Autowired
    private IDbTemporaryService dbTemporaryService;

    /**
     * 查询临时车管理列表
     */
    @PreAuthorize("@ss.hasPermi('temporary:temporary:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbTemporary dbTemporary)
    {
        startPage();
        List<DbTemporary> list = dbTemporaryService.selectDbTemporaryList(dbTemporary);
        return getDataTable(list);
    }

    /**
     * 导出临时车管理列表
     */
    @PreAuthorize("@ss.hasPermi('temporary:temporary:export')")
    @Log(title = "临时车管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbTemporary dbTemporary)
    {
        List<DbTemporary> list = dbTemporaryService.selectDbTemporaryList(dbTemporary);
        ExcelUtil<DbTemporary> util = new ExcelUtil<DbTemporary>(DbTemporary.class);
        util.exportExcel(response, list, "临时车管理数据");
    }

    /**
     * 获取临时车管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('temporary:temporary:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbTemporaryService.selectDbTemporaryById(id));
    }

    /**
     * 新增临时车管理
     */
    @PreAuthorize("@ss.hasPermi('temporary:temporary:add')")
    @Log(title = "临时车管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbTemporary dbTemporary)
    {
        return toAjax(dbTemporaryService.insertDbTemporary(dbTemporary));
    }

    /**
     * 修改临时车管理
     */
    @PreAuthorize("@ss.hasPermi('temporary:temporary:edit')")
    @Log(title = "临时车管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbTemporary dbTemporary)
    {
        return toAjax(dbTemporaryService.updateDbTemporary(dbTemporary));
    }

    /**
     * 删除临时车管理
     */
    @PreAuthorize("@ss.hasPermi('temporary:temporary:remove')")
    @Log(title = "临时车管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbTemporaryService.deleteDbTemporaryByIds(ids));
    }
}
