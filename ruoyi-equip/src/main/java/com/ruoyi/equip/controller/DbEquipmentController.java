package com.ruoyi.equip.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbEquipment;
import com.ruoyi.domain.StaffUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.equip.service.IDbEquipmentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.domain.*;
/**
 * 设备信息Controller
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
@Api("设备管理-设备信息")
@RestController
@RequestMapping("/equip/ment")
public class DbEquipmentController extends BaseController
{
    @Autowired
    private IDbEquipmentService dbEquipmentService;

    /**
     * 查询设备信息列表
     */
    @PreAuthorize("@ss.hasPermi('equip:ment:list')")
    @GetMapping("/list")
    @ApiOperation("设备信息列表")
    public TableDataInfo list(DbEquipment dbEquipment)
    {
        startPage();
        List<DbEquipment> list = dbEquipmentService.selectDbEquipmentList(dbEquipment);
        return getDataTable(list);
    }

    @GetMapping("/repairList")
    public List<StaffUser> repairList(){
        return dbEquipmentService.findStaffUser();
    }

    /**
     * 导出设备信息列表
     */
    @PreAuthorize("@ss.hasPermi('equip:ment:export')")
    @Log(title = "设备信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("设备信息导出")
    public void export(HttpServletResponse response, DbEquipment dbEquipment)
    {
        List<DbEquipment> list = dbEquipmentService.selectDbEquipmentList(dbEquipment);
        ExcelUtil<DbEquipment> util = new ExcelUtil<DbEquipment>(DbEquipment.class);
        util.exportExcel(response, list, "设备信息数据");
    }

    /**
     * 获取设备信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('equip:ment:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("设备信息详情")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbEquipmentService.selectDbEquipmentById(id));
    }

    /**
     * 新增设备信息
     */
    @PreAuthorize("@ss.hasPermi('equip:ment:add')")
    @Log(title = "设备信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("设备信息新增")
    public AjaxResult add(@RequestBody DbEquipment dbEquipment)
    {
        return toAjax(dbEquipmentService.insertDbEquipment(dbEquipment));
    }

    /**
     * 修改设备信息
     */
    @PreAuthorize("@ss.hasPermi('equip:ment:edit')")
    @Log(title = "设备信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("设备信息修改")
    public AjaxResult edit(@RequestBody DbEquipment dbEquipment)
    {
        return toAjax(dbEquipmentService.updateDbEquipment(dbEquipment));
    }

    /**
     * 删除设备信息
     */
    @PreAuthorize("@ss.hasPermi('equip:ment:remove')")
    @Log(title = "设备信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("设备信息删除")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbEquipmentService.deleteDbEquipmentByIds(ids));
    }
}
