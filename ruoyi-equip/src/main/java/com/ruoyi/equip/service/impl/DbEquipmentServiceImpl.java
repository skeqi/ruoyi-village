package com.ruoyi.equip.service.impl;

import java.util.List;

import com.ruoyi.domain.DbEquipment;
import com.ruoyi.domain.StaffUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.equip.mapper.DbEquipmentMapper;
import com.ruoyi.equip.service.IDbEquipmentService;
import com.ruoyi.domain.*;
/**
 * 设备信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
@Service
public class DbEquipmentServiceImpl implements IDbEquipmentService 
{
    @Autowired
    private DbEquipmentMapper dbEquipmentMapper;

    /**
     * 查询设备信息
     * 
     * @param id 设备信息主键
     * @return 设备信息
     */
    @Override
    public DbEquipment selectDbEquipmentById(Long id)
    {
        return dbEquipmentMapper.selectDbEquipmentById(id);
    }

    /**
     * 查询设备信息列表
     * 
     * @param dbEquipment 设备信息
     * @return 设备信息
     */
    @Override
    public List<DbEquipment> selectDbEquipmentList(DbEquipment dbEquipment)
    {
        return dbEquipmentMapper.selectDbEquipmentList(dbEquipment);
    }

    /**
     * 新增设备信息
     * 
     * @param dbEquipment 设备信息
     * @return 结果
     */
    @Override
    public int insertDbEquipment(DbEquipment dbEquipment)
    {
        return dbEquipmentMapper.insertDbEquipment(dbEquipment);
    }

    /**
     * 修改设备信息
     * 
     * @param dbEquipment 设备信息
     * @return 结果
     */
    @Override
    public int updateDbEquipment(DbEquipment dbEquipment)
    {
        return dbEquipmentMapper.updateDbEquipment(dbEquipment);
    }

    /**
     * 批量删除设备信息
     * 
     * @param ids 需要删除的设备信息主键
     * @return 结果
     */
    @Override
    public int deleteDbEquipmentByIds(Long[] ids)
    {
        return dbEquipmentMapper.deleteDbEquipmentByIds(ids);
    }

    /**
     * 删除设备信息信息
     * 
     * @param id 设备信息主键
     * @return 结果
     */
    @Override
    public int deleteDbEquipmentById(Long id)
    {
        return dbEquipmentMapper.deleteDbEquipmentById(id);
    }

    @Override
    public List<StaffUser> findStaffUser() {
        return dbEquipmentMapper.findStaffUser();
    }
}
