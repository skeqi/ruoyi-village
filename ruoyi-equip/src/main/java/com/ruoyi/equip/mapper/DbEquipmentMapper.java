package com.ruoyi.equip.mapper;

import com.ruoyi.domain.DbEquipment;
import com.ruoyi.domain.StaffUser;

import java.util.List;
import com.ruoyi.domain.*;
/**
 * 设备信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
public interface DbEquipmentMapper 
{
    /**
     * 查询设备信息
     * 
     * @param id 设备信息主键
     * @return 设备信息
     */
    public DbEquipment selectDbEquipmentById(Long id);

    /**
     * 查询设备信息列表
     * 
     * @param dbEquipment 设备信息
     * @return 设备信息集合
     */
    public List<DbEquipment> selectDbEquipmentList(DbEquipment dbEquipment);

    /**
     * 新增设备信息
     * 
     * @param dbEquipment 设备信息
     * @return 结果
     */
    public int insertDbEquipment(DbEquipment dbEquipment);

    /**
     * 修改设备信息
     * 
     * @param dbEquipment 设备信息
     * @return 结果
     */
    public int updateDbEquipment(DbEquipment dbEquipment);

    /**
     * 删除设备信息
     * 
     * @param id 设备信息主键
     * @return 结果
     */
    public int deleteDbEquipmentById(Long id);

    /**
     * 批量删除设备信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbEquipmentByIds(Long[] ids);

    public List<StaffUser> findStaffUser();
}
