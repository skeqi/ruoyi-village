package com.ruoyi.pay.mapper;

import java.util.List;
import com.ruoyi.domain.DbTemvehicleStandard;

/**
 * 临时车收费标准Mapper接口
 * 
 * @author wlx
 * @date 2023-03-09
 */
public interface DbTemvehicleStandardMapper 
{
    /**
     * 查询临时车收费标准
     * 
     * @param id 临时车收费标准主键
     * @return 临时车收费标准
     */
    public DbTemvehicleStandard selectDbTemvehicleStandardById(Long id);

    /**
     * 查询临时车收费标准列表
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 临时车收费标准集合
     */
    public List<DbTemvehicleStandard> selectDbTemvehicleStandardList(DbTemvehicleStandard dbTemvehicleStandard);

    /**
     * 新增临时车收费标准
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 结果
     */
    public int insertDbTemvehicleStandard(DbTemvehicleStandard dbTemvehicleStandard);

    /**
     * 修改临时车收费标准
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 结果
     */
    public int updateDbTemvehicleStandard(DbTemvehicleStandard dbTemvehicleStandard);

    /**
     * 删除临时车收费标准
     * 
     * @param id 临时车收费标准主键
     * @return 结果
     */
    public int deleteDbTemvehicleStandardById(Long id);

    /**
     * 批量删除临时车收费标准
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbTemvehicleStandardByIds(Long[] ids);
}
