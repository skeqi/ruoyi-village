package com.ruoyi.pay.mapper;

import java.util.List;
import com.ruoyi.domain.DbBillcall;

/**
 * 账单催缴Mapper接口
 * 
 * @author wlx
 * @date 2023-03-14
 */
public interface DbBillcallMapper 
{
    /**
     * 查询账单催缴
     * 
     * @param id 账单催缴主键
     * @return 账单催缴
     */
    public DbBillcall selectDbBillcallById(Long id);

    /**
     * 查询账单催缴列表
     * 
     * @param dbBillcall 账单催缴
     * @return 账单催缴集合
     */
    public List<DbBillcall> selectDbBillcallList(DbBillcall dbBillcall);

    /**
     * 新增账单催缴
     * 
     * @param dbBillcall 账单催缴
     * @return 结果
     */
    public int insertDbBillcall(DbBillcall dbBillcall);

    /**
     * 修改账单催缴
     * 
     * @param dbBillcall 账单催缴
     * @return 结果
     */
    public int updateDbBillcall(DbBillcall dbBillcall);

    /**
     * 删除账单催缴
     * 
     * @param id 账单催缴主键
     * @return 结果
     */
    public int deleteDbBillcallById(Long id);

    /**
     * 批量删除账单催缴
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbBillcallByIds(Long[] ids);
}
