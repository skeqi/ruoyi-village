package com.ruoyi.pay.mapper;

import java.util.List;
import com.ruoyi.domain.DbPayParking;

/**
 * 临时车缴费Mapper接口
 * 
 * @author wlx
 * @date 2023-03-09
 */
public interface DbPayParkingMapper 
{
    /**
     * 查询临时车缴费
     * 
     * @param id 临时车缴费主键
     * @return 临时车缴费
     */
    public DbPayParking selectDbPayParkingById(Long id);

    /**
     * 查询临时车缴费列表
     * 
     * @param dbPayParking 临时车缴费
     * @return 临时车缴费集合
     */
    public List<DbPayParking> selectDbPayParkingList(DbPayParking dbPayParking);

    /**
     * 新增临时车缴费
     * 
     * @param dbPayParking 临时车缴费
     * @return 结果
     */
    public int insertDbPayParking(DbPayParking dbPayParking);

    /**
     * 修改临时车缴费
     * 
     * @param dbPayParking 临时车缴费
     * @return 结果
     */
    public int updateDbPayParking(DbPayParking dbPayParking);

    /**
     * 删除临时车缴费
     * 
     * @param id 临时车缴费主键
     * @return 结果
     */
    public int deleteDbPayParkingById(Long id);

    /**
     * 批量删除临时车缴费
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbPayParkingByIds(Long[] ids);
}
