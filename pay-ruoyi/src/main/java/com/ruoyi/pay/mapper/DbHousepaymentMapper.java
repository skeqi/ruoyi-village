package com.ruoyi.pay.mapper;

import java.util.List;
import com.ruoyi.domain.DbHousepayment;

/**
 * 房屋缴费Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
public interface DbHousepaymentMapper 
{
    /**
     * 查询房屋缴费
     * 
     * @param id 房屋缴费主键
     * @return 房屋缴费
     */
    public DbHousepayment selectDbHousepaymentById(Long id);

    /**
     * 查询房屋缴费列表
     * 
     * @param dbHousepayment 房屋缴费
     * @return 房屋缴费集合
     */
    public List<DbHousepayment> selectDbHousepaymentList(DbHousepayment dbHousepayment);

    /**
     * 新增房屋缴费
     * 
     * @param dbHousepayment 房屋缴费
     * @return 结果
     */
    public int insertDbHousepayment(DbHousepayment dbHousepayment);

    /**
     * 修改房屋缴费
     * 
     * @param dbHousepayment 房屋缴费
     * @return 结果
     */
    public int updateDbHousepayment(DbHousepayment dbHousepayment);

    /**
     * 删除房屋缴费
     * 
     * @param id 房屋缴费主键
     * @return 结果
     */
    public int deleteDbHousepaymentById(Long id);

    /**
     * 批量删除房屋缴费
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbHousepaymentByIds(Long[] ids);
}
