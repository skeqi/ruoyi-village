package com.ruoyi.pay.mapper;

import java.util.List;
import com.ruoyi.domain.DbCharging;
import org.apache.ibatis.annotations.Param;

/**
 * 收费标准Mapper接口
 * 
 * @author wlx
 * @date 2023-03-08
 */
public interface DbChargingMapper 
{
    public DbCharging selectChargingByExptype(@Param("exptype") Long exptype);

    /**
     * 查询收费标准
     * 
     * @param id 收费标准主键
     * @return 收费标准
     */
    public DbCharging selectDbChargingById(Long id);

    /**
     * 查询收费标准列表
     * 
     * @param dbCharging 收费标准
     * @return 收费标准集合
     */
    public List<DbCharging> selectDbChargingList(DbCharging dbCharging);

    /**
     * 新增收费标准
     * 
     * @param dbCharging 收费标准
     * @return 结果
     */
    public int insertDbCharging(DbCharging dbCharging);

    /**
     * 修改收费标准
     * 
     * @param dbCharging 收费标准
     * @return 结果
     */
    public int updateDbCharging(DbCharging dbCharging);

    /**
     * 删除收费标准
     * 
     * @param id 收费标准主键
     * @return 结果
     */
    public int deleteDbChargingById(Long id);

    /**
     * 批量删除收费标准
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbChargingByIds(Long[] ids);
}
