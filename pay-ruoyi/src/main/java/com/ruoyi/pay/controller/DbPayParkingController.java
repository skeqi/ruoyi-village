package com.ruoyi.pay.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbPayParking;
import com.ruoyi.pay.service.IDbPayParkingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 临时车缴费Controller
 * 
 * @author wlx
 * @date 2023-03-09
 */
@RestController
@RequestMapping("/pay/parking")
public class DbPayParkingController extends BaseController
{
    @Autowired
    private IDbPayParkingService dbPayParkingService;

    /**
     * 查询临时车缴费列表
     */
    @PreAuthorize("@ss.hasPermi('pay:parking:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbPayParking dbPayParking)
    {
        startPage();
        List<DbPayParking> list = dbPayParkingService.selectDbPayParkingList(dbPayParking);
        return getDataTable(list);
    }

    /**
     * 导出临时车缴费列表
     */
    @PreAuthorize("@ss.hasPermi('pay:parking:export')")
    @Log(title = "临时车缴费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbPayParking dbPayParking)
    {
        List<DbPayParking> list = dbPayParkingService.selectDbPayParkingList(dbPayParking);
        ExcelUtil<DbPayParking> util = new ExcelUtil<DbPayParking>(DbPayParking.class);
        util.exportExcel(response, list, "临时车缴费数据");
    }

    /**
     * 获取临时车缴费详细信息
     */
    @PreAuthorize("@ss.hasPermi('pay:parking:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbPayParkingService.selectDbPayParkingById(id));
    }

    /**
     * 新增临时车缴费
     */
    @PreAuthorize("@ss.hasPermi('pay:parking:add')")
    @Log(title = "临时车缴费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbPayParking dbPayParking)
    {
        return toAjax(dbPayParkingService.insertDbPayParking(dbPayParking));
    }

    /**
     * 修改临时车缴费
     */
    @PreAuthorize("@ss.hasPermi('pay:parking:edit')")
    @Log(title = "临时车缴费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbPayParking dbPayParking)
    {
        return toAjax(dbPayParkingService.updateDbPayParking(dbPayParking));
    }

    /**
     * 删除临时车缴费
     */
    @PreAuthorize("@ss.hasPermi('pay:parking:remove')")
    @Log(title = "临时车缴费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbPayParkingService.deleteDbPayParkingByIds(ids));
    }
}
