package com.ruoyi.pay.controller;

import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.annotation.Id;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbHousepayment;
import com.ruoyi.pay.service.IDbHousepaymentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 房屋缴费Controller
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
@Api(tags="缴费管理-房屋缴费")
@RestController
@RequestMapping("/pay/housepay")
public class DbHousepaymentController extends BaseController
{
    @Autowired
    private IDbHousepaymentService dbHousepaymentService;

    @ApiOperation("计算用量金额(用量*单价)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "consumption",value = "用量",dataType = "String",dataTypeClass = String.class),
            @ApiImplicitParam(name = "unitprice",value = "单价",dataType = "String",dataTypeClass = String.class)
    })
    @GetMapping("/calculate")
    public R<String> calculate(String consumption, String unitprice){
//        Double con = new Double(consumption);
//        Double uni = new Double(unitprice);

        BigDecimal con = new BigDecimal(consumption);
        BigDecimal uni = new BigDecimal(unitprice);
//        Double i = con * uni;
        return R.ok(con.multiply(uni).toString());
//       return i.toString();
    }

    /**
     * 查询房屋缴费列表
     */
    @ApiOperation("房屋缴费列表")
    @ApiImplicitParam(name = "dbHousepayment",value = "房屋缴费",required = false,dataTypeClass = DbHousepayment.class)
    @PreAuthorize("@ss.hasPermi('pay:housepay:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbHousepayment dbHousepayment)
    {
        startPage();
        List<DbHousepayment> list = dbHousepaymentService.selectDbHousepaymentList(dbHousepayment);
        return getDataTable(list);
    }

    /**
     * 导出房屋缴费列表
     */
    @PreAuthorize("@ss.hasPermi('pay:housepay:export')")
    @Log(title = "房屋缴费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbHousepayment dbHousepayment)
    {
        List<DbHousepayment> list = dbHousepaymentService.selectDbHousepaymentList(dbHousepayment);
        ExcelUtil<DbHousepayment> util = new ExcelUtil<DbHousepayment>(DbHousepayment.class);
        util.exportExcel(response, list, "房屋缴费数据");
    }

    /**
     * 获取房屋缴费详细信息
     */
    @ApiOperation("房屋缴费详细信息")
    @ApiImplicitParam(name = "id",value = "房屋缴费id",dataTypeClass = Long.class)
//    @PreAuthorize("@ss.hasPermi('pay:housepay:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbHousepaymentService.selectDbHousepaymentById(id));
    }

    /**
     * 新增房屋缴费1
     */
    @PreAuthorize("@ss.hasPermi('pay:housepay:add')")
    @Log(title = "房屋缴费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbHousepayment dbHousepayment)
    {

        return toAjax(dbHousepaymentService.insertDbHousepayment(dbHousepayment));
    }

    /**
     * 修改房屋缴费
     */
    @PreAuthorize("@ss.hasPermi('pay:housepay:edit')")
    @Log(title = "房屋缴费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbHousepayment dbHousepayment)
    {
        return toAjax(dbHousepaymentService.updateDbHousepayment(dbHousepayment));
    }

    /**
     * 删除房屋缴费
     */
    @PreAuthorize("@ss.hasPermi('pay:housepay:remove')")
    @Log(title = "房屋缴费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbHousepaymentService.deleteDbHousepaymentByIds(ids));
    }
}
