package com.ruoyi.pay.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbCharging;
import com.ruoyi.pay.service.IDbChargingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 收费标准Controller
 * 
 * @author wlx
 * @date 2023-03-08
 */
@RestController
@RequestMapping("/pay/standard")
public class DbChargingController extends BaseController
{

    @Autowired
    private IDbChargingService dbChargingService;

    @GetMapping("/getByType")
    public DbCharging getByType(Long typeid){
        return dbChargingService.selectChargingByExptype(typeid);
    }

    /**
     * 查询收费标准列表
     */
    @PreAuthorize("@ss.hasPermi('pay:standard:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbCharging dbCharging)
    {
        startPage();
        List<DbCharging> list = dbChargingService.selectDbChargingList(dbCharging);
        return getDataTable(list);
    }

    /**
     * 导出收费标准列表
     */
    @PreAuthorize("@ss.hasPermi('pay:standard:export')")
    @Log(title = "收费标准", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbCharging dbCharging)
    {
        List<DbCharging> list = dbChargingService.selectDbChargingList(dbCharging);
        ExcelUtil<DbCharging> util = new ExcelUtil<DbCharging>(DbCharging.class);
        util.exportExcel(response, list, "收费标准数据");
    }

    /**
     * 获取收费标准详细信息
     */
    @PreAuthorize("@ss.hasPermi('pay:standard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbChargingService.selectDbChargingById(id));
    }

    /**
     * 新增收费标准
     */
    @PreAuthorize("@ss.hasPermi('pay:standard:add')")
    @Log(title = "收费标准", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbCharging dbCharging)
    {
        return toAjax(dbChargingService.insertDbCharging(dbCharging));
    }

    /**
     * 修改收费标准
     */
    @PreAuthorize("@ss.hasPermi('pay:standard:edit')")
    @Log(title = "收费标准", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbCharging dbCharging)
    {
        return toAjax(dbChargingService.updateDbCharging(dbCharging));
    }

    /**
     * 删除收费标准
     */
    @PreAuthorize("@ss.hasPermi('pay:standard:remove')")
    @Log(title = "收费标准", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbChargingService.deleteDbChargingByIds(ids));
    }
}
