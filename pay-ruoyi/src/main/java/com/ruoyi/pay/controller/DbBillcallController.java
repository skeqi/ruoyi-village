package com.ruoyi.pay.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbBillcall;
import com.ruoyi.pay.service.IDbBillcallService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 账单催缴Controller
 * 
 * @author wlx
 * @date 2023-03-14
 */
@RestController
@RequestMapping("/pay/call")
public class DbBillcallController extends BaseController
{
    @Autowired
    private IDbBillcallService dbBillcallService;

    /**
     * 查询账单催缴列表
     */
    @PreAuthorize("@ss.hasPermi('pay:call:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbBillcall dbBillcall)
    {
        startPage();
        List<DbBillcall> list = dbBillcallService.selectDbBillcallList(dbBillcall);
        return getDataTable(list);
    }

    /**
     * 导出账单催缴列表
     */
    @PreAuthorize("@ss.hasPermi('pay:call:export')")
    @Log(title = "账单催缴", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbBillcall dbBillcall)
    {
        List<DbBillcall> list = dbBillcallService.selectDbBillcallList(dbBillcall);
        ExcelUtil<DbBillcall> util = new ExcelUtil<DbBillcall>(DbBillcall.class);
        util.exportExcel(response, list, "账单催缴数据");
    }

    /**
     * 获取账单催缴详细信息
     */
    @PreAuthorize("@ss.hasPermi('pay:call:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbBillcallService.selectDbBillcallById(id));
    }

    /**
     * 新增账单催缴
     */
    @PreAuthorize("@ss.hasPermi('pay:call:add')")
    @Log(title = "账单催缴", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbBillcall dbBillcall)
    {
        return toAjax(dbBillcallService.insertDbBillcall(dbBillcall));
    }

    /**
     * 修改账单催缴
     */
    @PreAuthorize("@ss.hasPermi('pay:call:edit')")
    @Log(title = "账单催缴", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbBillcall dbBillcall)
    {
        return toAjax(dbBillcallService.updateDbBillcall(dbBillcall));
    }

    /**
     * 删除账单催缴
     */
    @PreAuthorize("@ss.hasPermi('pay:call:remove')")
    @Log(title = "账单催缴", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbBillcallService.deleteDbBillcallByIds(ids));
    }
}
