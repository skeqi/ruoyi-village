package com.ruoyi.pay.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbTemvehicleStandard;
import com.ruoyi.pay.service.IDbTemvehicleStandardService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 临时车收费标准Controller
 * 
 * @author wlx
 * @date 2023-03-09
 */
@Api(tags="缴费管理-临时车收费标准")
@RestController
@RequestMapping("/pay/temvehicle")
public class DbTemvehicleStandardController extends BaseController
{
    @Autowired
    private IDbTemvehicleStandardService dbTemvehicleStandardService;

    /**
     * 查询临时车收费标准列表
     */

    @PreAuthorize("@ss.hasPermi('pay:temvehicle:list')")
    @GetMapping("/list")
    @ApiOperation("列表")
    @ApiImplicitParam(name = "dbTemvehicleStandard",value = "收费标准",dataType = "DbTemvehicleStandard",dataTypeClass = DbTemvehicleStandard.class)
    public TableDataInfo list(DbTemvehicleStandard dbTemvehicleStandard)
    {
        startPage();
        List<DbTemvehicleStandard> list = dbTemvehicleStandardService.selectDbTemvehicleStandardList(dbTemvehicleStandard);
        return getDataTable(list);
    }

    /**
     * 导出临时车收费标准列表
     */
    @PreAuthorize("@ss.hasPermi('pay:temvehicle:export')")
    @Log(title = "临时车收费标准", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbTemvehicleStandard dbTemvehicleStandard)
    {
        List<DbTemvehicleStandard> list = dbTemvehicleStandardService.selectDbTemvehicleStandardList(dbTemvehicleStandard);
        ExcelUtil<DbTemvehicleStandard> util = new ExcelUtil<DbTemvehicleStandard>(DbTemvehicleStandard.class);
        util.exportExcel(response, list, "临时车收费标准数据");
    }

    /**
     * 获取临时车收费标准详细信息
     */
    @ApiOperation("根据id,获取详细信息")
    @ApiImplicitParam(name = "id",value = "收费标准id",dataType = "Long",dataTypeClass = Long.class)
    @PreAuthorize("@ss.hasPermi('pay:temvehicle:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbTemvehicleStandardService.selectDbTemvehicleStandardById(id));
    }

    /**
     * 新增临时车收费标准
     */
    @PreAuthorize("@ss.hasPermi('pay:temvehicle:add')")
    @Log(title = "临时车收费标准", businessType = BusinessType.INSERT)
    @ApiOperation("添加收费标准")
    @ApiImplicitParam(name = "dbTemvehicleStandard",value = "收费标准",dataType = "dbTemvehicleStandard",dataTypeClass = DbTemvehicleStandard.class)
    @PostMapping
    public AjaxResult add(@RequestBody DbTemvehicleStandard dbTemvehicleStandard)
    {
        return toAjax(dbTemvehicleStandardService.insertDbTemvehicleStandard(dbTemvehicleStandard));
    }

    /**
     * 修改临时车收费标准
     */
    @PreAuthorize("@ss.hasPermi('pay:temvehicle:edit')")
    @Log(title = "临时车收费标准", businessType = BusinessType.UPDATE)
    @ApiOperation("修改收费标准")
    @ApiImplicitParam(name = "dbTemvehicleStandard",value = "收费标准",dataType = "dbTemvehicleStandard",dataTypeClass = DbTemvehicleStandard.class)
    @PutMapping
    public AjaxResult edit(@RequestBody DbTemvehicleStandard dbTemvehicleStandard)
    {
        return toAjax(dbTemvehicleStandardService.updateDbTemvehicleStandard(dbTemvehicleStandard));
    }

    /**
     * 删除临时车收费标准
     */
    @PreAuthorize("@ss.hasPermi('pay:temvehicle:remove')")
    @Log(title = "临时车收费标准", businessType = BusinessType.DELETE)
    @ApiOperation("删除收费标准")
    @ApiImplicitParam(name = "ids",value = "收费标准数组id",dataType = "Long[]",dataTypeClass = Long[].class)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbTemvehicleStandardService.deleteDbTemvehicleStandardByIds(ids));
    }
}
