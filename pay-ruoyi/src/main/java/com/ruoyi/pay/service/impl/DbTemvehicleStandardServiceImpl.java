package com.ruoyi.pay.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pay.mapper.DbTemvehicleStandardMapper;
import com.ruoyi.domain.DbTemvehicleStandard;
import com.ruoyi.pay.service.IDbTemvehicleStandardService;

/**
 * 临时车收费标准Service业务层处理
 * 
 * @author wlx
 * @date 2023-03-09
 */
@Service
public class DbTemvehicleStandardServiceImpl implements IDbTemvehicleStandardService 
{
    @Autowired
    private DbTemvehicleStandardMapper dbTemvehicleStandardMapper;

    /**
     * 查询临时车收费标准
     * 
     * @param id 临时车收费标准主键
     * @return 临时车收费标准
     */
    @Override
    public DbTemvehicleStandard selectDbTemvehicleStandardById(Long id)
    {
        return dbTemvehicleStandardMapper.selectDbTemvehicleStandardById(id);
    }

    /**
     * 查询临时车收费标准列表
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 临时车收费标准
     */
    @Override
    public List<DbTemvehicleStandard> selectDbTemvehicleStandardList(DbTemvehicleStandard dbTemvehicleStandard)
    {
        return dbTemvehicleStandardMapper.selectDbTemvehicleStandardList(dbTemvehicleStandard);
    }

    /**
     * 新增临时车收费标准
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 结果
     */
    @Override
    public int insertDbTemvehicleStandard(DbTemvehicleStandard dbTemvehicleStandard)
    {
        return dbTemvehicleStandardMapper.insertDbTemvehicleStandard(dbTemvehicleStandard);
    }

    /**
     * 修改临时车收费标准
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 结果
     */
    @Override
    public int updateDbTemvehicleStandard(DbTemvehicleStandard dbTemvehicleStandard)
    {
        return dbTemvehicleStandardMapper.updateDbTemvehicleStandard(dbTemvehicleStandard);
    }

    /**
     * 批量删除临时车收费标准
     * 
     * @param ids 需要删除的临时车收费标准主键
     * @return 结果
     */
    @Override
    public int deleteDbTemvehicleStandardByIds(Long[] ids)
    {
        return dbTemvehicleStandardMapper.deleteDbTemvehicleStandardByIds(ids);
    }

    /**
     * 删除临时车收费标准信息
     * 
     * @param id 临时车收费标准主键
     * @return 结果
     */
    @Override
    public int deleteDbTemvehicleStandardById(Long id)
    {
        return dbTemvehicleStandardMapper.deleteDbTemvehicleStandardById(id);
    }
}
