package com.ruoyi.pay.service.impl;

import java.security.acl.Owner;
import java.util.List;

import com.ruoyi.domain.DbOwner;

import com.ruoyi.owner.mapper.DbOwnerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pay.mapper.DbHousepaymentMapper;
import com.ruoyi.domain.DbHousepayment;
import com.ruoyi.pay.service.IDbHousepaymentService;

/**
 * 房屋缴费Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
@Service
public class DbHousepaymentServiceImpl implements IDbHousepaymentService 
{
    @Autowired
    private DbHousepaymentMapper dbHousepaymentMapper;

    @Autowired
    private DbOwnerMapper dbOwnerMapper;
    /**
     * 查询房屋缴费
     * 
     * @param id 房屋缴费主键
     * @return 房屋缴费
     */
    @Override
    public DbHousepayment selectDbHousepaymentById(Long id)
    {
        return dbHousepaymentMapper.selectDbHousepaymentById(id);
    }

    /**
     * 查询房屋缴费列表
     * 
     * @param dbHousepayment 房屋缴费
     * @return 房屋缴费
     */
    @Override
    public List<DbHousepayment> selectDbHousepaymentList(DbHousepayment dbHousepayment)
    {
        return dbHousepaymentMapper.selectDbHousepaymentList(dbHousepayment);
    }

    /**
     * 新增房屋缴费
     * 
     * @param dbHousepayment 房屋缴费
     * @return 结果
     */
    @Override
    public int insertDbHousepayment(DbHousepayment dbHousepayment)
    {
        //查询房屋的绑定业主

        DbOwner owner = dbOwnerMapper.selectOwnerByUnit(dbHousepayment.getHouId());
        dbHousepayment.setOwnerId(owner.getId());
        return dbHousepaymentMapper.insertDbHousepayment(dbHousepayment);
    }

    /**
     * 修改房屋缴费
     * 
     * @param dbHousepayment 房屋缴费
     * @return 结果
     */
    @Override
    public int updateDbHousepayment(DbHousepayment dbHousepayment)
    {
        return dbHousepaymentMapper.updateDbHousepayment(dbHousepayment);
    }

    /**
     * 批量删除房屋缴费
     * 
     * @param ids 需要删除的房屋缴费主键
     * @return 结果
     */
    @Override
    public int deleteDbHousepaymentByIds(Long[] ids)
    {
        return dbHousepaymentMapper.deleteDbHousepaymentByIds(ids);
    }

    /**
     * 删除房屋缴费信息
     * 
     * @param id 房屋缴费主键
     * @return 结果
     */
    @Override
    public int deleteDbHousepaymentById(Long id)
    {
        return dbHousepaymentMapper.deleteDbHousepaymentById(id);
    }
}
