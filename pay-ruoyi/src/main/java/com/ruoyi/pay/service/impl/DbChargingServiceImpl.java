package com.ruoyi.pay.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pay.mapper.DbChargingMapper;
import com.ruoyi.domain.DbCharging;
import com.ruoyi.pay.service.IDbChargingService;

/**
 * 收费标准Service业务层处理
 * 
 * @author wlx
 * @date 2023-03-08
 */
@Service
public class DbChargingServiceImpl implements IDbChargingService 
{
    @Autowired
    private DbChargingMapper dbChargingMapper;

    @Override
    public DbCharging selectChargingByExptype(Long exptype) {
        return dbChargingMapper.selectChargingByExptype(exptype);
    }

    /**
     * 查询收费标准
     * 
     * @param id 收费标准主键
     * @return 收费标准
     */
    @Override
    public DbCharging selectDbChargingById(Long id)
    {
        return dbChargingMapper.selectDbChargingById(id);
    }

    /**
     * 查询收费标准列表
     * 
     * @param dbCharging 收费标准
     * @return 收费标准
     */
    @Override
    public List<DbCharging> selectDbChargingList(DbCharging dbCharging)
    {
        return dbChargingMapper.selectDbChargingList(dbCharging);
    }

    /**
     * 新增收费标准
     * 
     * @param dbCharging 收费标准
     * @return 结果
     */
    @Override
    public int insertDbCharging(DbCharging dbCharging)
    {
        return dbChargingMapper.insertDbCharging(dbCharging);
    }

    /**
     * 修改收费标准
     * 
     * @param dbCharging 收费标准
     * @return 结果
     */
    @Override
    public int updateDbCharging(DbCharging dbCharging)
    {
        return dbChargingMapper.updateDbCharging(dbCharging);
    }

    /**
     * 批量删除收费标准
     * 
     * @param ids 需要删除的收费标准主键
     * @return 结果
     */
    @Override
    public int deleteDbChargingByIds(Long[] ids)
    {
        return dbChargingMapper.deleteDbChargingByIds(ids);
    }

    /**
     * 删除收费标准信息
     * 
     * @param id 收费标准主键
     * @return 结果
     */
    @Override
    public int deleteDbChargingById(Long id)
    {
        return dbChargingMapper.deleteDbChargingById(id);
    }
}
