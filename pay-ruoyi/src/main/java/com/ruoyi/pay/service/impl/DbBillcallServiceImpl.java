package com.ruoyi.pay.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pay.mapper.DbBillcallMapper;
import com.ruoyi.domain.DbBillcall;
import com.ruoyi.pay.service.IDbBillcallService;

/**
 * 账单催缴Service业务层处理
 * 
 * @author wlx
 * @date 2023-03-14
 */
@Service
public class DbBillcallServiceImpl implements IDbBillcallService 
{
    @Autowired
    private DbBillcallMapper dbBillcallMapper;

    /**
     * 查询账单催缴
     * 
     * @param id 账单催缴主键
     * @return 账单催缴
     */
    @Override
    public DbBillcall selectDbBillcallById(Long id)
    {
        return dbBillcallMapper.selectDbBillcallById(id);
    }

    /**
     * 查询账单催缴列表
     * 
     * @param dbBillcall 账单催缴
     * @return 账单催缴
     */
    @Override
    public List<DbBillcall> selectDbBillcallList(DbBillcall dbBillcall)
    {
        return dbBillcallMapper.selectDbBillcallList(dbBillcall);
    }

    /**
     * 新增账单催缴
     * 
     * @param dbBillcall 账单催缴
     * @return 结果
     */
    @Override
    public int insertDbBillcall(DbBillcall dbBillcall)
    {
        return dbBillcallMapper.insertDbBillcall(dbBillcall);
    }

    /**
     * 修改账单催缴
     * 
     * @param dbBillcall 账单催缴
     * @return 结果
     */
    @Override
    public int updateDbBillcall(DbBillcall dbBillcall)
    {
        return dbBillcallMapper.updateDbBillcall(dbBillcall);
    }

    /**
     * 批量删除账单催缴
     * 
     * @param ids 需要删除的账单催缴主键
     * @return 结果
     */
    @Override
    public int deleteDbBillcallByIds(Long[] ids)
    {
        return dbBillcallMapper.deleteDbBillcallByIds(ids);
    }

    /**
     * 删除账单催缴信息
     * 
     * @param id 账单催缴主键
     * @return 结果
     */
    @Override
    public int deleteDbBillcallById(Long id)
    {
        return dbBillcallMapper.deleteDbBillcallById(id);
    }
}
