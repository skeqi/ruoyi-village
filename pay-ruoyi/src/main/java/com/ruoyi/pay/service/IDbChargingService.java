package com.ruoyi.pay.service;

import java.util.List;
import com.ruoyi.domain.DbCharging;
import org.apache.ibatis.annotations.Param;

/**
 * 收费标准Service接口
 * 
 * @author wlx
 * @date 2023-03-08
 */
public interface IDbChargingService 
{

    public DbCharging selectChargingByExptype( Long exptype);

    /**
     * 查询收费标准
     * 
     * @param id 收费标准主键
     * @return 收费标准
     */
    public DbCharging selectDbChargingById(Long id);

    /**
     * 查询收费标准列表
     * 
     * @param dbCharging 收费标准
     * @return 收费标准集合
     */
    public List<DbCharging> selectDbChargingList(DbCharging dbCharging);

    /**
     * 新增收费标准
     * 
     * @param dbCharging 收费标准
     * @return 结果
     */
    public int insertDbCharging(DbCharging dbCharging);

    /**
     * 修改收费标准
     * 
     * @param dbCharging 收费标准
     * @return 结果
     */
    public int updateDbCharging(DbCharging dbCharging);

    /**
     * 批量删除收费标准
     * 
     * @param ids 需要删除的收费标准主键集合
     * @return 结果
     */
    public int deleteDbChargingByIds(Long[] ids);

    /**
     * 删除收费标准信息
     * 
     * @param id 收费标准主键
     * @return 结果
     */
    public int deleteDbChargingById(Long id);
}
