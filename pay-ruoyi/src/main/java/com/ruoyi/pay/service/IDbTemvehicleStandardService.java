package com.ruoyi.pay.service;

import java.util.List;
import com.ruoyi.domain.DbTemvehicleStandard;

/**
 * 临时车收费标准Service接口
 * 
 * @author wlx
 * @date 2023-03-09
 */
public interface IDbTemvehicleStandardService 
{
    /**
     * 查询临时车收费标准
     * 
     * @param id 临时车收费标准主键
     * @return 临时车收费标准
     */
    public DbTemvehicleStandard selectDbTemvehicleStandardById(Long id);

    /**
     * 查询临时车收费标准列表
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 临时车收费标准集合
     */
    public List<DbTemvehicleStandard> selectDbTemvehicleStandardList(DbTemvehicleStandard dbTemvehicleStandard);

    /**
     * 新增临时车收费标准
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 结果
     */
    public int insertDbTemvehicleStandard(DbTemvehicleStandard dbTemvehicleStandard);

    /**
     * 修改临时车收费标准
     * 
     * @param dbTemvehicleStandard 临时车收费标准
     * @return 结果
     */
    public int updateDbTemvehicleStandard(DbTemvehicleStandard dbTemvehicleStandard);

    /**
     * 批量删除临时车收费标准
     * 
     * @param ids 需要删除的临时车收费标准主键集合
     * @return 结果
     */
    public int deleteDbTemvehicleStandardByIds(Long[] ids);

    /**
     * 删除临时车收费标准信息
     * 
     * @param id 临时车收费标准主键
     * @return 结果
     */
    public int deleteDbTemvehicleStandardById(Long id);
}
