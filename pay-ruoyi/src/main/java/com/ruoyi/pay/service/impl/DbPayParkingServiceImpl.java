package com.ruoyi.pay.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pay.mapper.DbPayParkingMapper;
import com.ruoyi.domain.DbPayParking;
import com.ruoyi.pay.service.IDbPayParkingService;

/**
 * 临时车缴费Service业务层处理
 * 
 * @author wlx
 * @date 2023-03-09
 */
@Service
public class DbPayParkingServiceImpl implements IDbPayParkingService 
{
    @Autowired
    private DbPayParkingMapper dbPayParkingMapper;

    /**
     * 查询临时车缴费
     * 
     * @param id 临时车缴费主键
     * @return 临时车缴费
     */
    @Override
    public DbPayParking selectDbPayParkingById(Long id)
    {
        return dbPayParkingMapper.selectDbPayParkingById(id);
    }

    /**
     * 查询临时车缴费列表
     * 
     * @param dbPayParking 临时车缴费
     * @return 临时车缴费
     */
    @Override
    public List<DbPayParking> selectDbPayParkingList(DbPayParking dbPayParking)
    {
        return dbPayParkingMapper.selectDbPayParkingList(dbPayParking);
    }

    /**
     * 新增临时车缴费
     * 
     * @param dbPayParking 临时车缴费
     * @return 结果
     */
    @Override
    public int insertDbPayParking(DbPayParking dbPayParking)
    {
        return dbPayParkingMapper.insertDbPayParking(dbPayParking);
    }

    /**
     * 修改临时车缴费
     * 
     * @param dbPayParking 临时车缴费
     * @return 结果
     */
    @Override
    public int updateDbPayParking(DbPayParking dbPayParking)
    {
        return dbPayParkingMapper.updateDbPayParking(dbPayParking);
    }

    /**
     * 批量删除临时车缴费
     * 
     * @param ids 需要删除的临时车缴费主键
     * @return 结果
     */
    @Override
    public int deleteDbPayParkingByIds(Long[] ids)
    {
        return dbPayParkingMapper.deleteDbPayParkingByIds(ids);
    }

    /**
     * 删除临时车缴费信息
     * 
     * @param id 临时车缴费主键
     * @return 结果
     */
    @Override
    public int deleteDbPayParkingById(Long id)
    {
        return dbPayParkingMapper.deleteDbPayParkingById(id);
    }
}
