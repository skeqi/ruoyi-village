package com.ruoyi.owner.mapper;

import java.util.List;
import com.ruoyi.domain.*;
/**
 * 业主信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
public interface DbOwnerMapper 
{
    /**
     * 查询业主信息
     *
     * @param unit 房屋id
     * @return 业主信息
     */
    public DbOwner selectOwnerByUnit(Long unit);

    /**
     * 查询业主信息
     * 
     * @param id 业主信息主键
     * @return 业主信息
     */
    public DbOwner selectDbOwnerById(Long id);

    /**
     * 查询业主信息列表
     * 
     * @param dbOwner 业主信息
     * @return 业主信息集合
     */
    public List<DbOwner> selectDbOwnerList(DbOwner dbOwner);

    /**
     * 新增业主信息
     * 
     * @param dbOwner 业主信息
     * @return 结果
     */
    public int insertDbOwner(DbOwner dbOwner);

    /**
     * 修改业主信息
     * 
     * @param dbOwner 业主信息
     * @return 结果
     */
    public int updateDbOwner(DbOwner dbOwner);

    /**
     * 删除业主信息
     * 
     * @param id 业主信息主键
     * @return 结果
     */
    public int deleteDbOwnerById(Long id);

    /**
     * 批量删除业主信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbOwnerByIds(Long[] ids);

    /*
    房屋号绑定
     */
    public List<DbHouse> findhouse();
}
