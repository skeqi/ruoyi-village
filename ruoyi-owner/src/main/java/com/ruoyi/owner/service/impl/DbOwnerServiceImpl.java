package com.ruoyi.owner.service.impl;

import java.util.List;

import com.ruoyi.domain.DbHouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.owner.mapper.DbOwnerMapper;
import com.ruoyi.domain.*;
import com.ruoyi.owner.service.IDbOwnerService;

/**
 * 业主信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class DbOwnerServiceImpl implements IDbOwnerService 
{
    @Autowired
    private DbOwnerMapper dbOwnerMapper;

    /**
     * 查询业主信息
     * 
     * @param id 业主信息主键
     * @return 业主信息
     */
    @Override
    public DbOwner selectDbOwnerById(Long id)
    {
        return dbOwnerMapper.selectDbOwnerById(id);
    }

    /**
     * 查询业主信息列表
     * 
     * @param dbOwner 业主信息
     * @return 业主信息
     */
    @Override
    public List<DbOwner> selectDbOwnerList(DbOwner dbOwner)
    {
        return dbOwnerMapper.selectDbOwnerList(dbOwner);
    }

    /**
     * 新增业主信息
     * 
     * @param dbOwner 业主信息
     * @return 结果
     */
    @Override
    public int insertDbOwner(DbOwner dbOwner)
    {
        return dbOwnerMapper.insertDbOwner(dbOwner);
    }

    /**
     * 修改业主信息
     * 
     * @param dbOwner 业主信息
     * @return 结果
     */
    @Override
    public int updateDbOwner(DbOwner dbOwner)
    {
        return dbOwnerMapper.updateDbOwner(dbOwner);
    }

    /**
     * 批量删除业主信息
     * 
     * @param ids 需要删除的业主信息主键
     * @return 结果
     */
    @Override
    public int deleteDbOwnerByIds(Long[] ids)
    {
        return dbOwnerMapper.deleteDbOwnerByIds(ids);
    }

    /**
     * 删除业主信息信息
     * 
     * @param id 业主信息主键
     * @return 结果
     */
    @Override
    public int deleteDbOwnerById(Long id)
    {
        return dbOwnerMapper.deleteDbOwnerById(id);
    }

    @Override
    public List<DbHouse> findhouse() {
        return dbOwnerMapper.findhouse();
    }
}
