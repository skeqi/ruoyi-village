package com.ruoyi.owner.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbHouse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.*;
import com.ruoyi.owner.service.IDbOwnerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主信息Controller
 * 
 * @author ruoyi
 * @date 2023-03-23
 */
@RestController
@RequestMapping("/owner/owner")
public class DbOwnerController extends BaseController
{
    @Autowired
    private IDbOwnerService dbOwnerService;

    /**
     * 查询业主信息列表
     */
    @PreAuthorize("@ss.hasPermi('owner:owner:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbOwner dbOwner)
    {
        startPage();
        List<DbOwner> list = dbOwnerService.selectDbOwnerList(dbOwner);
        return getDataTable(list);
    }

    /**
     * 导出业主信息列表
     */
    @PreAuthorize("@ss.hasPermi('owner:owner:export')")
    @Log(title = "业主信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbOwner dbOwner)
    {
        List<DbOwner> list = dbOwnerService.selectDbOwnerList(dbOwner);
        ExcelUtil<DbOwner> util = new ExcelUtil<DbOwner>(DbOwner.class);
        util.exportExcel(response, list, "业主信息数据");
    }

    /**
     * 获取业主信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('owner:owner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbOwnerService.selectDbOwnerById(id));
    }

    /**
     * 新增业主信息
     */
    @PreAuthorize("@ss.hasPermi('owner:owner:add')")
    @Log(title = "业主信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbOwner dbOwner)
    {
        return toAjax(dbOwnerService.insertDbOwner(dbOwner));
    }

    /**
     * 修改业主信息
     */
    @PreAuthorize("@ss.hasPermi('owner:owner:edit')")
    @Log(title = "业主信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbOwner dbOwner)
    {
        return toAjax(dbOwnerService.updateDbOwner(dbOwner));
    }

    /**
     * 删除业主信息
     */
    @PreAuthorize("@ss.hasPermi('owner:owner:remove')")
    @Log(title = "业主信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbOwnerService.deleteDbOwnerByIds(ids));
    }

    @GetMapping("/findho")
    public List<DbHouse> findhouse(){return dbOwnerService.findhouse();}
}
