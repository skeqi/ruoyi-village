package com.ruoyi.house.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbHouse;

import com.ruoyi.domain.DbBuilding;
import com.ruoyi.domain.DbFloor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbHouseType;

import com.ruoyi.house.service.IDbHouseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 房屋信息Controller
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/house/house")
public class DbHouseController extends BaseController
{
    @Autowired
    private IDbHouseService dbHouseService;

    /**
     * 查询房屋信息列表
     */
    @PreAuthorize("@ss.hasPermi('house:house:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbHouse dbHouse)
    {
        startPage();
        List<DbHouse> list = dbHouseService.selectDbHouseList(dbHouse);
        return getDataTable(list);
    }

    /**
     * 导出房屋信息列表
     */
    @PreAuthorize("@ss.hasPermi('house:house:export')")
    @Log(title = "房屋信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbHouse dbHouse)
    {
        List<DbHouse> list = dbHouseService.selectDbHouseList(dbHouse);
        ExcelUtil<DbHouse> util = new ExcelUtil<DbHouse>(DbHouse.class);
        util.exportExcel(response, list, "房屋信息数据");
    }

    @GetMapping("/buiDing")
    public List<DbBuilding> buiDing(){
        return dbHouseService.findBuil();
    }

    @GetMapping("/floor")
    public List<DbFloor> floor(){
        return dbHouseService.findFloor();
    }

    /**
     * 获取房屋信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('house:house:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbHouseService.selectDbHouseById(id));
    }

    /**
     * 新增房屋信息
     */
    @PreAuthorize("@ss.hasPermi('house:house:add')")
    @Log(title = "房屋信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbHouse dbHouse)
    {
        return toAjax(dbHouseService.insertDbHouse(dbHouse));
    }

    /**
     * 修改房屋信息
     */
    @PreAuthorize("@ss.hasPermi('house:house:edit')")
    @Log(title = "房屋信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbHouse dbHouse)
    {
        return toAjax(dbHouseService.updateDbHouse(dbHouse));
    }

    /**
     * 删除房屋信息
     */
    @PreAuthorize("@ss.hasPermi('house:house:remove')")
    @Log(title = "房屋信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbHouseService.deleteDbHouseByIds(ids));
    }
}
