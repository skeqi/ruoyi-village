package com.ruoyi.house.service.impl;

import java.util.List;

import com.ruoyi.domain.DbBuilding;
import com.ruoyi.domain.DbFloor;
import com.ruoyi.domain.DbHouseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.house.mapper.DbHouseMapper;
import com.ruoyi.domain.DbHouse;
import com.ruoyi.house.service.IDbHouseService;

/**
 * 房屋信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class DbHouseServiceImpl implements IDbHouseService 
{
    @Autowired
    private DbHouseMapper dbHouseMapper;

    /**
     * 查询房屋信息
     * 
     * @param id 房屋信息主键
     * @return 房屋信息
     */
    @Override
    public DbHouse selectDbHouseById(Long id)
    {
        return dbHouseMapper.selectDbHouseById(id);
    }

    /**
     * 查询房屋信息列表
     * 
     * @param dbHouse 房屋信息
     * @return 房屋信息
     */
    @Override
    public List<DbHouse> selectDbHouseList(DbHouse dbHouse)
    {
        return dbHouseMapper.selectDbHouseList(dbHouse);
    }

    /**
     * 新增房屋信息
     * 
     * @param dbHouse 房屋信息
     * @return 结果
     */
    @Override
    public int insertDbHouse(DbHouse dbHouse)
    {
        return dbHouseMapper.insertDbHouse(dbHouse);
    }

    /**
     * 修改房屋信息
     * 
     * @param dbHouse 房屋信息
     * @return 结果
     */
    @Override
    public int updateDbHouse(DbHouse dbHouse)
    {
        return dbHouseMapper.updateDbHouse(dbHouse);
    }

    /**
     * 批量删除房屋信息
     * 
     * @param ids 需要删除的房屋信息主键
     * @return 结果
     */
    @Override
    public int deleteDbHouseByIds(Long[] ids)
    {
        return dbHouseMapper.deleteDbHouseByIds(ids);
    }

    /**
     * 删除房屋信息信息
     * 
     * @param id 房屋信息主键
     * @return 结果
     */
    @Override
    public int deleteDbHouseById(Long id)
    {
        return dbHouseMapper.deleteDbHouseById(id);
    }

    @Override
    public List<DbHouseType> findType() {
        return dbHouseMapper.findType();
    }

    @Override
    public List<DbBuilding> findBuil() {
        return dbHouseMapper.findBuil();
    }

    @Override
    public List<DbFloor> findFloor() {
        return dbHouseMapper.findFloor();
    }

}
