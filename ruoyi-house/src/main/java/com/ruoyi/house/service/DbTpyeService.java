package com.ruoyi.house.service;

import com.ruoyi.domain.DbHouseType;


import java.util.List;

public interface DbTpyeService {
    public List<DbHouseType> findAll();
}
