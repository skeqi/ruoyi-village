package com.ruoyi.house.mapper;

import java.util.List;

import com.ruoyi.domain.DbHouse;
import com.ruoyi.domain.DbHouseType;


import com.ruoyi.domain.DbBuilding;
import com.ruoyi.domain.DbFloor;

import com.ruoyi.domain.DbHouseType;

/**
 * 房屋信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public interface DbHouseMapper
{
    /**
     * 查询房屋信息
     * 
     * @param id 房屋信息主键
     * @return 房屋信息
     */
    public DbHouse selectDbHouseById(Long id);

    /**
     * 查询房屋信息列表
     * 
     * @param dbHouse 房屋信息
     * @return 房屋信息集合
     */
    public List<DbHouse> selectDbHouseList(DbHouse dbHouse);

    /**
     * 新增房屋信息
     * 
     * @param dbHouse 房屋信息
     * @return 结果
     */
    public int insertDbHouse(DbHouse dbHouse);

    /**
     * 修改房屋信息
     * 
     * @param dbHouse 房屋信息
     * @return 结果
     */
    public int updateDbHouse(DbHouse dbHouse);

    /**
     * 删除房屋信息
     * 
     * @param id 房屋信息主键
     * @return 结果
     */
    public int deleteDbHouseById(Long id);

    /**
     * 批量删除房屋信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbHouseByIds(Long[] ids);

    /*
    *房屋类型
     */
    public List<DbHouseType> findType();

    public List<DbBuilding> findBuil();

    public List<DbFloor> findFloor();

}
