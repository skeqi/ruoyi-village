package com.ruoyi.house.mapper;

import com.ruoyi.domain.DbHouseType;

import java.util.List;

public interface DbTypeMapper {
    /**
     * 查询户型管理
     *
     * @param id 户型管理主键
     * @return 户型管理
     */
    public DbHouseType selectDbHousetypeById(Long id);

    List<DbHouseType> findAll();
}
