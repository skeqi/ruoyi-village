package com.ruoyi.building.mapper;

import java.util.List;
import com.ruoyi.domain.*;
/**
 * 楼栋管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public interface DbBuildingMapper 
{
    /**
     * 查询楼栋管理
     * 
     * @param buidingid 楼栋管理主键
     * @return 楼栋管理
     */
    public DbBuilding selectDbBuildingByBuidingid(Long buidingid);

    /**
     * 查询楼栋管理列表
     * 
     * @param dbBuilding 楼栋管理
     * @return 楼栋管理集合
     */
    public List<DbBuilding> selectDbBuildingList(DbBuilding dbBuilding);

    /**
     * 新增楼栋管理
     * 
     * @param dbBuilding 楼栋管理
     * @return 结果
     */
    public int insertDbBuilding(DbBuilding dbBuilding);

    /**
     * 修改楼栋管理
     * 
     * @param dbBuilding 楼栋管理
     * @return 结果
     */
    public int updateDbBuilding(DbBuilding dbBuilding);

    /**
     * 删除楼栋管理
     * 
     * @param buidingid 楼栋管理主键
     * @return 结果
     */
    public int deleteDbBuildingByBuidingid(Long buidingid);

    /**
     * 批量删除楼栋管理
     * 
     * @param buidingids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbBuildingByBuidingids(Long[] buidingids);

    public List<StaffUser> findAnBao();

    public List<StaffUser> findFuWu();
}
