package com.ruoyi.building.service.impl;

import java.util.List;

import com.ruoyi.domain.DbBuilding;
import com.ruoyi.domain.StaffUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.building.mapper.DbBuildingMapper;
import com.ruoyi.building.service.IDbBuildingService;
import com.ruoyi.domain.*;

/**
 * 楼栋管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
@Service
public class DbBuildingServiceImpl implements IDbBuildingService 
{
    @Autowired
    private DbBuildingMapper dbBuildingMapper;

    /**
     * 查询楼栋管理
     * 
     * @param buidingid 楼栋管理主键
     * @return 楼栋管理
     */
    @Override
    public DbBuilding selectDbBuildingByBuidingid(Long buidingid)
    {
        return dbBuildingMapper.selectDbBuildingByBuidingid(buidingid);
    }

    /**
     * 查询楼栋管理列表
     * 
     * @param dbBuilding 楼栋管理
     * @return 楼栋管理
     */
    @Override
    public List<DbBuilding> selectDbBuildingList(DbBuilding dbBuilding)
    {
        return dbBuildingMapper.selectDbBuildingList(dbBuilding);
    }

    /**
     * 新增楼栋管理
     * 
     * @param dbBuilding 楼栋管理
     * @return 结果
     */
    @Override
    public int insertDbBuilding(DbBuilding dbBuilding)
    {
        return dbBuildingMapper.insertDbBuilding(dbBuilding);
    }

    /**
     * 修改楼栋管理
     * 
     * @param dbBuilding 楼栋管理
     * @return 结果
     */
    @Override
    public int updateDbBuilding(DbBuilding dbBuilding)
    {
        return dbBuildingMapper.updateDbBuilding(dbBuilding);
    }

    /**
     * 批量删除楼栋管理
     * 
     * @param buidingids 需要删除的楼栋管理主键
     * @return 结果
     */
    @Override
    public int deleteDbBuildingByBuidingids(Long[] buidingids)
    {
        return dbBuildingMapper.deleteDbBuildingByBuidingids(buidingids);
    }

    /**
     * 删除楼栋管理信息
     * 
     * @param buidingid 楼栋管理主键
     * @return 结果
     */
    @Override
    public int deleteDbBuildingByBuidingid(Long buidingid)
    {
        return dbBuildingMapper.deleteDbBuildingByBuidingid(buidingid);
    }

    @Override
    public List<StaffUser> findAnBao() {
        return dbBuildingMapper.findAnBao();
    }

    @Override
    public List<StaffUser> findFuWu() {
        return dbBuildingMapper.findFuWu();
    }
}
