package com.ruoyi.building.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbBuilding;
import com.ruoyi.domain.StaffUser;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.building.service.IDbBuildingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.domain.*;

/**
 * 楼栋管理Controller
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/building/building")
public class DbBuildingController extends BaseController
{
    @Autowired
    private IDbBuildingService dbBuildingService;

    /**
     * 查询楼栋管理列表
     */
    @PreAuthorize("@ss.hasPermi('building:building:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbBuilding dbBuilding)
    {
        startPage();
        List<DbBuilding> list = dbBuildingService.selectDbBuildingList(dbBuilding);
        return getDataTable(list);
    }

    /**
     * 导出楼栋管理列表
     */
    @PreAuthorize("@ss.hasPermi('building:building:export')")
    @Log(title = "楼栋管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbBuilding dbBuilding)
    {
        List<DbBuilding> list = dbBuildingService.selectDbBuildingList(dbBuilding);
        ExcelUtil<DbBuilding> util = new ExcelUtil<DbBuilding>(DbBuilding.class);
        util.exportExcel(response, list, "楼栋管理数据");
    }

    /**
     * 获取楼栋管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('building:building:query')")
    @GetMapping(value = "/{buidingid}")
    public AjaxResult getInfo(@PathVariable("buidingid") Long buidingid)
    {
        return success(dbBuildingService.selectDbBuildingByBuidingid(buidingid));
    }

    @GetMapping("/AnBao")
    public List<StaffUser> getAnBao(){
        return dbBuildingService.findAnBao();
    }

    @GetMapping("/FuWu")
    public List<StaffUser> getFuWu(){
        return dbBuildingService.findFuWu();
    }

    /**
     * 新增楼栋管理
     */
    @PreAuthorize("@ss.hasPermi('building:building:add')")
    @Log(title = "楼栋管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbBuilding dbBuilding)
    {
        return toAjax(dbBuildingService.insertDbBuilding(dbBuilding));
    }

    /**
     * 修改楼栋管理
     */
    @PreAuthorize("@ss.hasPermi('building:building:edit')")
    @Log(title = "楼栋管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbBuilding dbBuilding)
    {
        return toAjax(dbBuildingService.updateDbBuilding(dbBuilding));
    }

    /**
     * 删除楼栋管理
     */
    @PreAuthorize("@ss.hasPermi('building:building:remove')")
    @Log(title = "楼栋管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{buidingids}")
    public AjaxResult remove(@PathVariable Long[] buidingids)
    {
        return toAjax(dbBuildingService.deleteDbBuildingByBuidingids(buidingids));
    }
}
