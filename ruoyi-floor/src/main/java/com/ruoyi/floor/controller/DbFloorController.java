package com.ruoyi.floor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbFloor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.floor.service.IDbFloorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 楼层管理Controller
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
@RestController
@RequestMapping("/floor/floor")
public class DbFloorController extends BaseController
{
    @Autowired
    private IDbFloorService dbFloorService;

    /**
     * 查询楼层管理列表
     */
    @PreAuthorize("@ss.hasPermi('floor:floor:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbFloor dbFloor)
    {
        startPage();
        List<DbFloor> list = dbFloorService.selectDbFloorList(dbFloor);
        return getDataTable(list);
    }

    /**
     * 导出楼层管理列表
     */
    @PreAuthorize("@ss.hasPermi('floor:floor:export')")
    @Log(title = "楼层管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbFloor dbFloor)
    {
        List<DbFloor> list = dbFloorService.selectDbFloorList(dbFloor);
        ExcelUtil<DbFloor> util = new ExcelUtil<DbFloor>(DbFloor.class);
        util.exportExcel(response, list, "楼层管理数据");
    }

    /**
     * 获取楼层管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('floor:floor:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbFloorService.selectDbFloorById(id));
    }

    /**
     * 新增楼层管理
     */
    @PreAuthorize("@ss.hasPermi('floor:floor:add')")
    @Log(title = "楼层管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbFloor dbFloor)
    {
        return toAjax(dbFloorService.insertDbFloor(dbFloor));
    }

    /**
     * 修改楼层管理
     */
    @PreAuthorize("@ss.hasPermi('floor:floor:edit')")
    @Log(title = "楼层管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbFloor dbFloor)
    {
        return toAjax(dbFloorService.updateDbFloor(dbFloor));
    }

    /**
     * 删除楼层管理
     */
    @PreAuthorize("@ss.hasPermi('floor:floor:remove')")
    @Log(title = "楼层管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbFloorService.deleteDbFloorByIds(ids));
    }
}
