package com.ruoyi.floor.mapper;

import com.ruoyi.domain.DbFloor;

import java.util.List;

/**
 * 楼层管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public interface DbFloorMapper 
{
    /**
     * 查询楼层管理
     * 
     * @param id 楼层管理主键
     * @return 楼层管理
     */
    public DbFloor selectDbFloorById(Long id);

    /**
     * 查询楼层管理列表
     * 
     * @param dbFloor 楼层管理
     * @return 楼层管理集合
     */
    public List<DbFloor> selectDbFloorList(DbFloor dbFloor);

    /**
     * 新增楼层管理
     * 
     * @param dbFloor 楼层管理
     * @return 结果
     */
    public int insertDbFloor(DbFloor dbFloor);

    /**
     * 修改楼层管理
     * 
     * @param dbFloor 楼层管理
     * @return 结果
     */
    public int updateDbFloor(DbFloor dbFloor);

    /**
     * 删除楼层管理
     * 
     * @param id 楼层管理主键
     * @return 结果
     */
    public int deleteDbFloorById(Long id);

    /**
     * 批量删除楼层管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbFloorByIds(Long[] ids);
}
