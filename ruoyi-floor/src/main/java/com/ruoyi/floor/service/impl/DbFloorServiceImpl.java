package com.ruoyi.floor.service.impl;

import java.util.List;

import com.ruoyi.domain.DbFloor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.floor.mapper.DbFloorMapper;
import com.ruoyi.floor.service.IDbFloorService;

/**
 * 楼层管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
@Service
public class DbFloorServiceImpl implements IDbFloorService 
{
    @Autowired
    private DbFloorMapper dbFloorMapper;

    /**
     * 查询楼层管理
     * 
     * @param id 楼层管理主键
     * @return 楼层管理
     */
    @Override
    public DbFloor selectDbFloorById(Long id)
    {
        return dbFloorMapper.selectDbFloorById(id);
    }

    /**
     * 查询楼层管理列表
     * 
     * @param dbFloor 楼层管理
     * @return 楼层管理
     */
    @Override
    public List<DbFloor> selectDbFloorList(DbFloor dbFloor)
    {
        return dbFloorMapper.selectDbFloorList(dbFloor);
    }

    /**
     * 新增楼层管理
     * 
     * @param dbFloor 楼层管理
     * @return 结果
     */
    @Override
    public int insertDbFloor(DbFloor dbFloor)
    {
        return dbFloorMapper.insertDbFloor(dbFloor);
    }

    /**
     * 修改楼层管理
     * 
     * @param dbFloor 楼层管理
     * @return 结果
     */
    @Override
    public int updateDbFloor(DbFloor dbFloor)
    {
        return dbFloorMapper.updateDbFloor(dbFloor);
    }

    /**
     * 批量删除楼层管理
     * 
     * @param ids 需要删除的楼层管理主键
     * @return 结果
     */
    @Override
    public int deleteDbFloorByIds(Long[] ids)
    {
        return dbFloorMapper.deleteDbFloorByIds(ids);
    }

    /**
     * 删除楼层管理信息
     * 
     * @param id 楼层管理主键
     * @return 结果
     */
    @Override
    public int deleteDbFloorById(Long id)
    {
        return dbFloorMapper.deleteDbFloorById(id);
    }
}
