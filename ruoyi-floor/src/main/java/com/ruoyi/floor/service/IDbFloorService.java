package com.ruoyi.floor.service;

import com.ruoyi.domain.DbFloor;

import java.util.List;

/**
 * 楼层管理Service接口
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public interface IDbFloorService 
{
    /**
     * 查询楼层管理
     * 
     * @param id 楼层管理主键
     * @return 楼层管理
     */
    public DbFloor selectDbFloorById(Long id);

    /**
     * 查询楼层管理列表
     * 
     * @param dbFloor 楼层管理
     * @return 楼层管理集合
     */
    public List<DbFloor> selectDbFloorList(DbFloor dbFloor);

    /**
     * 新增楼层管理
     * 
     * @param dbFloor 楼层管理
     * @return 结果
     */
    public int insertDbFloor(DbFloor dbFloor);

    /**
     * 修改楼层管理
     * 
     * @param dbFloor 楼层管理
     * @return 结果
     */
    public int updateDbFloor(DbFloor dbFloor);

    /**
     * 批量删除楼层管理
     * 
     * @param ids 需要删除的楼层管理主键集合
     * @return 结果
     */
    public int deleteDbFloorByIds(Long[] ids);

    /**
     * 删除楼层管理信息
     * 
     * @param id 楼层管理主键
     * @return 结果
     */
    public int deleteDbFloorById(Long id);
}
