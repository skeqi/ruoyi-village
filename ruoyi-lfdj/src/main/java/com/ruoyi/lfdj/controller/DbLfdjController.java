package com.ruoyi.lfdj.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbLfdj;
import com.ruoyi.lfdj.service.IDbLfdjService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 来访登记Controller
 * 
 * @author 郑东来
 * @date 2023-03-09
 */
@RestController
@RequestMapping("/lfdj/lfdj")
public class DbLfdjController extends BaseController
{
    @Autowired
    private IDbLfdjService dbLfdjService;

    /**
     * 查询来访登记列表
     */
    @PreAuthorize("@ss.hasPermi('lfdj:lfdj:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbLfdj dbLfdj)
    {
        startPage();
        List<DbLfdj> list = dbLfdjService.selectDbLfdjList(dbLfdj);
        return getDataTable(list);
    }

    /**
     * 导出来访登记列表
     */
    @PreAuthorize("@ss.hasPermi('lfdj:lfdj:export')")
    @Log(title = "来访登记", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbLfdj dbLfdj)
    {
        List<DbLfdj> list = dbLfdjService.selectDbLfdjList(dbLfdj);
        ExcelUtil<DbLfdj> util = new ExcelUtil<DbLfdj>(DbLfdj.class);
        util.exportExcel(response, list, "来访登记数据");
    }

    /**
     * 获取来访登记详细信息
     */
    @PreAuthorize("@ss.hasPermi('lfdj:lfdj:query')")
    @GetMapping(value = "/{lfdjId}")
    public AjaxResult getInfo(@PathVariable("lfdjId") Long lfdjId)
    {
        return success(dbLfdjService.selectDbLfdjByLfdjId(lfdjId));
    }

    /**
     * 新增来访登记
     */
    @PreAuthorize("@ss.hasPermi('lfdj:lfdj:add')")
    @Log(title = "来访登记", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbLfdj dbLfdj)
    {
        return toAjax(dbLfdjService.insertDbLfdj(dbLfdj));
    }

    /**
     * 修改来访登记
     */
    @PreAuthorize("@ss.hasPermi('lfdj:lfdj:edit')")
    @Log(title = "来访登记", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbLfdj dbLfdj)
    {
        return toAjax(dbLfdjService.updateDbLfdj(dbLfdj));
    }

    /**
     * 删除来访登记
     */
    @PreAuthorize("@ss.hasPermi('lfdj:lfdj:remove')")
    @Log(title = "来访登记", businessType = BusinessType.DELETE)
	@DeleteMapping("/{lfdjIds}")
    public AjaxResult remove(@PathVariable Long[] lfdjIds)
    {
        return toAjax(dbLfdjService.deleteDbLfdjByLfdjIds(lfdjIds));
    }
}
