package com.ruoyi.lfdj.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.lfdj.mapper.DbLfdjMapper;
import com.ruoyi.domain.DbLfdj;
import com.ruoyi.lfdj.service.IDbLfdjService;

/**
 * 来访登记Service业务层处理
 * 
 * @author 郑东来
 * @date 2023-03-09
 */
@Service
public class DbLfdjServiceImpl implements IDbLfdjService 
{
    @Autowired
    private DbLfdjMapper dbLfdjMapper;

    /**
     * 查询来访登记
     * 
     * @param lfdjId 来访登记主键
     * @return 来访登记
     */
    @Override
    public DbLfdj selectDbLfdjByLfdjId(Long lfdjId)
    {
        return dbLfdjMapper.selectDbLfdjByLfdjId(lfdjId);
    }

    /**
     * 查询来访登记列表
     * 
     * @param dbLfdj 来访登记
     * @return 来访登记
     */
    @Override
    public List<DbLfdj> selectDbLfdjList(DbLfdj dbLfdj)
    {
        return dbLfdjMapper.selectDbLfdjList(dbLfdj);
    }

    /**
     * 新增来访登记
     * 
     * @param dbLfdj 来访登记
     * @return 结果
     */
    @Override
    public int insertDbLfdj(DbLfdj dbLfdj)
    {
        return dbLfdjMapper.insertDbLfdj(dbLfdj);
    }

    /**
     * 修改来访登记
     * 
     * @param dbLfdj 来访登记
     * @return 结果
     */
    @Override
    public int updateDbLfdj(DbLfdj dbLfdj)
    {
        return dbLfdjMapper.updateDbLfdj(dbLfdj);
    }

    /**
     * 批量删除来访登记
     * 
     * @param lfdjIds 需要删除的来访登记主键
     * @return 结果
     */
    @Override
    public int deleteDbLfdjByLfdjIds(Long[] lfdjIds)
    {
        return dbLfdjMapper.deleteDbLfdjByLfdjIds(lfdjIds);
    }

    /**
     * 删除来访登记信息
     * 
     * @param lfdjId 来访登记主键
     * @return 结果
     */
    @Override
    public int deleteDbLfdjByLfdjId(Long lfdjId)
    {
        return dbLfdjMapper.deleteDbLfdjByLfdjId(lfdjId);
    }
}
