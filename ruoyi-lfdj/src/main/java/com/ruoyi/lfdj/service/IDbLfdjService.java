package com.ruoyi.lfdj.service;

import java.util.List;
import com.ruoyi.domain.DbLfdj;

/**
 * 来访登记Service接口
 * 
 * @author 郑东来
 * @date 2023-03-09
 */
public interface IDbLfdjService 
{
    /**
     * 查询来访登记
     * 
     * @param lfdjId 来访登记主键
     * @return 来访登记
     */
    public DbLfdj selectDbLfdjByLfdjId(Long lfdjId);

    /**
     * 查询来访登记列表
     * 
     * @param dbLfdj 来访登记
     * @return 来访登记集合
     */
    public List<DbLfdj> selectDbLfdjList(DbLfdj dbLfdj);

    /**
     * 新增来访登记
     * 
     * @param dbLfdj 来访登记
     * @return 结果
     */
    public int insertDbLfdj(DbLfdj dbLfdj);

    /**
     * 修改来访登记
     * 
     * @param dbLfdj 来访登记
     * @return 结果
     */
    public int updateDbLfdj(DbLfdj dbLfdj);

    /**
     * 批量删除来访登记
     * 
     * @param lfdjIds 需要删除的来访登记主键集合
     * @return 结果
     */
    public int deleteDbLfdjByLfdjIds(Long[] lfdjIds);

    /**
     * 删除来访登记信息
     * 
     * @param lfdjId 来访登记主键
     * @return 结果
     */
    public int deleteDbLfdjByLfdjId(Long lfdjId);
}
