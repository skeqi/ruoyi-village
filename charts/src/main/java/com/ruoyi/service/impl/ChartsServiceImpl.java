package com.ruoyi.service.impl;

import com.ruoyi.mapper.ChartsMapper;
import com.ruoyi.service.ChartsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ChartsServiceImpl implements ChartsService {
    @Resource
    private ChartsMapper chartsMapper;
    @Override
    public Long buildingCount() {
        return chartsMapper.buildingCount();
    }

    @Override
    public Long housesCount() {
        return chartsMapper.housesCount();
    }

    @Override
    public Long tousuCount() {
        return chartsMapper.tousuCount();
    }

    @Override
    public Long repairCount() {
        return chartsMapper.repairCount();
    }

    @Override
    public Long shopsCount() {
        return chartsMapper.shopsCount();
    }

    @Override
    public Long vehicleCount() {
        return chartsMapper.vehicleCount();
    }

    @Override
    public Long parkingCount() {
        return chartsMapper.parkingCount();
    }

    @Override
    public Long ownerCount() {
        return chartsMapper.ownerCount();
    }
}
