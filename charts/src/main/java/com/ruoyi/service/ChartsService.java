package com.ruoyi.service;

public interface ChartsService {
    //楼栋总数
    public Long buildingCount();

    //房屋总数
    public Long housesCount();

    //投诉总数
    public Long tousuCount();

    //维修总数
    public Long repairCount();

    //商铺总数
    public Long shopsCount();

    //车辆总数
    public Long vehicleCount();

    //车位总数
    public Long parkingCount();

    //业主总数
    public Long ownerCount();
}
