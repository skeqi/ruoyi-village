//package com.ruoyi.controller;
//
//import com.alibaba.fastjson2.JSON;
//import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
//import com.aliyun.tea.*;
//import com.ruoyi.common.core.redis.RedisCache;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Component;
//
//
//import javax.annotation.Resource;
//import java.util.Random;
//import java.util.concurrent.TimeUnit;
//@Component
//public class Sample {
//    @Autowired
//    private RedisTemplate redisTemplate;
//
//    /**
//     * 使用AK&SK初始化账号Client
//     *
//     * @param accessKeyId
//     * @param accessKeySecret
//     * @return Client
//     * @throws Exception
//     */
//    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
//        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
//                // 必填，您的 AccessKey ID
//                .setAccessKeyId(accessKeyId)
//                // 必填，您的 AccessKey Secret
//                .setAccessKeySecret(accessKeySecret);
//        // 访问的域名
//        config.endpoint = "dysmsapi.aliyuncs.com";
//        return new com.aliyun.dysmsapi20170525.Client(config);
//    }
//
//    public SendSmsResponse send(String phoneNumbers) throws Exception {
//        Random random = new Random();
//        Integer ran = random.nextInt(8999) + 1000;
//        // 工程代码泄露可能会导致AccessKey泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378657.html
//        com.aliyun.dysmsapi20170525.Client client = Sample.createClient("LTAI5tFLB56AZMs7XDe4LMVr", "z9UswYCgJv5m2LfkllumbU1LMmBbev");
//        com.aliyun.dysmsapi20170525.models.SendSmsRequest sendSmsRequest = new com.aliyun.dysmsapi20170525.models.SendSmsRequest()
//                .setSignName("阿里云短信测试")
//                .setTemplateCode("SMS_154950909")
//                .setPhoneNumbers(phoneNumbers)
//                .setTemplateParam("{\"code\":\"" + ran + "\"}");
//
//        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
//        SendSmsResponse response = null;
//        redisTemplate.opsForValue().set(phoneNumbers,ran,1,TimeUnit.MINUTES);
//        try {
//            // 复制代码运行请自行打印 API 的返回值
//            response = client.sendSmsWithOptions(sendSmsRequest, runtime);
//            System.out.println(JSON.toJSONString(response));
//        } catch (TeaException error) {
//            // 如有需要，请打印 error
//            com.aliyun.teautil.Common.assertAsString(error.message);
//        }
//        return response;
//    }
//}