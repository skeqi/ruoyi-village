package com.ruoyi.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.service.ChartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
public class ChartsController {
    @Resource
    private ChartsService chartsService;
//    //楼栋总数
//    public Long buildingCount();
@Autowired
private RedisTemplate redisTemplate;
    @GetMapping("/buildingCount")

    public AjaxResult buildingCount() {
        redisTemplate.opsForValue().set("phoneNumbers","ran",1, TimeUnit.MINUTES);
        return AjaxResult.success(chartsService.buildingCount());

    }

    //
//    //房屋总数
//    public Long housesCount();
    @GetMapping("/housesCount")
    public AjaxResult housesCount() {
        return AjaxResult.success(chartsService.housesCount());
    }

    //
//    //投诉总数
//    public Long tousuCount();
    @GetMapping("/tousuCount")
    public AjaxResult tousuCount() {
        return AjaxResult.success(chartsService.tousuCount());
    }

    //
//    //维修总数
//    public Long repairCount();
    @GetMapping("/repairCount")
    public AjaxResult repairCount() {
        return AjaxResult.success(chartsService.repairCount());
    }

    //    //商铺总数
//    public Long shopsCount();
    @GetMapping("/shopsCount")
    public AjaxResult shopsCount() {
        return AjaxResult.success(chartsService.shopsCount());
    }

    //
//    //车辆总数
//    public Long vehicleCount();
    @GetMapping("/vehicleCount")
    public AjaxResult vehicleCount() {
        return AjaxResult.success(chartsService.vehicleCount());
    }

    //
//    //车位总数
//    public Long parkingCount();
    @GetMapping("/parkingCount")
    public AjaxResult parkingCount() {
        return AjaxResult.success(chartsService.parkingCount());
    }

    //
//    //业主总数
//    public Long ownerCount();
    @GetMapping("/ownerCount")
    public AjaxResult ownerCount() {
        return AjaxResult.success(chartsService.ownerCount());
    }
}
