package com.ruoyi.repair.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.StaffUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.*;
import com.ruoyi.repair.service.IDbRepairService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报修信息Controller
 *
 * @author ruoyi
 * @date 2023-03-21
 */
@Api("设备管理-报修信息")
@RestController
@RequestMapping("/repair/repair")
public class DbRepairController extends BaseController
{
    @Autowired
    private IDbRepairService dbRepairService;

    /**
     * 查询报修信息列表
     */
    @PreAuthorize("@ss.hasPermi('repair:repair:list')")
    @GetMapping("/list")
    @ApiOperation("报修信息列表")
    public TableDataInfo list(DbRepair dbRepair)
    {
        startPage();
        List<DbRepair> list = dbRepairService.selectDbRepairList(dbRepair);
        return getDataTable(list);
    }

    @GetMapping("/repairList")
    @ApiOperation("员工信息列表")
    public List<StaffUser> repairList(){
        return dbRepairService.findStaffUser();
    }

    /**
     * 导出报修信息列表
     */
    @PreAuthorize("@ss.hasPermi('repair:repair:export')")
    @Log(title = "报修信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("报修信息导出")
    public void export(HttpServletResponse response, DbRepair dbRepair)
    {
        List<DbRepair> list = dbRepairService.selectDbRepairList(dbRepair);
        ExcelUtil<DbRepair> util = new ExcelUtil<DbRepair>(DbRepair.class);
        util.exportExcel(response, list, "报修信息数据");
    }

    /**
     * 获取报修信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('repair:repair:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("报修信息详情")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbRepairService.selectDbRepairById(id));
    }

    /**
     * 新增报修信息
     */
    @PreAuthorize("@ss.hasPermi('repair:repair:add')")
    @Log(title = "报修信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("报修信息新增")
    public AjaxResult add(@RequestBody DbRepair dbRepair)
    {
        return toAjax(dbRepairService.insertDbRepair(dbRepair));
    }

    @GetMapping("/cxadd")
    @ApiOperation("报修信息新增")
    public AjaxResult cxadd(@RequestBody DbRepair dbRepair)
    {
        return toAjax(dbRepairService.insertDbRepair(dbRepair));
    }

    /**
     * 修改报修信息
     */
    @PreAuthorize("@ss.hasPermi('repair:repair:edit')")
    @Log(title = "报修信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("报修信息修改")
    public AjaxResult edit(@RequestBody DbRepair dbRepair)
    {
        return toAjax(dbRepairService.updateDbRepair(dbRepair));
    }

    /**
     * 删除报修信息
     */
    @PreAuthorize("@ss.hasPermi('repair:repair:remove')")
    @Log(title = "报修信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("报修信息删除")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbRepairService.deleteDbRepairByIds(ids));
    }
}
