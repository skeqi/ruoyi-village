package com.ruoyi.repair.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.*;
import com.ruoyi.repair.service.IXUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 维修员工Controller
 * 
 * @author ruoyi
 * @date 2023-03-22
 */
@Api("设备管理-员工信息")
@RestController
@RequestMapping("/repair/xuser")
public class XUserController extends BaseController
{
    @Autowired
    private IXUserService xUserService;

    /**
     * 查询维修员工列表
     */
    @PreAuthorize("@ss.hasPermi('repair:xuser:list')")
    @GetMapping("/list")
    @ApiOperation("维修员工信息列表")
    public TableDataInfo list(XUser xUser)
    {
        startPage();
        List<XUser> list = xUserService.selectXUserList(xUser);
        return getDataTable(list);
    }

    /**
     * 查询维修员工列表
     */
    @PreAuthorize("@ss.hasPermi('repair:xuser:list1')")
    @GetMapping("/list1")
    @ApiOperation("安保员工信息列表")
    public TableDataInfo list1(XUser xUser)
    {
        startPage();
        List<XUser> list = xUserService.selectUserList(xUser);
        return getDataTable(list);
    }

    /**
     * 导出维修员工列表
     */
    @PreAuthorize("@ss.hasPermi('repair:xuser:export')")
    @Log(title = "维修员工", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("员工信息导出")
    public void export(HttpServletResponse response, XUser xUser)
    {
        List<XUser> list = xUserService.selectXUserList(xUser);
        ExcelUtil<XUser> util = new ExcelUtil<XUser>(XUser.class);
        util.exportExcel(response, list, "维修员工数据");
    }

    /**
     * 获取维修员工详细信息
     */
    @PreAuthorize("@ss.hasPermi('repair:xuser:query')")
    @GetMapping(value = "/{staffId}")
    @ApiOperation("员工信息详情")
    public AjaxResult getInfo(@PathVariable("staffId") Long staffId)
    {
        return success(xUserService.selectXUserByStaffId(staffId));
    }

    /**
     * 新增维修员工
     */
    @PreAuthorize("@ss.hasPermi('repair:xuser:add')")
    @Log(title = "维修员工", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("员工信息新增")
    public AjaxResult add(@RequestBody XUser xUser)
    {
        return toAjax(xUserService.insertXUser(xUser));
    }

    /**
     * 修改维修员工
     */
    @PreAuthorize("@ss.hasPermi('repair:xuser:edit')")
    @Log(title = "维修员工", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("员工信息修改")
    public AjaxResult edit(@RequestBody XUser xUser)
    {
        return toAjax(xUserService.updateXUser(xUser));
    }

    /**
     * 删除维修员工
     */
    @PreAuthorize("@ss.hasPermi('repair:xuser:remove')")
    @Log(title = "维修员工", businessType = BusinessType.DELETE)
	@DeleteMapping("/{staffIds}")
    @ApiOperation("员工信息删除")
    public AjaxResult remove(@PathVariable Long[] staffIds)
    {
        return toAjax(xUserService.deleteXUserByStaffIds(staffIds));
    }
}
