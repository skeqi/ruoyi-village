package com.ruoyi.repair.mapper;

import java.util.List;

import com.ruoyi.domain.StaffUser;
import com.ruoyi.domain.*;
/**
 * 报修信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public interface DbRepairMapper 
{
    /**
     * 查询报修信息
     * 
     * @param id 报修信息主键
     * @return 报修信息
     */
    public DbRepair selectDbRepairById(Long id);

    /**
     * 查询报修信息列表
     * 
     * @param dbRepair 报修信息
     * @return 报修信息集合
     */
    public List<DbRepair> selectDbRepairList(DbRepair dbRepair);

    /**
     * 新增报修信息
     * 
     * @param dbRepair 报修信息
     * @return 结果
     */
    public int insertDbRepair(DbRepair dbRepair);

    /**
     * 修改报修信息
     * 
     * @param dbRepair 报修信息
     * @return 结果
     */
    public int updateDbRepair(DbRepair dbRepair);

    /**
     * 删除报修信息
     * 
     * @param id 报修信息主键
     * @return 结果
     */
    public int deleteDbRepairById(Long id);

    /**
     * 批量删除报修信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbRepairByIds(Long[] ids);


    public List<StaffUser> findStaffUser();
}
