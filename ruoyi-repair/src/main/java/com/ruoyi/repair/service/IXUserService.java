package com.ruoyi.repair.service;

import java.util.List;
import com.ruoyi.domain.*;
/**
 * 维修员工Service接口
 * 
 * @author ruoyi
 * @date 2023-03-22
 */
public interface IXUserService 
{
    /**
     * 查询维修员工
     * 
     * @param staffId 维修员工主键
     * @return 维修员工
     */
    public XUser selectXUserByStaffId(Long staffId);

    /**
     * 查询维修员工列表
     * 
     * @param xUser 维修员工
     * @return 维修员工集合
     */
    public List<XUser> selectXUserList(XUser xUser);

    /**
     * 查询维修员工列表
     *
     * @param xUser 维修员工
     * @return 维修员工集合
     */
    public List<XUser> selectUserList(XUser xUser);

    /**
     * 新增维修员工
     * 
     * @param xUser 维修员工
     * @return 结果
     */
    public int insertXUser(XUser xUser);

    /**
     * 修改维修员工
     * 
     * @param xUser 维修员工
     * @return 结果
     */
    public int updateXUser(XUser xUser);

    /**
     * 批量删除维修员工
     * 
     * @param staffIds 需要删除的维修员工主键集合
     * @return 结果
     */
    public int deleteXUserByStaffIds(Long[] staffIds);

    /**
     * 删除维修员工信息
     * 
     * @param staffId 维修员工主键
     * @return 结果
     */
    public int deleteXUserByStaffId(Long staffId);
}
