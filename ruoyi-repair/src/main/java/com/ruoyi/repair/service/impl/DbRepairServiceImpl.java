package com.ruoyi.repair.service.impl;

import java.util.List;

import com.ruoyi.domain.StaffUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.repair.mapper.DbRepairMapper;
import com.ruoyi.domain.*;
import com.ruoyi.repair.service.IDbRepairService;

/**
 * 报修信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
@Service
public class DbRepairServiceImpl implements IDbRepairService 
{
    @Autowired
    private DbRepairMapper dbRepairMapper;

    /**
     * 查询报修信息
     * 
     * @param id 报修信息主键
     * @return 报修信息
     */
    @Override
    public DbRepair selectDbRepairById(Long id)
    {
        return dbRepairMapper.selectDbRepairById(id);
    }

    /**
     * 查询报修信息列表
     * 
     * @param dbRepair 报修信息
     * @return 报修信息
     */
    @Override
    public List<DbRepair> selectDbRepairList(DbRepair dbRepair)
    {
        return dbRepairMapper.selectDbRepairList(dbRepair);
    }

    /**
     * 新增报修信息
     * 
     * @param dbRepair 报修信息
     * @return 结果
     */
    @Override
    public int insertDbRepair(DbRepair dbRepair)
    {
        return dbRepairMapper.insertDbRepair(dbRepair);
    }

    /**
     * 修改报修信息
     * 
     * @param dbRepair 报修信息
     * @return 结果
     */
    @Override
    public int updateDbRepair(DbRepair dbRepair)
    {
        return dbRepairMapper.updateDbRepair(dbRepair);
    }

    /**
     * 批量删除报修信息
     * 
     * @param ids 需要删除的报修信息主键
     * @return 结果
     */
    @Override
    public int deleteDbRepairByIds(Long[] ids)
    {
        return dbRepairMapper.deleteDbRepairByIds(ids);
    }

    /**
     * 删除报修信息信息
     * 
     * @param id 报修信息主键
     * @return 结果
     */
    @Override
    public int deleteDbRepairById(Long id)
    {
        return dbRepairMapper.deleteDbRepairById(id);
    }

    @Override
    public List<StaffUser> findStaffUser() {
        return dbRepairMapper.findStaffUser();
    }
}
