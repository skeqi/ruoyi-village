package com.ruoyi.repair.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.repair.mapper.XUserMapper;
import com.ruoyi.domain.*;import com.ruoyi.repair.service.IXUserService;

/**
 * 维修员工Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-22
 */
@Service
public class XUserServiceImpl implements IXUserService 
{
    @Autowired
    private XUserMapper xUserMapper;

    /**
     * 查询维修员工
     * 
     * @param staffId 维修员工主键
     * @return 维修员工
     */
    @Override
    public XUser selectXUserByStaffId(Long staffId)
    {
        return xUserMapper.selectXUserByStaffId(staffId);
    }

    /**
     * 查询维修员工列表
     * 
     * @param xUser 维修员工
     * @return 维修员工
     */
    @Override
    public List<XUser> selectXUserList(XUser xUser)
    {
        return xUserMapper.selectXUserList(xUser);
    }

    @Override
    public List<XUser> selectUserList(XUser xUser) {
        return xUserMapper.selectUserList(xUser);
    }

    /**
     * 新增维修员工
     * 
     * @param xUser 维修员工
     * @return 结果
     */
    @Override
    public int insertXUser(XUser xUser)
    {
        return xUserMapper.insertXUser(xUser);
    }

    /**
     * 修改维修员工
     * 
     * @param xUser 维修员工
     * @return 结果
     */
    @Override
    public int updateXUser(XUser xUser)
    {
        return xUserMapper.updateXUser(xUser);
    }

    /**
     * 批量删除维修员工
     * 
     * @param staffIds 需要删除的维修员工主键
     * @return 结果
     */
    @Override
    public int deleteXUserByStaffIds(Long[] staffIds)
    {
        return xUserMapper.deleteXUserByStaffIds(staffIds);
    }

    /**
     * 删除维修员工信息
     * 
     * @param staffId 维修员工主键
     * @return 结果
     */
    @Override
    public int deleteXUserByStaffId(Long staffId)
    {
        return xUserMapper.deleteXUserByStaffId(staffId);
    }
}
