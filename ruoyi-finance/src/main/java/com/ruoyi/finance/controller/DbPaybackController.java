package com.ruoyi.finance.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbPayback;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.finance.service.IDbPaybackService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 还款记录Controller
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
@Api("还款记录")
@RestController
@RequestMapping("/finance/payback")
public class DbPaybackController extends BaseController
{
    @Autowired
    private IDbPaybackService dbPaybackService;

    /**
     * 查询还款记录列表
     */
    @PreAuthorize("@ss.hasPermi('finance:payback:list')")
    @GetMapping("/list")
    @ApiOperation("查询还款记录列表")
    public TableDataInfo list(DbPayback dbPayback)
    {
        startPage();
        List<DbPayback> list = dbPaybackService.selectDbPaybackList(dbPayback);
        return getDataTable(list);
    }

    /**
     * 导出还款记录列表
     */
    @PreAuthorize("@ss.hasPermi('finance:payback:export')")
    @Log(title = "还款记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出还款记录列表")
    public void export(HttpServletResponse response, DbPayback dbPayback)
    {
        List<DbPayback> list = dbPaybackService.selectDbPaybackList(dbPayback);
        ExcelUtil<DbPayback> util = new ExcelUtil<DbPayback>(DbPayback.class);
        util.exportExcel(response, list, "还款记录数据");
    }

    /**
     * 获取还款记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('finance:payback:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("获取还款记录详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbPaybackService.selectDbPaybackById(id));
    }

    /**
     * 新增还款记录
     */
    @PreAuthorize("@ss.hasPermi('finance:payback:add')")
    @Log(title = "还款记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("新增还款记录")
    public AjaxResult add(@RequestBody DbPayback dbPayback)
    {
        return toAjax(dbPaybackService.insertDbPayback(dbPayback));
    }

    /**
     * 修改还款记录
     */
    @PreAuthorize("@ss.hasPermi('finance:payback:edit')")
    @Log(title = "还款记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改还款记录")
    public AjaxResult edit(@RequestBody DbPayback dbPayback)
    {
        return toAjax(dbPaybackService.updateDbPayback(dbPayback));
    }

    /**
     * 删除还款记录
     */
    @PreAuthorize("@ss.hasPermi('finance:payback:remove')")
    @Log(title = "还款记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("删除还款记录")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbPaybackService.deleteDbPaybackByIds(ids));
    }
}
