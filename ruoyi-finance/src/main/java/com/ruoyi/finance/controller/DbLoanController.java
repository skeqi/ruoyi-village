package com.ruoyi.finance.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbLoan;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.finance.service.IDbLoanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 放款记录Controller
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
@Api("放款记录")
@RestController
@RequestMapping("/finance/loan")
public class DbLoanController extends BaseController
{
    @Autowired
    private IDbLoanService dbLoanService;

    /**
     * 查询放款记录列表
     */
    @PreAuthorize("@ss.hasPermi('finance:loan:list')")
    @GetMapping("/list")
    @ApiOperation("查询放款记录列表")
    public TableDataInfo list(DbLoan dbLoan)
    {
        startPage();
        List<DbLoan> list = dbLoanService.selectDbLoanList(dbLoan);
        return getDataTable(list);
    }

    /**
     * 导出放款记录列表
     */
    @PreAuthorize("@ss.hasPermi('finance:loan:export')")
    @Log(title = "放款记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出放款记录列表")
    public void export(HttpServletResponse response, DbLoan dbLoan)
    {
        List<DbLoan> list = dbLoanService.selectDbLoanList(dbLoan);
        ExcelUtil<DbLoan> util = new ExcelUtil<DbLoan>(DbLoan.class);
        util.exportExcel(response, list, "放款记录数据");
    }

    /**
     * 获取放款记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('finance:loan:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("获取放款记录详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbLoanService.selectDbLoanById(id));
    }

    /**
     * 新增放款记录
     */
    @PreAuthorize("@ss.hasPermi('finance:loan:add')")
    @Log(title = "放款记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("新增放款记录")
    public AjaxResult add(@RequestBody DbLoan dbLoan)
    {
        return toAjax(dbLoanService.insertDbLoan(dbLoan));
    }

    /**
     * 修改放款记录
     */
    @PreAuthorize("@ss.hasPermi('finance:loan:edit')")
    @Log(title = "放款记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改放款记录")
    public AjaxResult edit(@RequestBody DbLoan dbLoan)
    {
        return toAjax(dbLoanService.updateDbLoan(dbLoan));
    }

    /**
     * 删除放款记录
     */
    @PreAuthorize("@ss.hasPermi('finance:loan:remove')")
    @Log(title = "放款记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("删除放款记录")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbLoanService.deleteDbLoanByIds(ids));
    }
}
