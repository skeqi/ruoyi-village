package com.ruoyi.finance.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.domain.DbHt;
import com.ruoyi.finance.service.IDbHtService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.domain.*;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 签署合同Controller
 *
 * @author lql
 * @date 2023-03-14
 */
@Api("签署合同")
@RestController
@RequestMapping("/finance/ht")
public class DbHtController extends BaseController
{
    @Autowired
    private IDbHtService dbHtService;

    /**
     * 查询签署合同列表
     */
    @PreAuthorize("@ss.hasPermi('finance:ht:list')")
    @GetMapping("/list")
    @ApiOperation("签署合同列表")
    public TableDataInfo list(DbHt dbHt)
    {
        startPage();
        List<DbHt> list = dbHtService.selectDbHtList(dbHt);
        return getDataTable(list);
    }

    /**
     * 导出签署合同列表
     */
    @PreAuthorize("@ss.hasPermi('finance:ht:export')")
    @Log(title = "签署合同", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出签署合同列表")
    public void export(HttpServletResponse response, DbHt dbHt)
    {
        List<DbHt> list = dbHtService.selectDbHtList(dbHt);
        ExcelUtil<DbHt> util = new ExcelUtil<DbHt>(DbHt.class);
        util.exportExcel(response, list, "签署合同数据");
    }

    /**
     * 获取签署合同详细信息
     */
    @PreAuthorize("@ss.hasPermi('finance:ht:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("获取签署合同详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbHtService.selectDbHtById(id));
    }

    /**
     * 新增签署合同
     */
    @PreAuthorize("@ss.hasPermi('finance:ht:add')")
    @Log(title = "签署合同", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("新增签署合同")
    public AjaxResult add(@RequestBody DbHt dbHt)
    {
        return toAjax(dbHtService.insertDbHt(dbHt));
    }

    /**
     * 修改签署合同
     */
    @PreAuthorize("@ss.hasPermi('finance:ht:edit')")
    @Log(title = "签署合同", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改签署合同")
    public AjaxResult edit(@RequestBody DbHt dbHt)
    {
        return toAjax(dbHtService.updateDbHt(dbHt));
    }

    /**
     * 删除签署合同
     */
    @PreAuthorize("@ss.hasPermi('finance:ht:remove')")
    @Log(title = "签署合同", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("删除签署合同")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbHtService.deleteDbHtByIds(ids));
    }
}
