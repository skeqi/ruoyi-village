package com.ruoyi.finance.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.domain.DbSqdk;
import com.ruoyi.finance.service.IDbSqdkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 申请贷款Controller
 *
 * @author lql
 * @date 2023-03-14
 */
@Api("申请贷款")
@RestController
@RequestMapping("/finance/sqdk")
public class DbSqdkController extends BaseController
{
    @Autowired
    private IDbSqdkService dbSqdkService;

    /**
     * 查询申请贷款列表
     */
    @PreAuthorize("@ss.hasPermi('finance:sqdk:list')")
    @GetMapping("/list")
    @ApiOperation("查询申请贷款列表")
    public TableDataInfo list(DbSqdk dbSqdk)
    {
        startPage();
        List<DbSqdk> list = dbSqdkService.selectDbSqdkList(dbSqdk);
        return getDataTable(list);
    }

    /**
     * 导出申请贷款列表
     */
    @PreAuthorize("@ss.hasPermi('finance:sqdk:export')")
    @Log(title = "申请贷款", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出申请贷款列表")
    public void export(HttpServletResponse response, DbSqdk dbSqdk)
    {
        List<DbSqdk> list = dbSqdkService.selectDbSqdkList(dbSqdk);
        ExcelUtil<DbSqdk> util = new ExcelUtil<DbSqdk>(DbSqdk.class);
        util.exportExcel(response, list, "申请贷款数据");
    }

    /**
     * 获取申请贷款详细信息
     */
    @PreAuthorize("@ss.hasPermi('finance:sqdk:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("获取申请贷款详细信息")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(dbSqdkService.selectDbSqdkById(id));
    }

    /**
     * 新增申请贷款
     */
    @PreAuthorize("@ss.hasPermi('finance:sqdk:add')")
    @Log(title = "申请贷款", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("新增申请贷款")
    public AjaxResult add(@RequestBody DbSqdk dbSqdk)
    {
        return toAjax(dbSqdkService.insertDbSqdk(dbSqdk));
    }

    /**
     * 修改申请贷款
     */
    @PreAuthorize("@ss.hasPermi('finance:sqdk:edit')")
    @Log(title = "申请贷款", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改申请贷款")
    public AjaxResult edit(@RequestBody DbSqdk dbSqdk)
    {
        return toAjax(dbSqdkService.updateDbSqdk(dbSqdk));
    }

    /**
     * 删除申请贷款
     */
    @PreAuthorize("@ss.hasPermi('finance:sqdk:remove')")
    @Log(title = "申请贷款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("删除申请贷款")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(dbSqdkService.deleteDbSqdkByIds(ids));
    }
}
