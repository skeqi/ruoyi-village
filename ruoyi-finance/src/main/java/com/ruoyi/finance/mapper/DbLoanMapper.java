package com.ruoyi.finance.mapper;

import com.ruoyi.domain.DbLoan;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 放款记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
public interface DbLoanMapper 
{
    /**
     * 查询放款记录
     * 
     * @param id 放款记录主键
     * @return 放款记录
     */
    public DbLoan selectDbLoanById(Long id);

    /**
     * 查询放款记录列表
     * 
     * @param dbLoan 放款记录
     * @return 放款记录集合
     */
    public List<DbLoan> selectDbLoanList(DbLoan dbLoan);

    /**
     * 新增放款记录
     * 
     * @param dbLoan 放款记录
     * @return 结果
     */
    public int insertDbLoan(DbLoan dbLoan);

    /**
     * 修改放款记录
     * 
     * @param dbLoan 放款记录
     * @return 结果
     */
    public int updateDbLoan(DbLoan dbLoan);

    /**
     * 删除放款记录
     * 
     * @param id 放款记录主键
     * @return 结果
     */
    public int deleteDbLoanById(Long id);

    /**
     * 批量删除放款记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbLoanByIds(Long[] ids);
}
