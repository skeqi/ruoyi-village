package com.ruoyi.finance.mapper;

import com.ruoyi.domain.DbPayback;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 还款记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
public interface DbPaybackMapper 
{
    /**
     * 查询还款记录
     * 
     * @param id 还款记录主键
     * @return 还款记录
     */
    public DbPayback selectDbPaybackById(Long id);

    /**
     * 查询还款记录列表
     * 
     * @param dbPayback 还款记录
     * @return 还款记录集合
     */
    public List<DbPayback> selectDbPaybackList(DbPayback dbPayback);

    /**
     * 新增还款记录
     * 
     * @param dbPayback 还款记录
     * @return 结果
     */
    public int insertDbPayback(DbPayback dbPayback);

    /**
     * 修改还款记录
     * 
     * @param dbPayback 还款记录
     * @return 结果
     */
    public int updateDbPayback(DbPayback dbPayback);

    /**
     * 删除还款记录
     * 
     * @param id 还款记录主键
     * @return 结果
     */
    public int deleteDbPaybackById(Long id);

    /**
     * 批量删除还款记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbPaybackByIds(Long[] ids);
}
