package com.ruoyi.finance.mapper;

import com.ruoyi.domain.DbSqdk;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 申请贷款Mapper接口
 * 
 * @author lql
 * @date 2023-03-14
 */
public interface DbSqdkMapper 
{
    /**
     * 查询申请贷款
     * 
     * @param id 申请贷款主键
     * @return 申请贷款
     */
    public DbSqdk selectDbSqdkById(Integer id);

    /**
     * 查询申请贷款列表
     * 
     * @param dbSqdk 申请贷款
     * @return 申请贷款集合
     */
    public List<DbSqdk> selectDbSqdkList(DbSqdk dbSqdk);

    /**
     * 新增申请贷款
     * 
     * @param dbSqdk 申请贷款
     * @return 结果
     */
    public int insertDbSqdk(DbSqdk dbSqdk);

    /**
     * 修改申请贷款
     * 
     * @param dbSqdk 申请贷款
     * @return 结果
     */
    public int updateDbSqdk(DbSqdk dbSqdk);

    /**
     * 删除申请贷款
     * 
     * @param id 申请贷款主键
     * @return 结果
     */
    public int deleteDbSqdkById(Integer id);

    /**
     * 批量删除申请贷款
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbSqdkByIds(Integer[] ids);
}
