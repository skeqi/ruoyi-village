package com.ruoyi.finance.service;

import com.ruoyi.domain.DbPayback;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 还款记录Service接口
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
public interface IDbPaybackService 
{
    /**
     * 查询还款记录
     * 
     * @param id 还款记录主键
     * @return 还款记录
     */
    public DbPayback selectDbPaybackById(Long id);

    /**
     * 查询还款记录列表
     * 
     * @param dbPayback 还款记录
     * @return 还款记录集合
     */
    public List<DbPayback> selectDbPaybackList(DbPayback dbPayback);

    /**
     * 新增还款记录
     * 
     * @param dbPayback 还款记录
     * @return 结果
     */
    public int insertDbPayback(DbPayback dbPayback);

    /**
     * 修改还款记录
     * 
     * @param dbPayback 还款记录
     * @return 结果
     */
    public int updateDbPayback(DbPayback dbPayback);

    /**
     * 批量删除还款记录
     * 
     * @param ids 需要删除的还款记录主键集合
     * @return 结果
     */
    public int deleteDbPaybackByIds(Long[] ids);

    /**
     * 删除还款记录信息
     * 
     * @param id 还款记录主键
     * @return 结果
     */
    public int deleteDbPaybackById(Long id);
}
