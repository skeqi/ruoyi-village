package com.ruoyi.finance.service.impl;

import java.util.List;

import com.ruoyi.domain.DbLoan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.finance.mapper.DbLoanMapper;
import com.ruoyi.finance.service.IDbLoanService;
import com.ruoyi.domain.*;

/**
 * 放款记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
@Service
public class DbLoanServiceImpl implements IDbLoanService 
{
    @Autowired
    private DbLoanMapper dbLoanMapper;

    /**
     * 查询放款记录
     * 
     * @param id 放款记录主键
     * @return 放款记录
     */
    @Override
    public DbLoan selectDbLoanById(Long id)
    {
        return dbLoanMapper.selectDbLoanById(id);
    }

    /**
     * 查询放款记录列表
     * 
     * @param dbLoan 放款记录
     * @return 放款记录
     */
    @Override
    public List<DbLoan> selectDbLoanList(DbLoan dbLoan)
    {
        return dbLoanMapper.selectDbLoanList(dbLoan);
    }

    /**
     * 新增放款记录
     * 
     * @param dbLoan 放款记录
     * @return 结果
     */
    @Override
    public int insertDbLoan(DbLoan dbLoan)
    {
        return dbLoanMapper.insertDbLoan(dbLoan);
    }

    /**
     * 修改放款记录
     * 
     * @param dbLoan 放款记录
     * @return 结果
     */
    @Override
    public int updateDbLoan(DbLoan dbLoan)
    {
        return dbLoanMapper.updateDbLoan(dbLoan);
    }

    /**
     * 批量删除放款记录
     * 
     * @param ids 需要删除的放款记录主键
     * @return 结果
     */
    @Override
    public int deleteDbLoanByIds(Long[] ids)
    {
        return dbLoanMapper.deleteDbLoanByIds(ids);
    }

    /**
     * 删除放款记录信息
     * 
     * @param id 放款记录主键
     * @return 结果
     */
    @Override
    public int deleteDbLoanById(Long id)
    {
        return dbLoanMapper.deleteDbLoanById(id);
    }
}
