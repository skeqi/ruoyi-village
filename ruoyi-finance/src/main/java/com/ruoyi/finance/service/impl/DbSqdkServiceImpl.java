package com.ruoyi.finance.service.impl;

import java.util.List;

import com.ruoyi.domain.DbSqdk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.finance.mapper.DbSqdkMapper;
import com.ruoyi.finance.service.IDbSqdkService;
import com.ruoyi.domain.*;

/**
 * 申请贷款Service业务层处理
 * 
 * @author lql
 * @date 2023-03-14
 */
@Service
public class DbSqdkServiceImpl implements IDbSqdkService 
{
    @Autowired
    private DbSqdkMapper dbSqdkMapper;

    /**
     * 查询申请贷款
     * 
     * @param id 申请贷款主键
     * @return 申请贷款
     */
    @Override
    public DbSqdk selectDbSqdkById(Integer id)
    {
        return dbSqdkMapper.selectDbSqdkById(id);
    }

    /**
     * 查询申请贷款列表
     * 
     * @param dbSqdk 申请贷款
     * @return 申请贷款
     */
    @Override
    public List<DbSqdk> selectDbSqdkList(DbSqdk dbSqdk)
    {
        return dbSqdkMapper.selectDbSqdkList(dbSqdk);
    }

    /**
     * 新增申请贷款
     * 
     * @param dbSqdk 申请贷款
     * @return 结果
     */
    @Override
    public int insertDbSqdk(DbSqdk dbSqdk)
    {
        return dbSqdkMapper.insertDbSqdk(dbSqdk);
    }

    /**
     * 修改申请贷款
     * 
     * @param dbSqdk 申请贷款
     * @return 结果
     */
    @Override
    public int updateDbSqdk(DbSqdk dbSqdk)
    {
        return dbSqdkMapper.updateDbSqdk(dbSqdk);
    }

    /**
     * 批量删除申请贷款
     * 
     * @param ids 需要删除的申请贷款主键
     * @return 结果
     */
    @Override
    public int deleteDbSqdkByIds(Integer[] ids)
    {
        return dbSqdkMapper.deleteDbSqdkByIds(ids);
    }

    /**
     * 删除申请贷款信息
     * 
     * @param id 申请贷款主键
     * @return 结果
     */
    @Override
    public int deleteDbSqdkById(Integer id)
    {
        return dbSqdkMapper.deleteDbSqdkById(id);
    }
}
