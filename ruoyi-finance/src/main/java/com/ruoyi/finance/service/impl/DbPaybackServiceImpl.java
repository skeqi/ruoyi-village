package com.ruoyi.finance.service.impl;

import java.util.List;

import com.ruoyi.domain.DbPayback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.finance.mapper.DbPaybackMapper;
import com.ruoyi.finance.service.IDbPaybackService;
import com.ruoyi.domain.*;

/**
 * 还款记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
@Service
public class DbPaybackServiceImpl implements IDbPaybackService 
{
    @Autowired
    private DbPaybackMapper dbPaybackMapper;

    /**
     * 查询还款记录
     * 
     * @param id 还款记录主键
     * @return 还款记录
     */
    @Override
    public DbPayback selectDbPaybackById(Long id)
    {
        return dbPaybackMapper.selectDbPaybackById(id);
    }

    /**
     * 查询还款记录列表
     * 
     * @param dbPayback 还款记录
     * @return 还款记录
     */
    @Override
    public List<DbPayback> selectDbPaybackList(DbPayback dbPayback)
    {
        return dbPaybackMapper.selectDbPaybackList(dbPayback);
    }

    /**
     * 新增还款记录
     * 
     * @param dbPayback 还款记录
     * @return 结果
     */
    @Override
    public int insertDbPayback(DbPayback dbPayback)
    {
        return dbPaybackMapper.insertDbPayback(dbPayback);
    }

    /**
     * 修改还款记录
     * 
     * @param dbPayback 还款记录
     * @return 结果
     */
    @Override
    public int updateDbPayback(DbPayback dbPayback)
    {
        return dbPaybackMapper.updateDbPayback(dbPayback);
    }

    /**
     * 批量删除还款记录
     * 
     * @param ids 需要删除的还款记录主键
     * @return 结果
     */
    @Override
    public int deleteDbPaybackByIds(Long[] ids)
    {
        return dbPaybackMapper.deleteDbPaybackByIds(ids);
    }

    /**
     * 删除还款记录信息
     * 
     * @param id 还款记录主键
     * @return 结果
     */
    @Override
    public int deleteDbPaybackById(Long id)
    {
        return dbPaybackMapper.deleteDbPaybackById(id);
    }
}
