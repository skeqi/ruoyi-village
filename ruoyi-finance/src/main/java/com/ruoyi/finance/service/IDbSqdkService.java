package com.ruoyi.finance.service;

import com.ruoyi.domain.DbSqdk;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 申请贷款Service接口
 * 
 * @author lql
 * @date 2023-03-14
 */
public interface IDbSqdkService 
{
    /**
     * 查询申请贷款
     * 
     * @param id 申请贷款主键
     * @return 申请贷款
     */
    public DbSqdk selectDbSqdkById(Integer id);

    /**
     * 查询申请贷款列表
     * 
     * @param dbSqdk 申请贷款
     * @return 申请贷款集合
     */
    public List<DbSqdk> selectDbSqdkList(DbSqdk dbSqdk);

    /**
     * 新增申请贷款
     * 
     * @param dbSqdk 申请贷款
     * @return 结果
     */
    public int insertDbSqdk(DbSqdk dbSqdk);

    /**
     * 修改申请贷款
     * 
     * @param dbSqdk 申请贷款
     * @return 结果
     */
    public int updateDbSqdk(DbSqdk dbSqdk);

    /**
     * 批量删除申请贷款
     * 
     * @param ids 需要删除的申请贷款主键集合
     * @return 结果
     */
    public int deleteDbSqdkByIds(Integer[] ids);

    /**
     * 删除申请贷款信息
     * 
     * @param id 申请贷款主键
     * @return 结果
     */
    public int deleteDbSqdkById(Integer id);
}
