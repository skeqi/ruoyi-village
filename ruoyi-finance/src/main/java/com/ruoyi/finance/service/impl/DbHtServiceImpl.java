package com.ruoyi.finance.service.impl;

import java.util.List;

import com.ruoyi.domain.DbHt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.finance.mapper.DbHtMapper;
import com.ruoyi.finance.service.IDbHtService;
import com.ruoyi.domain.*;

/**
 * 签署合同Service业务层处理
 * 
 * @author lql
 * @date 2023-03-14
 */
@Service
public class DbHtServiceImpl implements IDbHtService 
{
    @Autowired
    private DbHtMapper dbHtMapper;

    /**
     * 查询签署合同
     * 
     * @param id 签署合同主键
     * @return 签署合同
     */
    @Override
    public DbHt selectDbHtById(Long id)
    {
        return dbHtMapper.selectDbHtById(id);
    }

    /**
     * 查询签署合同列表
     * 
     * @param dbHt 签署合同
     * @return 签署合同
     */
    @Override
    public List<DbHt> selectDbHtList(DbHt dbHt)
    {
        return dbHtMapper.selectDbHtList(dbHt);
    }

    /**
     * 新增签署合同
     * 
     * @param dbHt 签署合同
     * @return 结果
     */
    @Override
    public int insertDbHt(DbHt dbHt)
    {
        return dbHtMapper.insertDbHt(dbHt);
    }

    /**
     * 修改签署合同
     * 
     * @param dbHt 签署合同
     * @return 结果
     */
    @Override
    public int updateDbHt(DbHt dbHt)
    {
        return dbHtMapper.updateDbHt(dbHt);
    }

    /**
     * 批量删除签署合同
     * 
     * @param ids 需要删除的签署合同主键
     * @return 结果
     */
    @Override
    public int deleteDbHtByIds(Long[] ids)
    {
        return dbHtMapper.deleteDbHtByIds(ids);
    }

    /**
     * 删除签署合同信息
     * 
     * @param id 签署合同主键
     * @return 结果
     */
    @Override
    public int deleteDbHtById(Long id)
    {
        return dbHtMapper.deleteDbHtById(id);
    }
}
