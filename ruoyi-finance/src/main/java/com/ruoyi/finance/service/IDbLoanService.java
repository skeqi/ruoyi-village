package com.ruoyi.finance.service;

import com.ruoyi.domain.DbLoan;
import com.ruoyi.domain.*;

import java.util.List;

/**
 * 放款记录Service接口
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
public interface IDbLoanService 
{
    /**
     * 查询放款记录
     * 
     * @param id 放款记录主键
     * @return 放款记录
     */
    public DbLoan selectDbLoanById(Long id);

    /**
     * 查询放款记录列表
     * 
     * @param dbLoan 放款记录
     * @return 放款记录集合
     */
    public List<DbLoan> selectDbLoanList(DbLoan dbLoan);

    /**
     * 新增放款记录
     * 
     * @param dbLoan 放款记录
     * @return 结果
     */
    public int insertDbLoan(DbLoan dbLoan);

    /**
     * 修改放款记录
     * 
     * @param dbLoan 放款记录
     * @return 结果
     */
    public int updateDbLoan(DbLoan dbLoan);

    /**
     * 批量删除放款记录
     * 
     * @param ids 需要删除的放款记录主键集合
     * @return 结果
     */
    public int deleteDbLoanByIds(Long[] ids);

    /**
     * 删除放款记录信息
     * 
     * @param id 放款记录主键
     * @return 结果
     */
    public int deleteDbLoanById(Long id);
}
