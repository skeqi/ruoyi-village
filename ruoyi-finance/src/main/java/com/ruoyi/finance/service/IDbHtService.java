package com.ruoyi.finance.service;

import com.ruoyi.domain.DbHt;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 签署合同Service接口
 * 
 * @author lql
 * @date 2023-03-14
 */
public interface IDbHtService 
{
    /**
     * 查询签署合同
     * 
     * @param id 签署合同主键
     * @return 签署合同
     */
    public DbHt selectDbHtById(Long id);

    /**
     * 查询签署合同列表
     * 
     * @param dbHt 签署合同
     * @return 签署合同集合
     */
    public List<DbHt> selectDbHtList(DbHt dbHt);

    /**
     * 新增签署合同
     * 
     * @param dbHt 签署合同
     * @return 结果
     */
    public int insertDbHt(DbHt dbHt);

    /**
     * 修改签署合同
     * 
     * @param dbHt 签署合同
     * @return 结果
     */
    public int updateDbHt(DbHt dbHt);

    /**
     * 批量删除签署合同
     * 
     * @param ids 需要删除的签署合同主键集合
     * @return 结果
     */
    public int deleteDbHtByIds(Long[] ids);

    /**
     * 删除签署合同信息
     * 
     * @param id 签署合同主键
     * @return 结果
     */
    public int deleteDbHtById(Long id);
}
