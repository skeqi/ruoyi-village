import request from '@/utils/request'

// 查询业主信息列表
export function listOwner(query) {
  return request({
    url: '/owner/owner/list',
    method: 'get',
    params: query
  })
}

// 查询业主信息详细
export function getOwner(id) {
  return request({
    url: '/owner/owner/' + id,
    method: 'get'
  })
}

// 新增业主信息
export function addOwner(data) {
  return request({
    url: '/owner/owner',
    method: 'post',
    data: data
  })
}

// 修改业主信息
export function updateOwner(data) {
  return request({
    url: '/owner/owner',
    method: 'put',
    data: data
  })
}

// 删除业主信息
export function delOwner(id) {
  return request({
    url: '/owner/owner/' + id,
    method: 'delete'
  })
}

/*
房屋号绑定
 */
export function findho() {
  return request({
    url: '/owner/owner/findho',
    method: 'get'
  })
}
