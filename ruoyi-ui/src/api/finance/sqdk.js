import request from '@/utils/request'

// 查询申请贷款列表
export function listSqdk(query) {
  return request({
    url: '/finance/sqdk/list',
    method: 'get',
    params: query
  })
}

// 查询申请贷款详细
export function getSqdk(id) {
  return request({
    url: '/finance/sqdk/' + id,
    method: 'get'
  })
}

// 新增申请贷款
export function addSqdk(data) {
  return request({
    url: '/finance/sqdk',
    method: 'post',
    data: data
  })
}

// 修改申请贷款
export function updateSqdk(data) {
  return request({
    url: '/finance/sqdk',
    method: 'put',
    data: data
  })
}

// 删除申请贷款
export function delSqdk(id) {
  return request({
    url: '/finance/sqdk/' + id,
    method: 'delete'
  })
}
