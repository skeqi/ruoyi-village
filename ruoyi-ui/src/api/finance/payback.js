import request from '@/utils/request'

// 查询还款记录列表
export function listPayback(query) {
  return request({
    url: '/finance/payback/list',
    method: 'get',
    params: query
  })
}

// 查询还款记录详细
export function getPayback(id) {
  return request({
    url: '/finance/payback/' + id,
    method: 'get'
  })
}

// 新增还款记录
export function addPayback(data) {
  return request({
    url: '/finance/payback',
    method: 'post',
    data: data
  })
}

// 修改还款记录
export function updatePayback(data) {
  return request({
    url: '/finance/payback',
    method: 'put',
    data: data
  })
}

// 删除还款记录
export function delPayback(id) {
  return request({
    url: '/finance/payback/' + id,
    method: 'delete'
  })
}
