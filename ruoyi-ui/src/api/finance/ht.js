import request from '@/utils/request'

// 查询签署合同列表
export function listHt(query) {
  return request({
    url: '/finance/ht/list',
    method: 'get',
    params: query
  })
}

// 查询签署合同详细
export function getHt(id) {
  return request({
    url: '/finance/ht/' + id,
    method: 'get'
  })
}

// 新增签署合同
export function addHt(data) {
  return request({
    url: '/finance/ht',
    method: 'post',
    data: data
  })
}

// 修改签署合同
export function updateHt(data) {
  return request({
    url: '/finance/ht',
    method: 'put',
    data: data
  })
}

// 删除签署合同
export function delHt(id) {
  return request({
    url: '/finance/ht/' + id,
    method: 'delete'
  })
}
