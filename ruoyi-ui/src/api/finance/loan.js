import request from '@/utils/request'

// 查询放款记录列表
export function listLoan(query) {
  return request({
    url: '/finance/loan/list',
    method: 'get',
    params: query
  })
}

// 查询放款记录详细
export function getLoan(id) {
  return request({
    url: '/finance/loan/' + id,
    method: 'get'
  })
}

// 新增放款记录
export function addLoan(data) {
  return request({
    url: '/finance/loan',
    method: 'post',
    data: data
  })
}

// 修改放款记录
export function updateLoan(data) {
  return request({
    url: '/finance/loan',
    method: 'put',
    data: data
  })
}

// 删除放款记录
export function delLoan(id) {
  return request({
    url: '/finance/loan/' + id,
    method: 'delete'
  })
}
