import request from '@/utils/request'

// 查询员工考勤列表
export function listYgkq(query) {
  return request({
    url: '/ygkq/ygkq/list',
    method: 'get',
    params: query
  })
}

// 查询员工考勤详细
export function getYgkq(staffId) {
  return request({
    url: '/ygkq/ygkq/' + staffId,
    method: 'get'
  })
}

// 新增员工考勤
export function addYgkq(data) {
  return request({
    url: '/ygkq/ygkq',
    method: 'post',
    data: data
  })
}

// 修改员工考勤
export function updateYgkq(data) {
  return request({
    url: '/ygkq/ygkq',
    method: 'put',
    data: data
  })
}

// 删除员工考勤
export function delYgkq(staffId) {
  return request({
    url: '/ygkq/ygkq/' + staffId,
    method: 'delete'
  })
}
