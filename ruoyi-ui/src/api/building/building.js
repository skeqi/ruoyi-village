import request from '@/utils/request'

// 查询楼栋管理列表
export function listBuilding(query) {
  return request({
    url: '/building/building/list',
    method: 'get',
    params: query
  })
}

// 查询楼栋管理详细
export function getBuilding(buidingid) {
  return request({
    url: '/building/building/' + buidingid,
    method: 'get'
  })
}

// 新增楼栋管理
export function addBuilding(data) {
  return request({
    url: '/building/building',
    method: 'post',
    data: data
  })
}

// 修改楼栋管理
export function updateBuilding(data) {
  return request({
    url: '/building/building',
    method: 'put',
    data: data
  })
}

// 删除楼栋管理
export function delBuilding(buidingid) {
  return request({
    url: '/building/building/' + buidingid,
    method: 'delete'
  })
}

export function getAnBao() {
  return request({
    url: '/building/building/AnBao',
    method: 'get'
  })
}

export function getFuWu() {
  return request({
    url: '/building/building/FuWu',
    method: 'get'
  })
}
