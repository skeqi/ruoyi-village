import request from '@/utils/request'

// 查询员工信息1列表
export function listYgb(query) {
  return request({
    url: '/ygb/ygb/list',
    method: 'get',
    params: query
  })
}

// 查询员工信息1详细
export function getYgb(staffId) {
  return request({
    url: '/ygb/ygb/' + staffId,
    method: 'get'
  })
}

// 新增员工信息1
export function addYgb(data) {
  return request({
    url: '/ygb/ygb',
    method: 'post',
    data: data
  })
}

// 修改员工信息1
export function updateYgb(data) {
  return request({
    url: '/ygb/ygb',
    method: 'put',
    data: data
  })
}

// 删除员工信息1
export function delYgb(staffId) {
  return request({
    url: '/ygb/ygb/' + staffId,
    method: 'delete'
  })
}
