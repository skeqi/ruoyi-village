import request from '@/utils/request'

// 查询疫苗接种情况列表
export function listYmzk(query) {
  return request({
    url: '/ymzk/ymzk/list',
    method: 'get',
    params: query
  })
}

// 查询疫苗接种情况详细
export function getYmzk(ymzkId) {
  return request({
    url: '/ymzk/ymzk/' + ymzkId,
    method: 'get'
  })
}

// 新增疫苗接种情况
export function addYmzk(data) {
  return request({
    url: '/ymzk/ymzk',
    method: 'post',
    data: data
  })
}

// 修改疫苗接种情况
export function updateYmzk(data) {
  return request({
    url: '/ymzk/ymzk',
    method: 'put',
    data: data
  })
}

// 删除疫苗接种情况
export function delYmzk(ymzkId) {
  return request({
    url: '/ymzk/ymzk/' + ymzkId,
    method: 'delete'
  })
}
