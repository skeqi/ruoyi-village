import request from '@/utils/request'

// 查询维修员工列表
export function listXuser(query) {
  return request({
    url: '/repair/xuser/list',
    method: 'get',
    params: query
  })
}

export function listUser(query) {
  return request({
    url: '/repair/xuser/list1',
    method: 'get',
    params: query
  })
}

// 查询维修员工详细
export function getXuser(staffId) {
  return request({
    url: '/repair/xuser/' + staffId,
    method: 'get'
  })
}

// 新增维修员工
export function addXuser(data) {
  return request({
    url: '/repair/xuser',
    method: 'post',
    data: data
  })
}

// 修改维修员工
export function updateXuser(data) {
  return request({
    url: '/repair/xuser',
    method: 'put',
    data: data
  })
}

// 删除维修员工
export function delXuser(staffId) {
  return request({
    url: '/repair/xuser/' + staffId,
    method: 'delete'
  })
}
