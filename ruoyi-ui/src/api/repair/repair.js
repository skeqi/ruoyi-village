import request from '@/utils/request'

// 查询报修信息列表
export function listRepair(query) {
  return request({
    url: '/repair/repair/list',
    method: 'get',
    params: query
  })
}

// 查询报修信息详细
export function getRepair(id) {
  return request({
    url: '/repair/repair/' + id,
    method: 'get'
  })
}

// 新增报修信息
export function addRepair(data) {
  return request({
    url: '/repair/repair',
    method: 'post',
    data: data
  })
}

// 修改报修信息
export function updateRepair(data) {
  return request({
    url: '/repair/repair',
    method: 'put',
    data: data
  })
}

// 删除报修信息
export function delRepair(id) {
  return request({
    url: '/repair/repair/' + id,
    method: 'delete'
  })
}

export function listStaffUid(id) {
  return request({
    url: '/repair/repair/' + id,
    method: 'get'
  })
}

export function repairList() {
  return request({
    url: '/repair/repair/repairList',
    method: 'get'
  })
}
