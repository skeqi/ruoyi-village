import request from '@/utils/request'

// 查询返省登记列表
export function listFsdj(query) {
  return request({
    url: '/fsdj/fsdj/list',
    method: 'get',
    params: query
  })
}

// 查询返省登记详细
export function getFsdj(id) {
  return request({
    url: '/fsdj/fsdj/' + id,
    method: 'get'
  })
}

// 新增返省登记
export function addFsdj(data) {
  return request({
    url: '/fsdj/fsdj',
    method: 'post',
    data: data
  })
}

// 修改返省登记
export function updateFsdj(data) {
  return request({
    url: '/fsdj/fsdj',
    method: 'put',
    data: data
  })
}

// 删除返省登记
export function delFsdj(id) {
  return request({
    url: '/fsdj/fsdj/' + id,
    method: 'delete'
  })
}
