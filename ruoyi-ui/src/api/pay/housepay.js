import request from '@/utils/request'
//计算费用
export function calculate(query) {
  return request({
    url : '/pay/housepay/calculate',
    method : 'get',
    params : query
  })
}

// 查询房屋缴费列表
export function listHousepay(query) {
  return request({
    url: '/pay/housepay/list',
    method: 'get',
    params: query
  })
}

// 查询房屋缴费详细
export function getHousepay(id) {
  return request({
    url: '/pay/housepay/' + id,
    method: 'get'
  })
}

// 新增房屋缴费
export function addHousepay(data) {
  return request({
    url: '/pay/housepay',
    method: 'post',
    data: data
  })
}

// 修改房屋缴费
export function updateHousepay(data) {
  return request({
    url: '/pay/housepay',
    method: 'put',
    data: data
  })
}

// 删除房屋缴费
export function delHousepay(id) {
  return request({
    url: '/pay/housepay/' + id,
    method: 'delete'
  })
}
