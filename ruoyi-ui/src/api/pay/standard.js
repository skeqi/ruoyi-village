import request from '@/utils/request'
export function getByType(query) {
  return request({
    url: '/pay/standard/getByType',
    method: 'get',
    params: query
  })
}

// 查询收费标准列表
export function listStandard(query) {
  return request({
    url: '/pay/standard/list',
    method: 'get',
    params: query
  })
}

// 查询收费标准详细
export function getStandard(id) {
  return request({
    url: '/pay/standard/' + id,
    method: 'get'
  })
}

// 新增收费标准
export function addStandard(data) {
  return request({
    url: '/pay/standard',
    method: 'post',
    data: data
  })
}

// 修改收费标准
export function updateStandard(data) {
  return request({
    url: '/pay/standard',
    method: 'put',
    data: data
  })
}

// 删除收费标准
export function delStandard(id) {
  return request({
    url: '/pay/standard/' + id,
    method: 'delete'
  })
}
