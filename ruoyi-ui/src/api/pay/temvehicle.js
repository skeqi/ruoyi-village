import request from '@/utils/request'

// 查询临时车收费标准列表
export function listTemvehicle(query) {
  return request({
    url: '/pay/temvehicle/list',
    method: 'get',
    params: query
  })
}

// 查询临时车收费标准详细
export function getTemvehicle(id) {
  return request({
    url: '/pay/temvehicle/' + id,
    method: 'get'
  })
}

// 新增临时车收费标准
export function addTemvehicle(data) {
  return request({
    url: '/pay/temvehicle',
    method: 'post',
    data: data
  })
}

// 修改临时车收费标准
export function updateTemvehicle(data) {
  return request({
    url: '/pay/temvehicle',
    method: 'put',
    data: data
  })
}

// 删除临时车收费标准
export function delTemvehicle(id) {
  return request({
    url: '/pay/temvehicle/' + id,
    method: 'delete'
  })
}
