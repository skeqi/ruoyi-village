import request from '@/utils/request'

// 查询账单催缴列表
export function listCall(query) {
  return request({
    url: '/pay/call/list',
    method: 'get',
    params: query
  })
}

// 查询账单催缴详细
export function getCall(id) {
  return request({
    url: '/pay/call/' + id,
    method: 'get'
  })
}

// 新增账单催缴
export function addCall(data) {
  return request({
    url: '/pay/call',
    method: 'post',
    data: data
  })
}

// 修改账单催缴
export function updateCall(data) {
  return request({
    url: '/pay/call',
    method: 'put',
    data: data
  })
}

// 删除账单催缴
export function delCall(id) {
  return request({
    url: '/pay/call/' + id,
    method: 'delete'
  })
}
