import request from '@/utils/request'

// 查询临时车缴费列表
export function listParking(query) {
  return request({
    url: '/pay/parking/list',
    method: 'get',
    params: query
  })
}

// 查询临时车缴费详细
export function getParking(id) {
  return request({
    url: '/pay/parking/' + id,
    method: 'get'
  })
}

// 新增临时车缴费
export function addParking(data) {
  return request({
    url: '/pay/parking',
    method: 'post',
    data: data
  })
}

// 修改临时车缴费
export function updateParking(data) {
  return request({
    url: '/pay/parking',
    method: 'put',
    data: data
  })
}

// 删除临时车缴费
export function delParking(id) {
  return request({
    url: '/pay/parking/' + id,
    method: 'delete'
  })
}
