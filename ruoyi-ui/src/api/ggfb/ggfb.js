import request from '@/utils/request'

// 查询公告发布列表
export function listGgfb(query) {
  return request({
    url: '/ggfb/ggfb/list',
    method: 'get',
    params: query
  })
}

// 查询公告发布详细
export function getGgfb(bulletinId) {
  return request({
    url: '/ggfb/ggfb/' + bulletinId,
    method: 'get'
  })
}

// 新增公告发布
export function addGgfb(data) {
  return request({
    url: '/ggfb/ggfb',
    method: 'post',
    data: data
  })
}

// 修改公告发布
export function updateGgfb(data) {
  return request({
    url: '/ggfb/ggfb',
    method: 'put',
    data: data
  })
}

// 删除公告发布
export function delGgfb(bulletinId) {
  return request({
    url: '/ggfb/ggfb/' + bulletinId,
    method: 'delete'
  })
}
