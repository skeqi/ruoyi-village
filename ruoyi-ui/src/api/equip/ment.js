import request from '@/utils/request'

// 查询设备信息列表
export function listMent(query) {
  return request({
    url: '/equip/ment/list',
    method: 'get',
    params: query
  })
}

// 查询设备信息详细
export function getMent(id) {
  return request({
    url: '/equip/ment/' + id,
    method: 'get'
  })
}

// 新增设备信息
export function addMent(data) {
  return request({
    url: '/equip/ment',
    method: 'post',
    data: data
  })
}

// 修改设备信息
export function updateMent(data) {
  return request({
    url: '/equip/ment',
    method: 'put',
    data: data
  })
}

// 删除设备信息
export function delMent(id) {
  return request({
    url: '/equip/ment/' + id,
    method: 'delete'
  })
}

export function repairList() {
  return request({
    url: '/equip/ment/repairList',
    method: 'get'
  })
}
