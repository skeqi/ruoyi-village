import request from '@/utils/request'

// 查询临时车管理列表
export function listTemporary(query) {
  return request({
    url: '/temporary/temporary/list',
    method: 'get',
    params: query
  })
}

// 查询临时车管理详细
export function getTemporary(id) {
  return request({
    url: '/temporary/temporary/' + id,
    method: 'get'
  })
}

// 新增临时车管理
export function addTemporary(data) {
  return request({
    url: '/temporary/temporary',
    method: 'post',
    data: data
  })
}

// 修改临时车管理
export function updateTemporary(data) {
  return request({
    url: '/temporary/temporary',
    method: 'put',
    data: data
  })
}

// 删除临时车管理
export function delTemporary(id) {
  return request({
    url: '/temporary/temporary/' + id,
    method: 'delete'
  })
}
