import request from '@/utils/request'

// 查询员工职业列表
export function listYgzy(query) {
  return request({
    url: '/ygzy/ygzy/list',
    method: 'get',
    params: query
  })
}

// 查询员工职业详细
export function getYgzy(staffId) {
  return request({
    url: '/ygzy/ygzy/' + staffId,
    method: 'get'
  })
}

// 新增员工职业
export function addYgzy(data) {
  return request({
    url: '/ygzy/ygzy',
    method: 'post',
    data: data
  })
}

// 修改员工职业
export function updateYgzy(data) {
  return request({
    url: '/ygzy/ygzy',
    method: 'put',
    data: data
  })
}

// 删除员工职业
export function delYgzy(staffId) {
  return request({
    url: '/ygzy/ygzy/' + staffId,
    method: 'delete'
  })
}
