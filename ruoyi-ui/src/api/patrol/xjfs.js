import request from '@/utils/request'

// 查询巡检任务详情列表
export function listXjfs(query) {
  return request({
    url: '/xjfs/xjfs/list',
    method: 'get',
    params: query
  })
}

// 查询巡检任务详情详细
export function getXjfs(id) {
  return request({
    url: '/xjfs/xjfs/' + id,
    method: 'get'
  })
}

// 新增巡检任务详情
export function addXjfs(data) {
  return request({
    url: '/xjfs/xjfs',
    method: 'post',
    data: data
  })
}

// 修改巡检任务详情
export function updateXjfs(data) {
  return request({
    url: '/xjfs/xjfs',
    method: 'put',
    data: data
  })
}

// 删除巡检任务详情
export function delXjfs(id) {
  return request({
    url: '/xjfs/xjfs/' + id,
    method: 'delete'
  })
}
