import request from '@/utils/request'

// 查询巡检任务列表
export function listTask(query) {
  return request({
    url: '/patrol/task/list',
    method: 'get',
    params: query
  })
}

// 查询巡检任务详细
export function getTask(id) {
  return request({
    url: '/patrol/task/' + id,
    method: 'get'
  })
}

// 新增巡检任务
export function addTask(data) {
  return request({
    url: '/patrol/task',
    method: 'post',
    data: data
  })
}

// 修改巡检任务
export function updateTask(data) {
  return request({
    url: '/patrol/task',
    method: 'put',
    data: data
  })
}

// 删除巡检任务
export function delTask(id) {
  return request({
    url: '/patrol/task/' + id,
    method: 'delete'
  })
}

export function repList() {
  return request({
    url: '/patrol/task/repList',
    method: 'get'
  })
}
