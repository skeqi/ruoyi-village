import request from '@/utils/request'

// 查询来访登记列表
export function listLfdj(query) {
  return request({
    url: '/lfdj/lfdj/list',
    method: 'get',
    params: query
  })
}

// 查询来访登记详细
export function getLfdj(lfdjId) {
  return request({
    url: '/lfdj/lfdj/' + lfdjId,
    method: 'get'
  })
}

// 新增来访登记
export function addLfdj(data) {
  return request({
    url: '/lfdj/lfdj',
    method: 'post',
    data: data
  })
}

// 修改来访登记
export function updateLfdj(data) {
  return request({
    url: '/lfdj/lfdj',
    method: 'put',
    data: data
  })
}

// 删除来访登记
export function delLfdj(lfdjId) {
  return request({
    url: '/lfdj/lfdj/' + lfdjId,
    method: 'delete'
  })
}
