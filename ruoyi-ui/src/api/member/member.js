import request from '@/utils/request'

// 查询业主家庭成员列表
export function listMember(query) {
  return request({
    url: '/member/member/list',
    method: 'get',
    params: query
  })
}

// 查询业主家庭成员详细
export function getMember(id) {
  return request({
    url: '/member/member/' + id,
    method: 'get'
  })
}

// 新增业主家庭成员
export function addMember(data) {
  return request({
    url: '/member/member',
    method: 'post',
    data: data
  })
}

// 修改业主家庭成员
export function updateMember(data) {
  return request({
    url: '/member/member',
    method: 'put',
    data: data
  })
}

// 删除业主家庭成员
export function delMember(id) {
  return request({
    url: '/member/member/' + id,
    method: 'delete'
  })
}

/*
绑定业主姓名
 */
export function findow() {
  return request({
    url:'/member/member/finder',
    method:'get'
  })
}
