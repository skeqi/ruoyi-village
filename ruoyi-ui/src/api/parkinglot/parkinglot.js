import request from '@/utils/request'

// 查询停车场列表
export function listParkinglot(query) {
  return request({
    url: '/parkinglot/parkinglot/list',
    method: 'get',
    params: query
  })
}

// 查询停车场详细
export function getParkinglot(id) {
  return request({
    url: '/parkinglot/parkinglot/' + id,
    method: 'get'
  })
}

// 新增停车场
export function addParkinglot(data) {
  return request({
    url: '/parkinglot/parkinglot',
    method: 'post',
    data: data
  })
}

// 修改停车场
export function updateParkinglot(data) {
  return request({
    url: '/parkinglot/parkinglot',
    method: 'put',
    data: data
  })
}

// 删除停车场
export function delParkinglot(id) {
  return request({
    url: '/parkinglot/parkinglot/' + id,
    method: 'delete'
  })
}
