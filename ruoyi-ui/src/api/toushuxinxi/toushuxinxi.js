import request from '@/utils/request'

// 查询投诉管理列表
export function listToushuxinxi(query) {
  return request({
    url: '/toushuxinxi/toushuxinxi/list',
    method: 'get',
    params: query
  })
}

// 查询投诉管理详细
export function getToushuxinxi(complaintId) {
  return request({
    url: '/toushuxinxi/toushuxinxi/' + complaintId,
    method: 'get'
  })
}

// 新增投诉管理
export function addToushuxinxi(data) {
  return request({
    url: '/toushuxinxi/toushuxinxi',
    method: 'post',
    data: data
  })
}

// 修改投诉管理
export function updateToushuxinxi(data) {
  return request({
    url: '/toushuxinxi/toushuxinxi',
    method: 'put',
    data: data
  })
}

// 删除投诉管理
export function delToushuxinxi(complaintId) {
  return request({
    url: '/toushuxinxi/toushuxinxi/' + complaintId,
    method: 'delete'
  })
}


export function owner() {
  return request({
    url: '/toushuxinxi/toushuxinxi/owner',
    method: 'get'
  })
}

