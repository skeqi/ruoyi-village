import request from '@/utils/request'

// 查询设备信息列表
export function buildingCount() {
  return request({
    url: '/buildingCount',
    method: 'get',
  })
}

//
//   //    //房屋总数
// //    public Long housesCount();
// @GetMapping("/housesCount")
//   public AjaxResult housesCount() {
//     return AjaxResult.success(chartsService.housesCount());
//   }
export function housesCount() {
  return request({
    url: '/housesCount',
    method: 'get',
  })
}

//
//   //
// //    //投诉总数
// //    public Long tousuCount();
// @GetMapping("/tousuCount")
//   public AjaxResult tousuCount() {
//     return AjaxResult.success(chartsService.tousuCount());
//   }
//
export function tousuCount() {
  return request({
    url: '/tousuCount',
    method: 'get',
  })
}

//   //
// //    //维修总数
// //    public Long repairCount();
// @GetMapping("/repairCount")
//   public AjaxResult repairCount() {
//     return AjaxResult.success(chartsService.repairCount());
//   }
export function repairCount() {
  return request({
    url: '/repairCount',
    method: 'get',
  })
}

//
//   //    //商铺总数
// //    public Long shopsCount();
// @GetMapping("/shopsCount")
//   public AjaxResult shopsCount() {
//     return AjaxResult.success(chartsService.shopsCount());
//   }
//
export function shopsCount() {
  return request({
    url: '/shopsCount',
    method: 'get',
  })
}

//   //
// //    //车辆总数
// //    public Long vehicleCount();
// @GetMapping("/vehicleCount")
//   public AjaxResult vehicleCount() {
//     return AjaxResult.success(chartsService.vehicleCount());
//   }
//
export function vehicleCount() {
  return request({
    url: '/vehicleCount',
    method: 'get',
  })
}

//   //
// //    //车位总数
// //    public Long parkingCount();
// @GetMapping("/parkingCount")
//   public AjaxResult parkingCount() {
//     return AjaxResult.success(chartsService.parkingCount());
//   }
//
export function parkingCount() {
  return request({
    url: '/parkingCount',
    method: 'get',
  })
}

//   //
// //    //业主总数
// //    public Long ownerCount();
// @GetMapping("/ownerCount")
//   public AjaxResult ownerCount() {
//     return AjaxResult.success(chartsService.ownerCount());
//   }
export function ownerCount() {
  return request({
    url: '/ownerCount',
    method: 'get',
  })
}

