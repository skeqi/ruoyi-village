import request from '@/utils/request'

// 查询员工职业列表
export function listYuangongzhizhe(query) {
  return request({
    url: '/yuangongzhizhe/yuangongzhizhe/list',
    method: 'get',
    params: query
  })
}

// 查询员工职业详细
export function getYuangongzhizhe(staffId) {
  return request({
    url: '/yuangongzhizhe/yuangongzhizhe/' + staffId,
    method: 'get'
  })
}

// 新增员工职业
export function addYuangongzhizhe(data) {
  return request({
    url: '/yuangongzhizhe/yuangongzhizhe',
    method: 'post',
    data: data
  })
}

// 修改员工职业
export function updateYuangongzhizhe(data) {
  return request({
    url: '/yuangongzhizhe/yuangongzhizhe',
    method: 'put',
    data: data
  })
}

// 删除员工职业
export function delYuangongzhizhe(staffId) {
  return request({
    url: '/yuangongzhizhe/yuangongzhizhe/' + staffId,
    method: 'delete'
  })
}
