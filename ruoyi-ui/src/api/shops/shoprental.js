import request from '@/utils/request'

// 查询商铺出租列表
export function listShoprental(query) {
  return request({
    url: '/shops/shoprental/list',
    method: 'get',
    params: query
  })
}

// 查询商铺出租详细
export function getShoprental(id) {
  return request({
    url: '/shops/shoprental/' + id,
    method: 'get'
  })
}

// 新增商铺出租
export function addShoprental(data) {
  return request({
    url: '/shops/shoprental',
    method: 'post',
    data: data
  })
}

// 修改商铺出租
export function updateShoprental(data) {
  return request({
    url: '/shops/shoprental',
    method: 'put',
    data: data
  })
}

// 删除商铺出租
export function delShoprental(id) {
  return request({
    url: '/shops/shoprental/' + id,
    method: 'delete'
  })
}
