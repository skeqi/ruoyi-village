import request from '@/utils/request'

// 查询信息发布列表
export function listXxfb(query) {
  return request({
    url: '/xxfb/xxfb/list',
    method: 'get',
    params: query
  })
}

// 查询信息发布详细
export function getXxfb(xxfbId) {
  return request({
    url: '/xxfb/xxfb/' + xxfbId,
    method: 'get'
  })
}

// 新增信息发布
export function addXxfb(data) {
  return request({
    url: '/xxfb/xxfb',
    method: 'post',
    data: data
  })
}

// 修改信息发布
export function updateXxfb(data) {
  return request({
    url: '/xxfb/xxfb',
    method: 'put',
    data: data
  })
}

// 删除信息发布
export function delXxfb(xxfbId) {
  return request({
    url: '/xxfb/xxfb/' + xxfbId,
    method: 'delete'
  })
}
