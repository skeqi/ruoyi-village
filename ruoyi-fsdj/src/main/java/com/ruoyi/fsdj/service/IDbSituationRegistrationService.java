package com.ruoyi.fsdj.service;

import java.util.List;
import com.ruoyi.domain.DbSituationRegistration;

/**
 * 返省登记Service接口
 * 
 * @author koilty
 * @date 2023-03-14
 */
public interface IDbSituationRegistrationService 
{
    /**
     * 查询返省登记
     * 
     * @param id 返省登记主键
     * @return 返省登记
     */
    public DbSituationRegistration selectDbSituationRegistrationById(Long id);

    /**
     * 查询返省登记列表
     * 
     * @param dbSituationRegistration 返省登记
     * @return 返省登记集合
     */
    public List<DbSituationRegistration> selectDbSituationRegistrationList(DbSituationRegistration dbSituationRegistration);

    /**
     * 新增返省登记
     * 
     * @param dbSituationRegistration 返省登记
     * @return 结果
     */
    public int insertDbSituationRegistration(DbSituationRegistration dbSituationRegistration);

    /**
     * 修改返省登记
     * 
     * @param dbSituationRegistration 返省登记
     * @return 结果
     */
    public int updateDbSituationRegistration(DbSituationRegistration dbSituationRegistration);

    /**
     * 批量删除返省登记
     * 
     * @param ids 需要删除的返省登记主键集合
     * @return 结果
     */
    public int deleteDbSituationRegistrationByIds(Long[] ids);

    /**
     * 删除返省登记信息
     * 
     * @param id 返省登记主键
     * @return 结果
     */
    public int deleteDbSituationRegistrationById(Long id);
}
