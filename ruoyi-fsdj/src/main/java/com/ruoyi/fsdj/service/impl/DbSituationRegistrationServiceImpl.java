package com.ruoyi.fsdj.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fsdj.mapper.DbSituationRegistrationMapper;
import com.ruoyi.domain.DbSituationRegistration;
import com.ruoyi.fsdj.service.IDbSituationRegistrationService;

/**
 * 返省登记Service业务层处理
 * 
 * @author koilty
 * @date 2023-03-14
 */
@Service
public class DbSituationRegistrationServiceImpl implements IDbSituationRegistrationService 
{
    @Autowired
    private DbSituationRegistrationMapper dbSituationRegistrationMapper;

    /**
     * 查询返省登记
     * 
     * @param id 返省登记主键
     * @return 返省登记
     */
    @Override
    public DbSituationRegistration selectDbSituationRegistrationById(Long id)
    {
        return dbSituationRegistrationMapper.selectDbSituationRegistrationById(id);
    }

    /**
     * 查询返省登记列表
     * 
     * @param dbSituationRegistration 返省登记
     * @return 返省登记
     */
    @Override
    public List<DbSituationRegistration> selectDbSituationRegistrationList(DbSituationRegistration dbSituationRegistration)
    {
        return dbSituationRegistrationMapper.selectDbSituationRegistrationList(dbSituationRegistration);
    }

    /**
     * 新增返省登记
     * 
     * @param dbSituationRegistration 返省登记
     * @return 结果
     */
    @Override
    public int insertDbSituationRegistration(DbSituationRegistration dbSituationRegistration)
    {
        return dbSituationRegistrationMapper.insertDbSituationRegistration(dbSituationRegistration);
    }

    /**
     * 修改返省登记
     * 
     * @param dbSituationRegistration 返省登记
     * @return 结果
     */
    @Override
    public int updateDbSituationRegistration(DbSituationRegistration dbSituationRegistration)
    {
        return dbSituationRegistrationMapper.updateDbSituationRegistration(dbSituationRegistration);
    }

    /**
     * 批量删除返省登记
     * 
     * @param ids 需要删除的返省登记主键
     * @return 结果
     */
    @Override
    public int deleteDbSituationRegistrationByIds(Long[] ids)
    {
        return dbSituationRegistrationMapper.deleteDbSituationRegistrationByIds(ids);
    }

    /**
     * 删除返省登记信息
     * 
     * @param id 返省登记主键
     * @return 结果
     */
    @Override
    public int deleteDbSituationRegistrationById(Long id)
    {
        return dbSituationRegistrationMapper.deleteDbSituationRegistrationById(id);
    }
}
