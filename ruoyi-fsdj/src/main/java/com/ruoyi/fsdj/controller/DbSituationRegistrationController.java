package com.ruoyi.fsdj.controller;

import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbSituationRegistration;
import com.ruoyi.fsdj.service.IDbSituationRegistrationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 返省登记Controller
 * 
 * @author koilty
 * @date 2023-03-14
 */
@Api(tags="疫情模块-返省登记")
@RestController
@RequestMapping("/fsdj/fsdj")
public class DbSituationRegistrationController extends BaseController
{
    @Autowired
    private IDbSituationRegistrationService dbSituationRegistrationService;


    /**
     * 查询返省登记列表
     */
    @PreAuthorize("@ss.hasPermi('fsdj:fsdj:list')")
    @GetMapping("/list")
    @ApiOperation("返省登记列表")
    public TableDataInfo list(DbSituationRegistration dbSituationRegistration)
    {
        startPage();
        List<DbSituationRegistration> list = dbSituationRegistrationService.selectDbSituationRegistrationList(dbSituationRegistration);
        return getDataTable(list);
    }

    /**
     * 导出返省登记列表
     */
    @PreAuthorize("@ss.hasPermi('fsdj:fsdj:export')")
    @Log(title = "返省登记", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出返省登记列表")
    public void export(HttpServletResponse response, DbSituationRegistration dbSituationRegistration)
    {
        List<DbSituationRegistration> list = dbSituationRegistrationService.selectDbSituationRegistrationList(dbSituationRegistration);
        ExcelUtil<DbSituationRegistration> util = new ExcelUtil<DbSituationRegistration>(DbSituationRegistration.class);
        util.exportExcel(response, list, "返省登记数据");
    }

    /**
     * 获取返省登记详细信息
     */
    @PreAuthorize("@ss.hasPermi('fsdj:fsdj:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("获取返省登记详情")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbSituationRegistrationService.selectDbSituationRegistrationById(id));
    }

    /**
     * 新增返省登记
     */
    @PreAuthorize("@ss.hasPermi('fsdj:fsdj:add')")
    @Log(title = "返省登记", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("新增返省登记")
    public AjaxResult add(@RequestBody DbSituationRegistration dbSituationRegistration)
    {
        return toAjax(dbSituationRegistrationService.insertDbSituationRegistration(dbSituationRegistration));
    }

    /**
     * 修改返省登记
     */
    @PreAuthorize("@ss.hasPermi('fsdj:fsdj:edit')")
    @Log(title = "返省登记", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改返省登记")
    public AjaxResult edit(@RequestBody DbSituationRegistration dbSituationRegistration)
    {
        return toAjax(dbSituationRegistrationService.updateDbSituationRegistration(dbSituationRegistration));
    }

    /**
     * 删除返省登记
     */
    @PreAuthorize("@ss.hasPermi('fsdj:fsdj:remove')")
    @Log(title = "返省登记", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("删除返省登记")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbSituationRegistrationService.deleteDbSituationRegistrationByIds(ids));
    }

}
