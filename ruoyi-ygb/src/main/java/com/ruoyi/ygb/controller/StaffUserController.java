package com.ruoyi.ygb.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.StaffUser;
import com.ruoyi.ygb.service.IStaffUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 员工信息1Controller
 * 
 * @author 郑东来
 * @date 2023-03-23
 */
@RestController
@RequestMapping("/ygb/ygb")
public class StaffUserController extends BaseController
{
    @Autowired
    private IStaffUserService staffUserService;

    /**
     * 查询员工信息1列表
     */
    @PreAuthorize("@ss.hasPermi('ygb:ygb:list')")
    @GetMapping("/list")
    public TableDataInfo list(StaffUser staffUser)
    {
        startPage();
        List<StaffUser> list = staffUserService.selectStaffUserList(staffUser);
        return getDataTable(list);
    }

    /**
     * 导出员工信息1列表
     */
    @PreAuthorize("@ss.hasPermi('ygb:ygb:export')")
    @Log(title = "员工信息1", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StaffUser staffUser)
    {
        List<StaffUser> list = staffUserService.selectStaffUserList(staffUser);
        ExcelUtil<StaffUser> util = new ExcelUtil<StaffUser>(StaffUser.class);
        util.exportExcel(response, list, "员工信息1数据");
    }

    /**
     * 获取员工信息1详细信息
     */
    @PreAuthorize("@ss.hasPermi('ygb:ygb:query')")
    @GetMapping(value = "/{staffId}")
    public AjaxResult getInfo(@PathVariable("staffId") Long staffId)
    {
        return success(staffUserService.selectStaffUserByStaffId(staffId));
    }

    /**
     * 新增员工信息1
     */
    @PreAuthorize("@ss.hasPermi('ygb:ygb:add')")
    @Log(title = "员工信息1", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StaffUser staffUser)
    {
        return toAjax(staffUserService.insertStaffUser(staffUser));
    }

    /**
     * 修改员工信息1
     */
    @PreAuthorize("@ss.hasPermi('ygb:ygb:edit')")
    @Log(title = "员工信息1", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StaffUser staffUser)
    {
        return toAjax(staffUserService.updateStaffUser(staffUser));
    }

    /**
     * 删除员工信息1
     */
    @PreAuthorize("@ss.hasPermi('ygb:ygb:remove')")
    @Log(title = "员工信息1", businessType = BusinessType.DELETE)
	@DeleteMapping("/{staffIds}")
    public AjaxResult remove(@PathVariable Long[] staffIds)
    {
        return toAjax(staffUserService.deleteStaffUserByStaffIds(staffIds));
    }
}
