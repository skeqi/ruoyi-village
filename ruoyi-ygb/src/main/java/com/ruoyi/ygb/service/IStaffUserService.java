package com.ruoyi.ygb.service;

import java.util.List;
import com.ruoyi.domain.StaffUser;

/**
 * 员工信息1Service接口
 * 
 * @author 郑东来
 * @date 2023-03-23
 */
public interface IStaffUserService 
{
    /**
     * 查询员工信息1
     * 
     * @param staffId 员工信息1主键
     * @return 员工信息1
     */
    public StaffUser selectStaffUserByStaffId(Long staffId);

    /**
     * 查询员工信息1列表
     * 
     * @param staffUser 员工信息1
     * @return 员工信息1集合
     */
    public List<StaffUser> selectStaffUserList(StaffUser staffUser);

    /**
     * 新增员工信息1
     * 
     * @param staffUser 员工信息1
     * @return 结果
     */
    public int insertStaffUser(StaffUser staffUser);

    /**
     * 修改员工信息1
     * 
     * @param staffUser 员工信息1
     * @return 结果
     */
    public int updateStaffUser(StaffUser staffUser);

    /**
     * 批量删除员工信息1
     * 
     * @param staffIds 需要删除的员工信息1主键集合
     * @return 结果
     */
    public int deleteStaffUserByStaffIds(Long[] staffIds);

    /**
     * 删除员工信息1信息
     * 
     * @param staffId 员工信息1主键
     * @return 结果
     */
    public int deleteStaffUserByStaffId(Long staffId);
}
