package com.ruoyi.ygb.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ygb.mapper.StaffUserMapper;
import com.ruoyi.domain.StaffUser;
import com.ruoyi.ygb.service.IStaffUserService;

/**
 * 员工信息1Service业务层处理
 * 
 * @author 郑东来
 * @date 2023-03-23
 */
@Service
public class StaffUserServiceImpl implements IStaffUserService 
{
    @Autowired
    private StaffUserMapper staffUserMapper;

    /**
     * 查询员工信息1
     * 
     * @param staffId 员工信息1主键
     * @return 员工信息1
     */
    @Override
    public StaffUser selectStaffUserByStaffId(Long staffId)
    {
        return staffUserMapper.selectStaffUserByStaffId(staffId);
    }

    /**
     * 查询员工信息1列表
     * 
     * @param staffUser 员工信息1
     * @return 员工信息1
     */
    @Override
    public List<StaffUser> selectStaffUserList(StaffUser staffUser)
    {
        return staffUserMapper.selectStaffUserList(staffUser);
    }

    /**
     * 新增员工信息1
     * 
     * @param staffUser 员工信息1
     * @return 结果
     */
    @Override
    public int insertStaffUser(StaffUser staffUser)
    {
        return staffUserMapper.insertStaffUser(staffUser);
    }

    /**
     * 修改员工信息1
     * 
     * @param staffUser 员工信息1
     * @return 结果
     */
    @Override
    public int updateStaffUser(StaffUser staffUser)
    {
        return staffUserMapper.updateStaffUser(staffUser);
    }

    /**
     * 批量删除员工信息1
     * 
     * @param staffIds 需要删除的员工信息1主键
     * @return 结果
     */
    @Override
    public int deleteStaffUserByStaffIds(Long[] staffIds)
    {
        return staffUserMapper.deleteStaffUserByStaffIds(staffIds);
    }

    /**
     * 删除员工信息1信息
     * 
     * @param staffId 员工信息1主键
     * @return 结果
     */
    @Override
    public int deleteStaffUserByStaffId(Long staffId)
    {
        return staffUserMapper.deleteStaffUserByStaffId(staffId);
    }
}
