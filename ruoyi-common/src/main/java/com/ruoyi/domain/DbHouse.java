package com.ruoyi.domain;

import java.math.BigDecimal;

import com.ruoyi.domain.DbHouseType;

import com.ruoyi.domain.DbBuilding;
import com.ruoyi.domain.DbFloor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 房屋信息对象 db_house
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public class DbHouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private DbBuilding building;
    /** 房屋id */
    private Long id;

    /** 房屋号 */
    @Excel(name = "房屋号")
    private String unit;

    /** 楼层 */
    @Excel(name = "楼层")
    private Long location;

    /** 房屋类型 */
    @Excel(name = "房屋类型")
    private Long type;

    /** 建筑面积 */
    @Excel(name = "建筑面积")
    private BigDecimal area;

    /** 出售状况 */
    @Excel(name = "出售状况")
    private String status;

    /** 楼栋名 */
    @Excel(name = "楼栋名")
    private String floor;

    private DbHouseType houseType;

    private DbFloor dbFloor;

    public DbFloor getDbFloor() {
        return dbFloor;
    }

    public void setDbFloor(DbFloor dbFloor) {
        this.dbFloor = dbFloor;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public DbHouseType getHouseType() {
        return houseType;
    }

    public void setHouseType(DbHouseType houseType) {
        this.houseType = houseType;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setLocation(Long location) 
    {
        this.location = location;
    }

    public Long getLocation() 
    {
        return location;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setArea(BigDecimal area) 
    {
        this.area = area;
    }

    public BigDecimal getArea() 
    {
        return area;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setFloor(String floor) 
    {
        this.floor = floor;
    }

    public String getFloor() 
    {
        return floor;
    }

    public DbBuilding getBuilding() {
        return building;
    }

    public void setBuilding(DbBuilding building) {
        this.building = building;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("unit", getUnit())
            .append("location", getLocation())
            .append("type", getType())
            .append("area", getArea())
            .append("status", getStatus())
            .append("floor", getFloor())
            .toString();
    }
}
