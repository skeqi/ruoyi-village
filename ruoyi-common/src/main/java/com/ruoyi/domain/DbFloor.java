package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 楼层管理对象 db_floor
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public class DbFloor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 楼层id */
    private Long id;

    /** 楼层
 */
    @Excel(name = "楼层 ")
    private Long location;

    /** 是否装备灭火器 */
    @Excel(name = "是否装备灭火器")
    private String firewall;

    /** 所属楼栋 */
    @Excel(name = "所属楼栋")
    private String floor;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLocation(Long location) 
    {
        this.location = location;
    }

    public Long getLocation() 
    {
        return location;
    }
    public void setFirewall(String firewall) 
    {
        this.firewall = firewall;
    }

    public String getFirewall() 
    {
        return firewall;
    }
    public void setFloor(String floor) 
    {
        this.floor = floor;
    }

    public String getFloor() 
    {
        return floor;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("location", getLocation())
            .append("firewall", getFirewall())
            .append("floor", getFloor())
            .toString();
    }
}
