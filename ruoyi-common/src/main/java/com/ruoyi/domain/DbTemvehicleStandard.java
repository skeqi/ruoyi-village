package com.ruoyi.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 临时车收费标准对象 db_temvehicle_standard
 * 
 * @author wlx
 * @date 2023-03-09
 */
public class DbTemvehicleStandard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 规则名 */
    @Excel(name = "规则名")
    private String rulename;

    /** 免费分钟 */
    @Excel(name = "免费分钟")
    private Long freeminutes;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 单位 */
    @Excel(name = "单位")
    private Long unit;

    /** 24小时收费上线 */
    @Excel(name = "24小时收费上线")
    private BigDecimal chargeOnline;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRulename(String rulename) 
    {
        this.rulename = rulename;
    }

    public String getRulename() 
    {
        return rulename;
    }
    public void setFreeminutes(Long freeminutes) 
    {
        this.freeminutes = freeminutes;
    }

    public Long getFreeminutes() 
    {
        return freeminutes;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setUnit(Long unit) 
    {
        this.unit = unit;
    }

    public Long getUnit() 
    {
        return unit;
    }
    public void setChargeOnline(BigDecimal chargeOnline) 
    {
        this.chargeOnline = chargeOnline;
    }

    public BigDecimal getChargeOnline() 
    {
        return chargeOnline;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("rulename", getRulename())
            .append("freeminutes", getFreeminutes())
            .append("price", getPrice())
            .append("unit", getUnit())
            .append("chargeOnline", getChargeOnline())
            .toString();
    }
}
