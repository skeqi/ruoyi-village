package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 来访登记对象 db_lfdj
 * 
 * @author 郑东来
 * @date 2023-03-09
 */
public class DbLfdj extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 来访ID */
    private Long lfdjId;

    /** 访客名字 */
    @Excel(name = "访客名字")
    private String lfdjName;

    /** 访客手机电话 */
    @Excel(name = "访客手机电话")
    private Long lfdjPhone;

    /** 来访事由 */
    @Excel(name = "来访事由")
    private String lfdjReasons;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String lfdjChepai;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lfdjBegin;

    /** 来访/离开时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "来访/离开时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lfdjEnd;

    /** 车辆状态1.审核通过2.待审核3.未审核 */
    @Excel(name = "车辆状态1.审核通过2.待审核3.未审核")
    private Long lfdjClzt;

    /** 状态11.已失效22.未生效 */
    @Excel(name = "状态11.已失效22.未生效")
    private Long lfdjZt;

    public void setLfdjId(Long lfdjId) 
    {
        this.lfdjId = lfdjId;
    }

    public Long getLfdjId() 
    {
        return lfdjId;
    }
    public void setLfdjName(String lfdjName) 
    {
        this.lfdjName = lfdjName;
    }

    public String getLfdjName() 
    {
        return lfdjName;
    }
    public void setLfdjPhone(Long lfdjPhone) 
    {
        this.lfdjPhone = lfdjPhone;
    }

    public Long getLfdjPhone() 
    {
        return lfdjPhone;
    }
    public void setLfdjReasons(String lfdjReasons) 
    {
        this.lfdjReasons = lfdjReasons;
    }

    public String getLfdjReasons() 
    {
        return lfdjReasons;
    }
    public void setLfdjChepai(String lfdjChepai) 
    {
        this.lfdjChepai = lfdjChepai;
    }

    public String getLfdjChepai() 
    {
        return lfdjChepai;
    }
    public void setLfdjBegin(Date lfdjBegin) 
    {
        this.lfdjBegin = lfdjBegin;
    }

    public Date getLfdjBegin() 
    {
        return lfdjBegin;
    }
    public void setLfdjEnd(Date lfdjEnd) 
    {
        this.lfdjEnd = lfdjEnd;
    }

    public Date getLfdjEnd() 
    {
        return lfdjEnd;
    }
    public void setLfdjClzt(Long lfdjClzt) 
    {
        this.lfdjClzt = lfdjClzt;
    }

    public Long getLfdjClzt() 
    {
        return lfdjClzt;
    }
    public void setLfdjZt(Long lfdjZt) 
    {
        this.lfdjZt = lfdjZt;
    }

    public Long getLfdjZt() 
    {
        return lfdjZt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("lfdjId", getLfdjId())
            .append("lfdjName", getLfdjName())
            .append("lfdjPhone", getLfdjPhone())
            .append("lfdjReasons", getLfdjReasons())
            .append("lfdjChepai", getLfdjChepai())
            .append("lfdjBegin", getLfdjBegin())
            .append("lfdjEnd", getLfdjEnd())
            .append("lfdjClzt", getLfdjClzt())
            .append("lfdjZt", getLfdjZt())
            .toString();
    }
}
