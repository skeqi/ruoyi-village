package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 返省登记对象 db_situation_registration
 * 
 * @author koilty
 * @date 2023-03-14
 */
public class DbSituationRegistration extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String registrationName;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String registrationPhone;

    /** 来源地 */
    @Excel(name = "来源地")
    private String registrationSource;

    /** 城市名称 */
    @Excel(name = "城市名称")
    private String registrationCityname;

    /** 返回时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "返回时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationBegin;

    /** 备注 */
    @Excel(name = "备注")
    private String registrationComment;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRegistrationName(String registrationName) 
    {
        this.registrationName = registrationName;
    }

    public String getRegistrationName() 
    {
        return registrationName;
    }
    public void setRegistrationPhone(String registrationPhone)
    {
        this.registrationPhone = registrationPhone;
    }

    public String getRegistrationPhone()
    {
        return registrationPhone;
    }
    public void setRegistrationSource(String registrationSource) 
    {
        this.registrationSource = registrationSource;
    }

    public String getRegistrationSource() 
    {
        return registrationSource;
    }
    public void setRegistrationCityname(String registrationCityname) 
    {
        this.registrationCityname = registrationCityname;
    }

    public String getRegistrationCityname() 
    {
        return registrationCityname;
    }
    public void setRegistrationBegin(Date registrationBegin) 
    {
        this.registrationBegin = registrationBegin;
    }

    public Date getRegistrationBegin() 
    {
        return registrationBegin;
    }
    public void setRegistrationComment(String registrationComment) 
    {
        this.registrationComment = registrationComment;
    }

    public String getRegistrationComment() 
    {
        return registrationComment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("registrationName", getRegistrationName())
            .append("registrationPhone", getRegistrationPhone())
            .append("registrationSource", getRegistrationSource())
            .append("registrationCityname", getRegistrationCityname())
            .append("registrationBegin", getRegistrationBegin())
            .append("registrationComment", getRegistrationComment())
            .toString();
    }
}
