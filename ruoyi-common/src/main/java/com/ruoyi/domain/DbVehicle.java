package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.domain.DbOwner;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车辆管理对象 db_vehicle
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
public class DbVehicle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车辆id */
    private Long id;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String number;

    /** 关联业主id */
    @Excel(name = "关联业主id")
    private Long ownerid;

    /** 关联车位编号 */
    @Excel(name = "关联车位编号")
    private Long parkingid;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date starttime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endtime;

    /** 1、进入 2、外出 */
    @Excel(name = "1、进入 2、外出")
    private String status;

    public DbOwner dbOwner;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public DbOwner getDbOwner() {
        return dbOwner;
    }

    public void setDbOwner(DbOwner dbOwner) {
        this.dbOwner = dbOwner;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setOwnerid(Long ownerid) 
    {
        this.ownerid = ownerid;
    }

    public Long getOwnerid() 
    {
        return ownerid;
    }
    public void setParkingid(Long parkingid) 
    {
        this.parkingid = parkingid;
    }

    public Long getParkingid() 
    {
        return parkingid;
    }
    public void setStarttime(Date starttime) 
    {
        this.starttime = starttime;
    }

    public Date getStarttime() 
    {
        return starttime;
    }
    public void setEndtime(Date endtime) 
    {
        this.endtime = endtime;
    }

    public Date getEndtime() 
    {
        return endtime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("number", getNumber())
            .append("ownerid", getOwnerid())
            .append("parkingid", getParkingid())
            .append("starttime", getStarttime())
            .append("endtime", getEndtime())
            .append("status", getStatus())
            .toString();
    }
}
