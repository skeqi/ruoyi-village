package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 员工职业对象 db_staff_occupation
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
public class DbStaffOccupation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 职业编号 */
    @Excel(name = "职业编号")
    private Long staffId;

    /** 所在岗位 */
    @Excel(name = "所在岗位")
    private String staffPost;

    public void setStaffId(Long staffId) 
    {
        this.staffId = staffId;
    }

    public Long getStaffId() 
    {
        return staffId;
    }
    public void setStaffPost(String staffPost) 
    {
        this.staffPost = staffPost;
    }

    public String getStaffPost() 
    {
        return staffPost;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("staffId", getStaffId())
            .append("staffPost", getStaffPost())
            .toString();
    }
}
