package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.domain.StaffUser;
import com.ruoyi.domain.User;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 巡检任务对象 db_patroltask
 * 
 * @author ruoyi
 * @date 2023-03-09
 */
public class DbPatroltask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 巡检任务id */
    private Long id;

    /** 计划名称 */
    @Excel(name = "计划名称")
    private String name;

    /** 计划巡检人 */
    @Excel(name = "计划巡检人")
    private Long gId;

    /** 当前巡检人 */
    @Excel(name = "当前巡检人")
    private Long sId;

    /** 巡检方式 */
    @Excel(name = "巡检方式")
    private Long type;

    /** 巡检状态 */
    @Excel(name = "巡检状态")
    private Long state;

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private StaffUser staffUser;

    private StaffUser staff;

    public StaffUser getStaff() {
        return staff;
    }

    public void setStaff(StaffUser staff) {
        this.staff = staff;
    }

    public StaffUser getStaffUser() {
        return staffUser;
    }

    public void setStaffUser(StaffUser staffUser) {
        this.staffUser = staffUser;
    }

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date begintime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date finishtime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setgId(Long gId) 
    {
        this.gId = gId;
    }

    public Long getgId() 
    {
        return gId;
    }
    public void setsId(Long sId) 
    {
        this.sId = sId;
    }

    public Long getsId() 
    {
        return sId;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setBegintime(Date begintime) 
    {
        this.begintime = begintime;
    }

    public Date getBegintime() 
    {
        return begintime;
    }
    public void setFinishtime(Date finishtime) 
    {
        this.finishtime = finishtime;
    }

    public Date getFinishtime() 
    {
        return finishtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("gId", getgId())
            .append("sId", getsId())
            .append("type", getType())
            .append("state", getState())
            .append("begintime", getBegintime())
            .append("finishtime", getFinishtime())
            .toString();
    }
}
