package com.ruoyi.domain;

import com.ruoyi.domain.DbOwner;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主家庭成员对象 db_member
 *
 * @author ruoyi
 * @date 2023-03-20
 */
public class DbMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 家庭成员ID */
    private Long id;

    /** 业主id */
    @Excel(name = "业主id")
    private Long ownerId;

    /** 业主id */
    @Excel(name = "关系")
    private String relation;

    /** 名称 */
    @Excel(name = "名称")
    private String member;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 身份证 */
    @Excel(name = "身份证")
    private String shenfenz;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phone;

    /** 家庭住址 */
    @Excel(name = "家庭住址")
    private String homeAdd;
    private String owner_name;


    private DbOwner dbOwner;

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public DbOwner getDbOwner() {
        return dbOwner;
    }

    public void setDbOwner(DbOwner dbOwner) {
        this.dbOwner = dbOwner;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOwnerId(Long ownerId)
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId()
    {
        return ownerId;
    }
    public void setMember(String member)
    {
        this.member = member;
    }

    public String getRelation() {
        return relation;
    }
    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getMember()
    {
        return member;
    }
    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getSex()
    {
        return sex;
    }
    public void setAge(Long age)
    {
        this.age = age;
    }

    public Long getAge()
    {
        return age;
    }
    public void setShenfenz(String shenfenz)
    {
        this.shenfenz = shenfenz;
    }

    public String getShenfenz()
    {
        return shenfenz;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setHomeAdd(String homeAdd)
    {
        this.homeAdd = homeAdd;
    }

    public String getHomeAdd()
    {
        return homeAdd;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerId", getOwnerId())
            .append("member", getMember())
            .append("sex", getSex())
            .append("age", getAge())
            .append("shenfenz", getShenfenz())
            .append("phone", getPhone())
            .append("homeAdd", getHomeAdd())
            .toString();
    }
}
