package com.ruoyi.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 设备信息对象 db_equipment
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
public class DbEquipment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备id */
    private Long id;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String ename;

    /** 设备编码 */
    @Excel(name = "设备编码")
    private Long coding;

    /** 设备品牌 */
    @Excel(name = "设备品牌")
    private String brand;

    /** 位置详情 */
    @Excel(name = "位置详情")
    private String particulars;

    /** 使用状态 */
    @Excel(name = "使用状态")
    private Long state;

    /** 采购价格 */
    @Excel(name = "采购价格")
    private BigDecimal price;

    /** 首次启用时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "首次启用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date starttime;

    /** 保修截止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "保修截止日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date guaranteetime;

    /** 责任人 */
    @Excel(name = "责任人")
    private Long person;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public StaffUser getStaffUser() {
        return staffUser;
    }

    public void setStaffUser(StaffUser staffUser) {
        this.staffUser = staffUser;
    }

    private StaffUser staffUser;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEname(String ename) 
    {
        this.ename = ename;
    }

    public String getEname() 
    {
        return ename;
    }
    public void setCoding(Long coding) 
    {
        this.coding = coding;
    }

    public Long getCoding() 
    {
        return coding;
    }
    public void setBrand(String brand) 
    {
        this.brand = brand;
    }

    public String getBrand() 
    {
        return brand;
    }
    public void setParticulars(String particulars) 
    {
        this.particulars = particulars;
    }

    public String getParticulars() 
    {
        return particulars;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStarttime(Date starttime) 
    {
        this.starttime = starttime;
    }

    public Date getStarttime() 
    {
        return starttime;
    }
    public void setGuaranteetime(Date guaranteetime) 
    {
        this.guaranteetime = guaranteetime;
    }

    public Date getGuaranteetime() 
    {
        return guaranteetime;
    }
    public void setPerson(Long person) 
    {
        this.person = person;
    }

    public Long getPerson() 
    {
        return person;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ename", getEname())
            .append("coding", getCoding())
            .append("brand", getBrand())
            .append("particulars", getParticulars())
            .append("state", getState())
            .append("price", getPrice())
            .append("starttime", getStarttime())
            .append("guaranteetime", getGuaranteetime())
            .append("person", getPerson())
            .append("remarks", getRemarks())
            .toString();
    }
}
