package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 员工考勤对象 db_staff_ygkq
 * 
 * @author 郑东来
 * @date 2023-03-22
 */
public class DbStaffYgkq extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工编号 */
    private Long staffId;

    /** 所在部门 */
    @Excel(name = "所在部门")
    private Long staffPost;

    /** 姓名 */
    @Excel(name = "姓名")
    private String staffName;

    /** 打卡日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "打卡日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date staffDkrq;

    /** 打卡记录 */
    @Excel(name = "打卡记录")
    private String staffDkjl;

    /** 工作时长(h) */
    @Excel(name = "工作时长(h)")
    private String staffGzsc;

    /** 状态0:异常1:补打卡2:请假 */
    @Excel(name = "状态0:异常1:补打卡2:请假")
    private Long staffZt;

    public void setStaffId(Long staffId) 
    {
        this.staffId = staffId;
    }

    public Long getStaffId() 
    {
        return staffId;
    }
    public void setStaffPost(Long staffPost) 
    {
        this.staffPost = staffPost;
    }

    public Long getStaffPost() 
    {
        return staffPost;
    }
    public void setStaffName(String staffName) 
    {
        this.staffName = staffName;
    }

    public String getStaffName() 
    {
        return staffName;
    }
    public void setStaffDkrq(Date staffDkrq) 
    {
        this.staffDkrq = staffDkrq;
    }

    public Date getStaffDkrq() 
    {
        return staffDkrq;
    }
    public void setStaffDkjl(String staffDkjl) 
    {
        this.staffDkjl = staffDkjl;
    }

    public String getStaffDkjl() 
    {
        return staffDkjl;
    }
    public void setStaffGzsc(String staffGzsc) 
    {
        this.staffGzsc = staffGzsc;
    }

    public String getStaffGzsc() 
    {
        return staffGzsc;
    }
    public void setStaffZt(Long staffZt) 
    {
        this.staffZt = staffZt;
    }

    public Long getStaffZt() 
    {
        return staffZt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("staffId", getStaffId())
            .append("staffPost", getStaffPost())
            .append("staffName", getStaffName())
            .append("staffDkrq", getStaffDkrq())
            .append("staffDkjl", getStaffDkjl())
            .append("staffGzsc", getStaffGzsc())
            .append("staffZt", getStaffZt())
            .toString();
    }
}
