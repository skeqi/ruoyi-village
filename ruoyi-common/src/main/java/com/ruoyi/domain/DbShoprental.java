package com.ruoyi.domain;

import java.math.BigDecimal;
import java.security.acl.Owner;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.ruoyi.domain.DbOwner;
import com.ruoyi.domain.DbShops;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商铺出租对象 db_shoprental
 * 
 * @author wlx
 * @date 2023-03-17
 */
@Data
public class DbShoprental extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /**商铺信息*/
    private DbShops shops;
    /**业主信息*/
    private DbOwner owner;

    /** 商铺租借id */
    private Integer id;

    /** 业主id */
    @Excel(name = "业主id")
    private Integer ownerId;

    /** 商铺id */
    private Integer shopsId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getShopsId() {
        return shopsId;
    }

    public void setShopsId(Integer shopsId) {
        this.shopsId = shopsId;
    }

    /** 起租时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起租时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date leasetime;

    /** 截租时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "截租时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date renttime;

    /** 租金费 */
    @Excel(name = "租金费")
    private BigDecimal rentalfee;





    public DbShops getShops() {
        return shops;
    }

    public void setShops(DbShops shops) {
        this.shops = shops;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setOwnerId(Integer ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Integer getOwnerId() 
    {
        return ownerId;
    }
    public void setLeasetime(Date leasetime) 
    {
        this.leasetime = leasetime;
    }

    public Date getLeasetime() 
    {
        return leasetime;
    }
    public void setRenttime(Date renttime) 
    {
        this.renttime = renttime;
    }

    public Date getRenttime() 
    {
        return renttime;
    }
    public void setRentalfee(BigDecimal rentalfee) 
    {
        this.rentalfee = rentalfee;
    }

    public BigDecimal getRentalfee() 
    {
        return rentalfee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerId", getOwnerId())
            .append("leasetime", getLeasetime())
            .append("renttime", getRenttime())
            .append("rentalfee", getRentalfee())
            .toString();
    }
}
