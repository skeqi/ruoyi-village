package com.ruoyi.domain;

import com.ruoyi.domain.DbHouse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主信息对象 db_owner
 *
 * @author ruoyi
 * @date 2023-03-23
 */
public class DbOwner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业主ID */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String yname;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 性别 */
    @Excel(name = "年龄")
    private String age;

    /** 身份证 */
    @Excel(name = "身份证")
    private String shenfenz;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phone;

    /** 工作单位 */
    @Excel(name = "工作单位")
    private String workUnit;

    /** 关联房屋id */
    @Excel(name = "关联房屋id")
    private Long unit;

    /** 贷款金额 */
    @Excel(name = "贷款金额")
    private String loanSum;

    /** 应还款金额 */
    @Excel(name = "应还款金额")
    private String refundSum;

    /** 审批状态 */
    @Excel(name = "审批状态")
    private String htstatus;

    private DbHouse dbHouse;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public DbHouse getDbHouse() {
        return dbHouse;
    }

    public void setDbHouse(DbHouse dbHouse) {
        this.dbHouse = dbHouse;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setYname(String yname)
    {
        this.yname = yname;
    }

    public String getYname()
    {
        return yname;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getSex()
    {
        return sex;
    }
    public void setShenfenz(String shenfenz)
    {
        this.shenfenz = shenfenz;
    }

    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }

    public String getShenfenz()
    {
        return shenfenz;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setWorkUnit(String workUnit)
    {
        this.workUnit = workUnit;
    }

    public String getWorkUnit()
    {
        return workUnit;
    }
    public void setUnit(Long unit)
    {
        this.unit = unit;
    }

    public Long getUnit()
    {
        return unit;
    }
    public void setLoanSum(String loanSum)
    {
        this.loanSum = loanSum;
    }

    public String getLoanSum()
    {
        return loanSum;
    }
    public void setRefundSum(String refundSum)
    {
        this.refundSum = refundSum;
    }

    public String getRefundSum()
    {
        return refundSum;
    }
    public void setHtstatus(String htstatus)
    {
        this.htstatus = htstatus;
    }

    public String getHtstatus()
    {
        return htstatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("yname", getYname())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("sex", getSex())
            .append("shenfenz", getShenfenz())
            .append("phone", getPhone())
            .append("workUnit", getWorkUnit())
            .append("unit", getUnit())
            .append("loanSum", getLoanSum())
            .append("refundSum", getRefundSum())
            .append("htstatus", getHtstatus())
            .toString();
    }
}
