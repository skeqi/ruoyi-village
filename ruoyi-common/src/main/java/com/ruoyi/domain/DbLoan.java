package com.ruoyi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 放款记录对象 db_loan
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
public class DbLoan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 放款账号 */
    @Excel(name = "放款账号")
    private String account;

    /** 放款账号银行类型 */
    @Excel(name = "放款账号银行类型")
    private Integer type;

    /** 户主 */
    @Excel(name = "户主")
    private String host;

    /** 实际放款金额 */
    @Excel(name = "实际放款金额")
    private BigDecimal real;

    /** 放日期款 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "放日期款", width = 30, dateFormat = "yyyy-MM-dd")
    private Date fangkuanDate;

    /** 合同还款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同还款日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date huankuanDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAccount(String account) 
    {
        this.account = account;
    }

    public String getAccount() 
    {
        return account;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setHost(String host) 
    {
        this.host = host;
    }

    public String getHost() 
    {
        return host;
    }
    public void setReal(BigDecimal real) 
    {
        this.real = real;
    }

    public BigDecimal getReal() 
    {
        return real;
    }
    public void setFangkuanDate(Date fangkuanDate) 
    {
        this.fangkuanDate = fangkuanDate;
    }

    public Date getFangkuanDate() 
    {
        return fangkuanDate;
    }
    public void setHuankuanDate(Date huankuanDate) 
    {
        this.huankuanDate = huankuanDate;
    }

    public Date getHuankuanDate() 
    {
        return huankuanDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("account", getAccount())
            .append("type", getType())
            .append("host", getHost())
            .append("real", getReal())
            .append("fangkuanDate", getFangkuanDate())
            .append("huankuanDate", getHuankuanDate())
            .toString();
    }
}
