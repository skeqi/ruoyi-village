package com.ruoyi.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 申请贷款对象 db_sqdk
 * 
 * @author lql
 * @date 2023-03-14
 */
public class DbSqdk extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 业主 */
    @Excel(name = "业主")
    private Integer ownerId;

    /** 卡号 */
    @Excel(name = "卡号")
    private String cardNum;

    /** 贷款金额 */
    @Excel(name = "贷款金额")
    private Integer num;

    /** 期限 */
    @Excel(name = "期限")
    private Integer term;

    /** 贷款方式  */
    @Excel(name = "贷款方式 ")
    private Integer loansType;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    private DbOwner dbOwner;

    public DbOwner getDbOwner() {
        return dbOwner;
    }

    public void setDbOwner(DbOwner dbOwner) {
        this.dbOwner = dbOwner;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setOwnerId(Integer ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Integer getOwnerId() 
    {
        return ownerId;
    }
    public void setCardNum(String cardNum) 
    {
        this.cardNum = cardNum;
    }

    public String getCardNum() 
    {
        return cardNum;
    }
    public void setNum(Integer num) 
    {
        this.num = num;
    }

    public Integer getNum() 
    {
        return num;
    }
    public void setTerm(Integer term) 
    {
        this.term = term;
    }

    public Integer getTerm() 
    {
        return term;
    }
    public void setLoansType(Integer loansType) 
    {
        this.loansType = loansType;
    }

    public Integer getLoansType() 
    {
        return loansType;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerId", getOwnerId())
            .append("cardNum", getCardNum())
            .append("num", getNum())
            .append("term", getTerm())
            .append("loansType", getLoansType())
            .append("remark", getRemark())
            .append("status", getStatus())
            .toString();
    }
}
