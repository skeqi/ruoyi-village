package com.ruoyi.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 临时车管理对象 db_temporary
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
public class DbTemporary extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 获取车牌号 */
    @Excel(name = "获取车牌号")
    private String number;

    /** 进入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "进入时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private String enterinto;

    /** 出去时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出去时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private String getout;

    /** 计算时长 */
    @Excel(name = "计算时长")
    private String duration;

    /** 费用计算 */
    @Excel(name = "费用计算")
    private BigDecimal cost;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setEnterinto(String enterinto)
    {
        this.enterinto = enterinto;
    }

    public String getEnterinto()
    {
        return enterinto;
    }
    public void setGetout(String getout)
    {
        this.getout = getout;
    }

    public String getGetout()
    {
        return getout;
    }
    public void setDuration(String duration) 
    {
        this.duration = duration;
    }

    public String getDuration() 
    {
        return duration;
    }
    public void setCost(BigDecimal cost) 
    {
        this.cost = cost;
    }

    public BigDecimal getCost() 
    {
        return cost;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("number", getNumber())
            .append("enterinto", getEnterinto())
            .append("getout", getGetout())
            .append("duration", getDuration())
            .append("cost", getCost())
            .toString();
    }
}
