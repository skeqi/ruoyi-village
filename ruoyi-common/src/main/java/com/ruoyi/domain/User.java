package com.ruoyi.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 员工表对象 db_staff_user
 *
 * @author Koilty
 * @date 2023-03-07
 */
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工编号 */
    @Excel(name = "员工编号")
    private Long yId;

    /** 职业编号 */
    @Excel(name = "职业编号")
    private Long staffUid;

    /** 姓名 */
    @Excel(name = "姓名")
    private String sUser;

    /** 性别 */
    @Excel(name = "性别")
    private String staffGender;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long staffAge;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private Long staffContact;

    /** 家庭住址  */
    @Excel(name = "家庭住址 ")
    private String staffAddress;

    private String staffName;

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setYId(Long yId)
    {
        this.yId = yId;
    }

    public Long getYId()
    {
        return yId;
    }
    public void setStaffUid(Long staffUid)
    {
        this.staffUid = staffUid;
    }

    public Long getStaffUid()
    {
        return staffUid;
    }
    public void setSUser(String sUser)
    {
        this.sUser = sUser;
    }

    public String getSUser()
    {
        return sUser;
    }
    public void setStaffGender(String staffGender)
    {
        this.staffGender = staffGender;
    }

    public String getStaffGender()
    {
        return staffGender;
    }
    public void setStaffAge(Long staffAge)
    {
        this.staffAge = staffAge;
    }

    public Long getStaffAge()
    {
        return staffAge;
    }
    public void setStaffContact(Long staffContact)
    {
        this.staffContact = staffContact;
    }

    public Long getStaffContact()
    {
        return staffContact;
    }
    public void setStaffAddress(String staffAddress)
    {
        this.staffAddress = staffAddress;
    }

    public String getStaffAddress()
    {
        return staffAddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("staffId", getYId())
                .append("staffUid", getStaffUid())
                .append("staffUser", getSUser())
                .append("staffGender", getStaffGender())
                .append("staffAge", getStaffAge())
                .append("staffContact", getStaffContact())
                .append("staffAddress", getStaffAddress())
                .toString();
    }
}