package com.ruoyi.domain;

import com.ruoyi.domain.StaffUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 楼栋管理对象 db_building
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public class DbBuilding extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 楼栋id */
    private Long buidingid;

    /** 楼栋名 */
    @Excel(name = "楼栋名")
    private String floor;

    /** 楼栋负责人 */
    @Excel(name = "楼栋负责人")
    private int houseprincipal;

    /** 安保人员 */
    @Excel(name = "安保人员")
    private int equipment;

    /* 楼层*/
    @Excel(name = "楼层数量")
    private int luoceng;

    /* 住户数量*/
    @Excel(name = "住户数量")
    private int zhuhu;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getLuoceng() {
        return luoceng;
    }

    public void setLuoceng(int luoceng) {
        this.luoceng = luoceng;
    }

    public int getZhuhu() {
        return zhuhu;
    }

    public void setZhuhu(int zhuhu) {
        this.zhuhu = zhuhu;
    }

    private StaffUser staffUser;

    private StaffUser staff;

    public StaffUser getStaff() {
        return staff;
    }

    public void setStaff(StaffUser staff) {
        this.staff = staff;
    }

    public StaffUser getStaffUser() {
        return staffUser;
    }

    public void setStaffUser(StaffUser staffUser) {
        this.staffUser = staffUser;
    }

    public void setBuidingid(Long buidingid)
    {
        this.buidingid = buidingid;
    }

    public Long getBuidingid() 
    {
        return buidingid;
    }
    public void setFloor(String floor)
    {
        this.floor = floor;
    }

    public String getFloor()
    {
        return floor;
    }
    public void setHouseprincipal(int houseprincipal)
    {
        this.houseprincipal = houseprincipal;
    }

    public int getHouseprincipal()
    {
        return houseprincipal;
    }
    public void setEquipment(int equipment)
    {
        this.equipment = equipment;
    }

    public int getEquipment()
    {
        return equipment;
    }

    @Override
    public String toString() {
        return "DbBuilding{" +
                "buidingid=" + buidingid +
                ", floor='" + floor + '\'' +
                ", houseprincipal=" + houseprincipal +
                ", equipment=" + equipment +
                ", luoceng=" + luoceng +
                ", zhuhu=" + zhuhu +
                ", staffUser=" + staffUser +
                ", staff=" + staff +
                '}';
    }
}
