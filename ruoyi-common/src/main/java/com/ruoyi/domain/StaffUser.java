package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 员工表对象 db_staff_user
 *
 * @author Koilty
 * @date 2023-03-07
 */
public class StaffUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 员工图片 */
    @Excel(name = "员工图片")
    private String staffPicture;

    /** 员工编号 */
    @Excel(name = "员工编号")
    private Long staffId;

    /** 职业编号 */
    @Excel(name = "职业编号")
    private Long staffUid;

    /** 姓名 */
    @Excel(name = "姓名")
    private String staffUser;

    /** 性别 */
    @Excel(name = "性别")
    private String staffGender;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long staffAge;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private Long staffContact;

    /** 家庭住址  */
    @Excel(name = "家庭住址 ")
    private String staffAddress;

    private Long yId;

    public Long getyId() {
        return yId;
    }

    public void setyId(Long yId) {
        this.yId = yId;
    }

    private String staffName;

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setStaffId(Long staffId)
    {
        this.staffId = staffId;
    }

    public Long getStaffId()
    {
        return staffId;
    }
    public void setStaffUid(Long staffUid)
    {
        this.staffUid = staffUid;
    }

    public Long getStaffUid()
    {
        return staffUid;
    }
    public void setStaffUser(String staffUser)
    {
        this.staffUser = staffUser;
    }

    public String getStaffUser()
    {
        return staffUser;
    }
    public void setStaffGender(String staffGender)
    {
        this.staffGender = staffGender;
    }

    public String getStaffGender()
    {
        return staffGender;
    }
    public void setStaffAge(Long staffAge)
    {
        this.staffAge = staffAge;
    }

    public Long getStaffAge()
    {
        return staffAge;
    }
    public void setStaffContact(Long staffContact)
    {
        this.staffContact = staffContact;
    }

    public Long getStaffContact()
    {
        return staffContact;
    }
    public void setStaffAddress(String staffAddress)
    {
        this.staffAddress = staffAddress;
    }

    public String getStaffAddress()
    {
        return staffAddress;
    }

    public String getStaffPicture() {
        return staffPicture;
    }

    public void setStaffPicture(String staffPicture) {
        this.staffPicture = staffPicture;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("staffId", getStaffId())
                .append("staffUid", getStaffUid())
                .append("staffUser", getStaffUser())
                .append("staffGender", getStaffGender())
                .append("staffAge", getStaffAge())
                .append("staffContact", getStaffContact())
                .append("staffAddress", getStaffAddress())
                .append("yId", getyId())
                .append("staffName", getStaffName())
                .toString();
    }
}