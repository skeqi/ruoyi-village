package com.ruoyi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 签署合同对象 db_ht
 * 
 * @author lql
 * @date 2023-03-14
 */
public class DbHt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 业主 */
    @Excel(name = "业主")
    private Integer ownerId;

    /** 放款金额 */
    @Excel(name = "放款金额")
    private BigDecimal amt;

    /** 贷款期限 */
    @Excel(name = "贷款期限")
    private Long term;

    /** 放款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "放款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date fangTime;

    /** 合同到期日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同到期日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date daoTime;

    /** 合同状态 */
    @Excel(name = "合同状态")
    private String status;

    /** 贷款利率 */
    @Excel(name = "贷款利率")
    private BigDecimal rate;

    /** 贷款用途 */
    @Excel(name = "贷款用途")
    private String purpose;

    private DbOwner dbOwner;

    public DbOwner getDbOwner() {
        return dbOwner;
    }

    public void setDbOwner(DbOwner dbOwner) {
        this.dbOwner = dbOwner;
    }


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOwnerId(Integer ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Integer getOwnerId() 
    {
        return ownerId;
    }
    public void setAmt(BigDecimal amt) 
    {
        this.amt = amt;
    }

    public BigDecimal getAmt() 
    {
        return amt;
    }
    public void setTerm(Long term) 
    {
        this.term = term;
    }

    public Long getTerm() 
    {
        return term;
    }
    public void setFangTime(Date fangTime) 
    {
        this.fangTime = fangTime;
    }

    public Date getFangTime() 
    {
        return fangTime;
    }
    public void setDaoTime(Date daoTime) 
    {
        this.daoTime = daoTime;
    }

    public Date getDaoTime() 
    {
        return daoTime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setRate(BigDecimal rate) 
    {
        this.rate = rate;
    }

    public BigDecimal getRate() 
    {
        return rate;
    }
    public void setPurpose(String purpose) 
    {
        this.purpose = purpose;
    }

    public String getPurpose() 
    {
        return purpose;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerId", getOwnerId())
            .append("amt", getAmt())
            .append("term", getTerm())
            .append("fangTime", getFangTime())
            .append("daoTime", getDaoTime())
            .append("status", getStatus())
            .append("rate", getRate())
            .append("purpose", getPurpose())
            .toString();
    }
}
