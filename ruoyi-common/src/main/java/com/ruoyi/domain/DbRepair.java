package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.domain.StaffUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报修信息对象 db_Repair
 * 
 * @author ruoyi
 * @date 2023-03-21
 */
public class DbRepair extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 报修id */
    private Long id;

    /** 业主 */
    @Excel(name = "业主")
    private Long ownerId;

    private String ownerName;

    /** 报修类型 */
    @Excel(name = "报修类型")
    private Long type;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdtime;

    /** 报修内容 */
    @Excel(name = "报修内容")
    private String content;

    /** 报修内容 */
    @Excel(name = "报修地址")
    private String location;

    /** 报修项目 */
    @Excel(name = "报修项目")
    private String text;

    /** 具体描述 */
    @Excel(name = "具体描述")
    private String handle;

    /** 维修员 */
    @Excel(name = "维修员")
    private Long xId;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    private DbOwner dbOwner;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public DbOwner getDbOwner() {
        return dbOwner;
    }

    public void setDbOwner(DbOwner dbOwner) {
        this.dbOwner = dbOwner;
    }

    private StaffUser staffUser;

    public StaffUser getStaffUser() {
        return staffUser;
    }

    public void setStaffUser(StaffUser staffUser) {
        this.staffUser = staffUser;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }


    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setCreatedtime(Date createdtime) 
    {
        this.createdtime = createdtime;
    }

    public Date getCreatedtime() 
    {
        return createdtime;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setText(String text) 
    {
        this.text = text;
    }

    public String getLocation() {
        return location;
    }

    public String getText()
    {
        return text;
    }
    public void setHandle(String handle) 
    {
        this.handle = handle;
    }

    public String getHandle() 
    {
        return handle;
    }
    public void setxId(Long xId) 
    {
        this.xId = xId;
    }

    public Long getxId() 
    {
        return xId;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerId", getOwnerId())
            .append("type", getType())
            .append("createdtime", getCreatedtime())
            .append("content", getContent())
            .append("location", getLocation())
            .append("text", getText())
            .append("handle", getHandle())
            .append("xId", getxId())
            .append("status", getStatus())
            .toString();
    }
}
