package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 临时车缴费对象 db_pay_parking
 * 
 * @author wlx
 * @date 2023-03-09
 */
public class DbPayParking extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 临时车收费标准id */
    @Excel(name = "临时车收费标准id")
    private Long temId;

    /** 临时车出入id */
    @Excel(name = "临时车出入id")
    private Long temporaryId;

    /** 缴费时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "缴费时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date creaettime;

    /**出入车辆登记*/
    private DbTemporary temporary;

    //临时车收费标准
    private DbTemvehicleStandard temvehicleStandard;

    public DbTemporary getTemporary() {
        return temporary;
    }

    public void setTemporary(DbTemporary temporary) {
        this.temporary = temporary;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public DbTemvehicleStandard getTemvehicleStandard() {
        return temvehicleStandard;
    }

    public void setTemvehicleStandard(DbTemvehicleStandard temvehicleStandard) {
        this.temvehicleStandard = temvehicleStandard;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTemId(Long temId) 
    {
        this.temId = temId;
    }

    public Long getTemId() 
    {
        return temId;
    }
    public void setTemporaryId(Long temporaryId) 
    {
        this.temporaryId = temporaryId;
    }

    public Long getTemporaryId() 
    {
        return temporaryId;
    }
    public void setCreaettime(Date creaettime) 
    {
        this.creaettime = creaettime;
    }

    public Date getCreaettime() 
    {
        return creaettime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("temId", getTemId())
            .append("temporaryId", getTemporaryId())
            .append("creaettime", getCreaettime())
            .toString();
    }
}
