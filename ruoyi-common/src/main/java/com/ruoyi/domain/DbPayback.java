package com.ruoyi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 还款记录对象 db_payback
 * 
 * @author ruoyi
 * @date 2023-03-24
 */
public class DbPayback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 业主 */
    @Excel(name = "业主")
    private Long ownerId;

    /** 卡号 */
    @Excel(name = "卡号")
    private String cardNum;

    /** 贷款合同 */
    @Excel(name = "贷款合同")
    private Long htId;

    /** 还款本金 */
    @Excel(name = "还款本金")
    private BigDecimal bj;

    /** 还款利息 */
    @Excel(name = "还款利息")
    private BigDecimal lx;

    /** 还款总额 */
    @Excel(name = "还款总额")
    private BigDecimal real;

    /** 最后还款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后还款日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhrq;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    private DbOwner dbOwner;

    public DbOwner getDbOwner() {
        return dbOwner;
    }

    public void setDbOwner(DbOwner dbOwner) {
        this.dbOwner = dbOwner;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setCardNum(String cardNum) 
    {
        this.cardNum = cardNum;
    }

    public String getCardNum() 
    {
        return cardNum;
    }
    public void setHtId(Long htId) 
    {
        this.htId = htId;
    }

    public Long getHtId() 
    {
        return htId;
    }
    public void setBj(BigDecimal bj) 
    {
        this.bj = bj;
    }

    public BigDecimal getBj() 
    {
        return bj;
    }
    public void setLx(BigDecimal lx) 
    {
        this.lx = lx;
    }

    public BigDecimal getLx() 
    {
        return lx;
    }
    public void setReal(BigDecimal real) 
    {
        this.real = real;
    }

    public BigDecimal getReal() 
    {
        return real;
    }
    public void setZhrq(Date zhrq) 
    {
        this.zhrq = zhrq;
    }

    public Date getZhrq() 
    {
        return zhrq;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerId", getOwnerId())
            .append("cardNum", getCardNum())
            .append("htId", getHtId())
            .append("bj", getBj())
            .append("lx", getLx())
            .append("real", getReal())
            .append("zhrq", getZhrq())
            .append("status", getStatus())
            .toString();
    }
}
