package com.ruoyi.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 账单催缴对象 db_billcall
 * 
 * @author wlx
 * @date 2023-03-14
 */
public class DbBillcall extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 账单编号 */
    private Long id;

    /** 收费标准id */
    @Excel(name = "收费标准id")
    private Long chargingId;

    /** 累计应收 */
    @Excel(name = "累计应收")
    private BigDecimal receivables;

    /** 实收金额 */
    @Excel(name = "实收金额")
    private BigDecimal money;

    /** 账单日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "账单日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date billtime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setChargingId(Long chargingId) 
    {
        this.chargingId = chargingId;
    }

    public Long getChargingId() 
    {
        return chargingId;
    }
    public void setReceivables(BigDecimal receivables) 
    {
        this.receivables = receivables;
    }

    public BigDecimal getReceivables() 
    {
        return receivables;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setBilltime(Date billtime) 
    {
        this.billtime = billtime;
    }

    public Date getBilltime() 
    {
        return billtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("chargingId", getChargingId())
            .append("receivables", getReceivables())
            .append("money", getMoney())
            .append("billtime", getBilltime())
            .toString();
    }
}
