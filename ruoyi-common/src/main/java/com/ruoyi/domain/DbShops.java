package com.ruoyi.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商铺信息对象 db_shops
 *
 * @author wlx
 * @date 2023-03-07
 */
public class DbShops extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商铺ID */
    @Excel(name = "商铺ID")
    private Integer id;

    /** 商铺编号 */
    @Excel(name = "商铺编号")
    private String number;

    /** 楼层 */
    @Excel(name = "楼层")
    private Integer storey;

    /** 建筑面积 */
    @Excel(name = "建筑面积")
    private String build;

    /** 室内面积 */
    @Excel(name = "室内面积")
    private String indoor;

    /** 租金 */
    @Excel(name = "租金")
    private BigDecimal rent;

    /** 商铺状态 */
    @Excel(name = "商铺状态")
    private Integer status;

    /** 商铺类型 */
    @Excel(name = "商铺类型")
    private Integer type;

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setNumber(String number)
    {
        this.number = number;
    }

    public String getNumber()
    {
        return number;
    }
    public void setStorey(Integer storey)
    {
        this.storey = storey;
    }

    public Integer getStorey()
    {
        return storey;
    }
    public void setBuild(String build)
    {
        this.build = build;
    }

    public String getBuild()
    {
        return build;
    }
    public void setIndoor(String indoor)
    {
        this.indoor = indoor;
    }

    public String getIndoor()
    {
        return indoor;
    }
    public void setRent(BigDecimal rent)
    {
        this.rent = rent;
    }

    public BigDecimal getRent()
    {
        return rent;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setType(Integer type)
    {
        this.type = type;
    }

    public Integer getType()
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("number", getNumber())
                .append("storey", getStorey())
                .append("build", getBuild())
                .append("indoor", getIndoor())
                .append("rent", getRent())
                .append("status", getStatus())
                .append("type", getType())
                .toString();
    }
}
