package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 巡检任务详情对象 db_taskdetails
 * 
 * @author ruoyi
 * @date 2023-03-25
 */
public class DbTaskdetails extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 任务详情ID */
    private Long id;

    /** 关联巡逻任务id */
    @Excel(name = "关联巡逻任务id")
    private Long patId;

    /** 关联巡检设备id */
    @Excel(name = "关联巡检设备id")
    private Long equId;

    /** 巡检状态 */
    @Excel(name = "巡检状态")
    private Long rstatus;

    /** 签到方式 */
    @Excel(name = "签到方式")
    private Long state;

    /** 巡检情况 */
    @Excel(name = "巡检情况")
    private String hstate;

    /** 巡检照片 */
    @Excel(name = "巡检照片")
    private String photo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPatId(Long patId) 
    {
        this.patId = patId;
    }

    public Long getPatId() 
    {
        return patId;
    }
    public void setEquId(Long equId) 
    {
        this.equId = equId;
    }

    public Long getEquId() 
    {
        return equId;
    }
    public void setRstatus(Long rstatus) 
    {
        this.rstatus = rstatus;
    }

    public Long getRstatus() 
    {
        return rstatus;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setHstate(String hstate) 
    {
        this.hstate = hstate;
    }

    public String getHstate() 
    {
        return hstate;
    }
    public void setPhoto(String photo) 
    {
        this.photo = photo;
    }

    public String getPhoto() 
    {
        return photo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("patId", getPatId())
            .append("equId", getEquId())
            .append("rstatus", getRstatus())
            .append("state", getState())
            .append("hstate", getHstate())
            .append("photo", getPhoto())
            .toString();
    }
}
