package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 停车场对象 db_parkinglot
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
public class DbParkinglot extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 停车场ID */
    private Long id;

    /** 停车场编号 */
    @Excel(name = "停车场编号")
    private Long tnumber;

    /** 请选择地上停车场或地下停车场 */
    @Excel(name = "请选择地上停车场或地下停车场")
    private String type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTnumber(Long tnumber) 
    {
        this.tnumber = tnumber;
    }

    public Long getTnumber() 
    {
        return tnumber;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tnumber", getTnumber())
            .append("type", getType())
            .append("remark", getRemark())
            .toString();
    }
}
