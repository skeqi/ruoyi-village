package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 信息发布对象 db_xxfb
 * 
 * @author 郑东来
 * @date 2023-03-21
 */
public class Xxfb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 信息编号 */
    @Excel(name = "信息编号")
    private Long xxfbId;

    /** 信息标题 */
    @Excel(name = "信息标题")
    private String xxfbTitle;

    /** 信息类型 */
    @Excel(name = "信息类型")
    private String xxfbType;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date xxfbBegin;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date xxfbEnd;

    /** 发布人 */
    @Excel(name = "发布人")
    private String xxfbFlag;

    /** 发布人 */
    @Excel(name = "信息详情")
    private String xxfbXianqin;

    public String getXxfbXianqin() {
        return xxfbXianqin;
    }

    public void setXxfbXianqin(String xxfbXianqin) {
        this.xxfbXianqin = xxfbXianqin;
    }

    public void setXxfbId(Long xxfbId)
    {
        this.xxfbId = xxfbId;
    }

    public Long getXxfbId() 
    {
        return xxfbId;
    }
    public void setXxfbTitle(String xxfbTitle) 
    {
        this.xxfbTitle = xxfbTitle;
    }

    public String getXxfbTitle() 
    {
        return xxfbTitle;
    }
    public void setXxfbType(String xxfbType) 
    {
        this.xxfbType = xxfbType;
    }

    public String getXxfbType() 
    {
        return xxfbType;
    }
    public void setXxfbBegin(Date xxfbBegin) 
    {
        this.xxfbBegin = xxfbBegin;
    }

    public Date getXxfbBegin() 
    {
        return xxfbBegin;
    }
    public void setXxfbEnd(Date xxfbEnd) 
    {
        this.xxfbEnd = xxfbEnd;
    }

    public Date getXxfbEnd() 
    {
        return xxfbEnd;
    }
    public void setXxfbFlag(String xxfbFlag) 
    {
        this.xxfbFlag = xxfbFlag;
    }

    public String getXxfbFlag() 
    {
        return xxfbFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("xxfbId", getXxfbId())
                .append("xxfbTitle", getXxfbTitle())
                .append("xxfbType", getXxfbType())
                .append("xxfbBegin", getXxfbBegin())
                .append("xxfbEnd", getXxfbEnd())
                .append("xxfbFlag", getXxfbFlag())
                .append("xxfbXianqin",getXxfbXianqin())
                .toString();
    }
}
