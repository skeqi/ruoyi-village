package com.ruoyi.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收费标准对象 db_charging
 * 
 * @author wlx
 * @date 2023-03-08
 */
public class DbCharging extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 收费标准id */
    @Excel(name = "收费标准id")
    private Long id;

    /** 费用类型 */
    @Excel(name = "费用类型")
    private Long exptype;

    /** 催缴类型 */
    @Excel(name = "催缴类型")
    private Long calltype;

    /** 缴费周期(单位:月) */
    @Excel(name = "缴费周期(单位:月)")
    private String payment;

    /** 计费单价(单位:元) */
    @Excel(name = "计费单价(单位:元)")
    private BigDecimal unitprice;

    /** 附加/固定费用 */
    @Excel(name = "附加/固定费用")
    private BigDecimal cost;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setExptype(Long exptype) 
    {
        this.exptype = exptype;
    }

    public Long getExptype() 
    {
        return exptype;
    }
    public void setCalltype(Long calltype) 
    {
        this.calltype = calltype;
    }

    public Long getCalltype() 
    {
        return calltype;
    }
    public void setPayment(String payment) 
    {
        this.payment = payment;
    }

    public String getPayment() 
    {
        return payment;
    }
    public void setUnitprice(BigDecimal unitprice) 
    {
        this.unitprice = unitprice;
    }

    public BigDecimal getUnitprice() 
    {
        return unitprice;
    }
    public void setCost(BigDecimal cost) 
    {
        this.cost = cost;
    }

    public BigDecimal getCost() 
    {
        return cost;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("exptype", getExptype())
            .append("calltype", getCalltype())
            .append("payment", getPayment())
            .append("unitprice", getUnitprice())
            .append("cost", getCost())
            .toString();
    }
}
