package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公告发布对象 db_notice_bulletin
 * 
 * @author koilty
 * @date 2023-03-07
 */
public class NoticeBulletin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公告编号 */
    @Excel(name = "公告编号")
    private Long bulletinId;

    /** 公告标题 */
    @Excel(name = "公告标题")
    private String bulletinTitle;

    /** 公告类型 */
    @Excel(name = "公告类型")
    private String bulletinType;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date bulletinBegin;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date bulletinEnd;

    /** 公告状态 */
    @Excel(name = "公告状态")
    private Long bulletinFlag;

    public void setBulletinId(Long bulletinId) 
    {
        this.bulletinId = bulletinId;
    }

    public Long getBulletinId() 
    {
        return bulletinId;
    }
    public void setBulletinTitle(String bulletinTitle) 
    {
        this.bulletinTitle = bulletinTitle;
    }

    public String getBulletinTitle() 
    {
        return bulletinTitle;
    }
    public void setBulletinType(String bulletinType) 
    {
        this.bulletinType = bulletinType;
    }

    public String getBulletinType() 
    {
        return bulletinType;
    }
    public void setBulletinBegin(Date bulletinBegin) 
    {
        this.bulletinBegin = bulletinBegin;
    }

    public Date getBulletinBegin() 
    {
        return bulletinBegin;
    }
    public void setBulletinEnd(Date bulletinEnd) 
    {
        this.bulletinEnd = bulletinEnd;
    }

    public Date getBulletinEnd() 
    {
        return bulletinEnd;
    }
    public void setBulletinFlag(Long bulletinFlag) 
    {
        this.bulletinFlag = bulletinFlag;
    }

    public Long getBulletinFlag() 
    {
        return bulletinFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("bulletinId", getBulletinId())
            .append("bulletinTitle", getBulletinTitle())
            .append("bulletinType", getBulletinType())
            .append("bulletinBegin", getBulletinBegin())
            .append("bulletinEnd", getBulletinEnd())
            .append("bulletinFlag", getBulletinFlag())
            .toString();
    }
}
