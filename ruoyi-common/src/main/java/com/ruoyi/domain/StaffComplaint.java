package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.domain.StaffUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 投诉管理对象 db_staff_complaint
 * 
 * @author 郑东来
 * @date 2023-03-24
 */
public class StaffComplaint extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 投诉编号 */
    private Long complaintId;

    /** 投诉类型 */
    @Excel(name = "投诉类型")
    private Long complaintType;

    /** 业主住址 */
    @Excel(name = "业主住址")
    private String complaintHouse;

    /** 业主 */
    @Excel(name = "业主")
    private String ownerId;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private Long complaintPhone;

    /** 状态 */
    @Excel(name = "状态")
    private Long complaintFlag;

    /** 业主住址 */
    @Excel(name = "投诉信息")
    private String complaintText;

    /** 处理员工 */
    @Excel(name = "处理员工")
    private Long complaintServices;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date creationtime;

    private DbOwner dbOwner;

    private StaffUser staffUser;

    public StaffUser getStaffUser() {
        return staffUser;
    }

    public void setStaffUser(StaffUser staffUser) {
        this.staffUser = staffUser;
    }

    public DbOwner getDbOwner() {
        return dbOwner;
    }

    public void setDbOwner(DbOwner dbOwner) {
        this.dbOwner = dbOwner;
    }

    public String getComplaintText() {
        return complaintText;
    }

    public void setComplaintText(String complaintText) {
        this.complaintText = complaintText;
    }

    public void setComplaintId(Long complaintId)
    {
        this.complaintId = complaintId;
    }

    public Long getComplaintId() 
    {
        return complaintId;
    }
    public void setComplaintType(Long complaintType) 
    {
        this.complaintType = complaintType;
    }

    public Long getComplaintType() 
    {
        return complaintType;
    }
    public void setComplaintHouse(String complaintHouse) 
    {
        this.complaintHouse = complaintHouse;
    }

    public String getComplaintHouse() {
        return complaintHouse;
    }

    public String getOwnerId()
    {
        return ownerId;
    }
    public void setOwnerId(String ownerId)
    {
        this.ownerId = ownerId;
    }

    public String getComplaintContacts() 
    {
        return ownerId;
    }
    public void setComplaintPhone(Long complaintPhone) 
    {
        this.complaintPhone = complaintPhone;
    }

    public Long getComplaintPhone() 
    {
        return complaintPhone;
    }
    public void setComplaintFlag(Long complaintFlag) 
    {
        this.complaintFlag = complaintFlag;
    }

    public Long getComplaintFlag() 
    {
        return complaintFlag;
    }
    public void setComplaintServices(Long complaintServices)
    {
        this.complaintServices = complaintServices;
    }

    public Long getComplaintServices()
    {
        return complaintServices;
    }
    public void setCreationtime(Date creationtime) 
    {
        this.creationtime = creationtime;
    }

    public Date getCreationtime() 
    {
        return creationtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("complaintId", getComplaintId())
            .append("complaintType", getComplaintType())
            .append("complaintHouse", getComplaintHouse())
            .append("ownerId", getOwnerId())
            .append("complaintContacts", getComplaintContacts())
            .append("complaintPhone", getComplaintPhone())
            .append("complaintFlag", getComplaintFlag())
            .append("complaintText", getComplaintText())
            .append("complaintServices", getComplaintServices())
            .append("creationtime", getCreationtime())
            .toString();
    }
}
