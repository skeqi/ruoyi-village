package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 疫苗接种状况对象 db_ymzk
 * 
 * @author 郑东来
 * @date 2023-03-17
 */
public class DbYmzk extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long ymzkId;

    /** 用户编号 */
    @Excel(name = "用户编号")
    private Long ymzkUserid;

    /** 用户姓名 */
    @Excel(name = "用户姓名")
    private String ymzkName;

    /** 第一针疫苗接种情况 */
    @Excel(name = "第一针疫苗接种情况")
    private String ymzkDiyizhenym;

    /** 第二针疫苗接种时间 */
    @Excel(name = "第二针疫苗接种时间")
    private String ymzkDiyizhensj;

    /** 第二针疫苗接种情况 */
    @Excel(name = "第二针疫苗接种情况")
    private String ymzkDierzhenym;

    /** 第二针疫苗接种时间 */
    @Excel(name = "第二针疫苗接种时间")
    private String ymzkDierzhensj;

    public void setYmzkId(Long ymzkId) 
    {
        this.ymzkId = ymzkId;
    }

    public Long getYmzkId() 
    {
        return ymzkId;
    }
    public void setYmzkUserid(Long ymzkUserid) 
    {
        this.ymzkUserid = ymzkUserid;
    }

    public Long getYmzkUserid() 
    {
        return ymzkUserid;
    }
    public void setYmzkName(String ymzkName) 
    {
        this.ymzkName = ymzkName;
    }

    public String getYmzkName() 
    {
        return ymzkName;
    }
    public void setYmzkDiyizhenym(String ymzkDiyizhenym) 
    {
        this.ymzkDiyizhenym = ymzkDiyizhenym;
    }

    public String getYmzkDiyizhenym() 
    {
        return ymzkDiyizhenym;
    }
    public void setYmzkDiyizhensj(String ymzkDiyizhensj) 
    {
        this.ymzkDiyizhensj = ymzkDiyizhensj;
    }

    public String getYmzkDiyizhensj() 
    {
        return ymzkDiyizhensj;
    }
    public void setYmzkDierzhenym(String ymzkDierzhenym) 
    {
        this.ymzkDierzhenym = ymzkDierzhenym;
    }

    public String getYmzkDierzhenym() 
    {
        return ymzkDierzhenym;
    }
    public void setYmzkDierzhensj(String ymzkDierzhensj) 
    {
        this.ymzkDierzhensj = ymzkDierzhensj;
    }

    public String getYmzkDierzhensj() 
    {
        return ymzkDierzhensj;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ymzkId", getYmzkId())
            .append("ymzkUserid", getYmzkUserid())
            .append("ymzkName", getYmzkName())
            .append("ymzkDiyizhenym", getYmzkDiyizhenym())
            .append("ymzkDiyizhensj", getYmzkDiyizhensj())
            .append("ymzkDierzhenym", getYmzkDierzhenym())
            .append("ymzkDierzhensj", getYmzkDierzhensj())
            .toString();
    }
}
