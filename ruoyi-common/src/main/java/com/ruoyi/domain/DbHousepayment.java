package com.ruoyi.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.domain.DbHouse;
import com.ruoyi.domain.DbOwner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 房屋缴费对象 db_housepayment
 * 
 * @author ruoyi
 * @date 2023-03-08
 */
@Data
@ApiModel
public class DbHousepayment extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    //    收费标准对象
    private DbCharging charging;

    //业主对象
    private DbOwner Owner;

    //房屋对象
    private DbHouse house;

    private String typeName;
    @ApiModelProperty("用量")
    /*用量*/
    private Double consumption;
    @ApiModelProperty("房屋费用id")
    /** 房屋费用id */
    private Long id;
    @ApiModelProperty("房屋id")
    /** 房屋id */
    @Excel(name = "房屋id")
    private Long houId;

    /** 业主id */
    @Excel(name = "业主id")
    private Long ownerId;

    /** 费用类型 */
    @Excel(name = "费用类型")
    private String type;

    /** 应收金额 */
    @Excel(name = "应收金额")
    private BigDecimal receivable;

    /** 建账时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "建账时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date accounta;

    /** 计费起始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计费起始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date origintime;

    /** 计费结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计费结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date finishtime;

    /** 说明 */
    @Excel(name = "说明")
    private String explaina;

    /** 状态 */
    @Excel(name = "状态")
    private Long statusa;

    /** 收费截止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收费截止日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endtime;




    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public DbCharging getCharging() {
        return charging;
    }

    public void setCharging(DbCharging charging) {
        this.charging = charging;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setHouId(Long houId) 
    {
        this.houId = houId;
    }

    public Long getHouId() 
    {
        return houId;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setAccounta(Date accounta) 
    {
        this.accounta = accounta;
    }

    public Date getAccounta() 
    {
        return accounta;
    }
    public void setOrigintime(Date origintime) 
    {
        this.origintime = origintime;
    }

    public Date getOrigintime() 
    {
        return origintime;
    }
    public void setFinishtime(Date finishtime) 
    {
        this.finishtime = finishtime;
    }

    public Date getFinishtime() 
    {
        return finishtime;
    }
    public void setExplaina(String explaina) 
    {
        this.explaina = explaina;
    }

    public String getExplaina() 
    {
        return explaina;
    }
    public void setStatusa(Long statusa) 
    {
        this.statusa = statusa;
    }

    public Long getStatusa() 
    {
        return statusa;
    }
    public void setEndtime(Date endtime) 
    {
        this.endtime = endtime;
    }

    public Date getEndtime() 
    {
        return endtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("houId", getHouId())
            .append("ownerId", getOwnerId())
            .append("type", getType())
            .append("receivable", getReceivable())
            .append("accounta", getAccounta())
            .append("origintime", getOrigintime())
            .append("finishtime", getFinishtime())
            .append("explaina", getExplaina())
            .append("statusa", getStatusa())
            .append("endtime", getEndtime())
            .toString();
    }
}
