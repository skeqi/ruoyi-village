package com.ruoyi.patrol.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.*;
import com.ruoyi.patrol.service.IDbTaskdetailsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 巡检任务详情Controller
 * 
 * @author ruoyi
 * @date 2023-03-25
 */
@RestController
@RequestMapping("/xjfs/xjfs")
public class DbTaskdetailsController extends BaseController
{
    @Autowired
    private IDbTaskdetailsService dbTaskdetailsService;

    /**
     * 查询巡检任务详情列表
     */
    @PreAuthorize("@ss.hasPermi('xjfs:xjfs:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbTaskdetails dbTaskdetails)
    {
        startPage();
        List<DbTaskdetails> list = dbTaskdetailsService.selectDbTaskdetailsList(dbTaskdetails);
        return getDataTable(list);
    }

    /**
     * 导出巡检任务详情列表
     */
    @PreAuthorize("@ss.hasPermi('xjfs:xjfs:export')")
    @Log(title = "巡检任务详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbTaskdetails dbTaskdetails)
    {
        List<DbTaskdetails> list = dbTaskdetailsService.selectDbTaskdetailsList(dbTaskdetails);
        ExcelUtil<DbTaskdetails> util = new ExcelUtil<DbTaskdetails>(DbTaskdetails.class);
        util.exportExcel(response, list, "巡检任务详情数据");
    }

    /**
     * 获取巡检任务详情详细信息
     */
    @PreAuthorize("@ss.hasPermi('xjfs:xjfs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbTaskdetailsService.selectDbTaskdetailsById(id));
    }

    /**
     * 新增巡检任务详情
     */
    @PreAuthorize("@ss.hasPermi('xjfs:xjfs:add')")
    @Log(title = "巡检任务详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbTaskdetails dbTaskdetails)
    {
        return toAjax(dbTaskdetailsService.insertDbTaskdetails(dbTaskdetails));
    }

    /**
     * 修改巡检任务详情
     */
    @PreAuthorize("@ss.hasPermi('xjfs:xjfs:edit')")
    @Log(title = "巡检任务详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbTaskdetails dbTaskdetails)
    {
        return toAjax(dbTaskdetailsService.updateDbTaskdetails(dbTaskdetails));
    }

    /**
     * 删除巡检任务详情
     */
    @PreAuthorize("@ss.hasPermi('xjfs:xjfs:remove')")
    @Log(title = "巡检任务详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbTaskdetailsService.deleteDbTaskdetailsByIds(ids));
    }
}
