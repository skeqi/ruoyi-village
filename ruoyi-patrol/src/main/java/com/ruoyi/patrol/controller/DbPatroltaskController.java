package com.ruoyi.patrol.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;

import com.ruoyi.patrol.service.IDbPatroltaskService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 巡检任务Controller
 * 
 * @author ruoyi
 * @date 2023-03-09
 */
@Api("设备管理-巡检任务")
@RestController
@RequestMapping("/patrol/task")
public class DbPatroltaskController extends BaseController
{
    @Autowired
    private IDbPatroltaskService dbPatroltaskService;

    /**
     * 查询巡检任务列表
     */
    @PreAuthorize("@ss.hasPermi('patrol:task:list')")
    @GetMapping("/list")
    @ApiOperation("巡检信息列表")
    public TableDataInfo list(DbPatroltask dbPatroltask)
    {
        startPage();
        List<DbPatroltask> list = dbPatroltaskService.selectDbPatroltaskList(dbPatroltask);
        return getDataTable(list);
    }

    @GetMapping("/repList")
    @ApiOperation("员工信息列表")
    public List<User> repairList(){
        return dbPatroltaskService.findStaffUser();
    }

    /**
     * 导出巡检任务列表
     */
    @PreAuthorize("@ss.hasPermi('patrol:task:export')")
    @Log(title = "巡检任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("巡检信息导出")
    public void export(HttpServletResponse response, DbPatroltask dbPatroltask)
    {
        List<DbPatroltask> list = dbPatroltaskService.selectDbPatroltaskList(dbPatroltask);
        ExcelUtil<DbPatroltask> util = new ExcelUtil<DbPatroltask>(DbPatroltask.class);
        util.exportExcel(response, list, "巡检任务数据");
    }

    /**
     * 获取巡检任务详细信息
     */
    @PreAuthorize("@ss.hasPermi('patrol:task:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("巡检信息详细")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbPatroltaskService.selectDbPatroltaskById(id));
    }

    /**
     * 新增巡检任务
     */
    @PreAuthorize("@ss.hasPermi('patrol:task:add')")
    @Log(title = "巡检任务", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("巡检信息新增")
    public AjaxResult add(@RequestBody DbPatroltask dbPatroltask)
    {
        return toAjax(dbPatroltaskService.insertDbPatroltask(dbPatroltask));
    }

    /**
     * 修改巡检任务
     */
    @PreAuthorize("@ss.hasPermi('patrol:task:edit')")
    @Log(title = "巡检任务", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("巡检信息修改")
    public AjaxResult edit(@RequestBody DbPatroltask dbPatroltask)
    {
        return toAjax(dbPatroltaskService.updateDbPatroltask(dbPatroltask));
    }

    /**
     * 删除巡检任务
     */
    @PreAuthorize("@ss.hasPermi('patrol:task:remove')")
    @Log(title = "巡检任务", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation("巡检信息删除")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbPatroltaskService.deleteDbPatroltaskByIds(ids));
    }
}
