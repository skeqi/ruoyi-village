package com.ruoyi.patrol.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.patrol.mapper.DbTaskdetailsMapper;
import com.ruoyi.domain.DbTaskdetails;
import com.ruoyi.patrol.service.IDbTaskdetailsService;

/**
 * 巡检任务详情Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-25
 */
@Service
public class DbTaskdetailsServiceImpl implements IDbTaskdetailsService 
{
    @Autowired
    private DbTaskdetailsMapper dbTaskdetailsMapper;

    /**
     * 查询巡检任务详情
     * 
     * @param id 巡检任务详情主键
     * @return 巡检任务详情
     */
    @Override
    public DbTaskdetails selectDbTaskdetailsById(Long id)
    {
        return dbTaskdetailsMapper.selectDbTaskdetailsById(id);
    }

    /**
     * 查询巡检任务详情列表
     * 
     * @param dbTaskdetails 巡检任务详情
     * @return 巡检任务详情
     */
    @Override
    public List<DbTaskdetails> selectDbTaskdetailsList(DbTaskdetails dbTaskdetails)
    {
        return dbTaskdetailsMapper.selectDbTaskdetailsList(dbTaskdetails);
    }

    /**
     * 新增巡检任务详情
     * 
     * @param dbTaskdetails 巡检任务详情
     * @return 结果
     */
    @Override
    public int insertDbTaskdetails(DbTaskdetails dbTaskdetails)
    {
        return dbTaskdetailsMapper.insertDbTaskdetails(dbTaskdetails);
    }

    /**
     * 修改巡检任务详情
     * 
     * @param dbTaskdetails 巡检任务详情
     * @return 结果
     */
    @Override
    public int updateDbTaskdetails(DbTaskdetails dbTaskdetails)
    {
        return dbTaskdetailsMapper.updateDbTaskdetails(dbTaskdetails);
    }

    /**
     * 批量删除巡检任务详情
     * 
     * @param ids 需要删除的巡检任务详情主键
     * @return 结果
     */
    @Override
    public int deleteDbTaskdetailsByIds(Long[] ids)
    {
        return dbTaskdetailsMapper.deleteDbTaskdetailsByIds(ids);
    }

    /**
     * 删除巡检任务详情信息
     * 
     * @param id 巡检任务详情主键
     * @return 结果
     */
    @Override
    public int deleteDbTaskdetailsById(Long id)
    {
        return dbTaskdetailsMapper.deleteDbTaskdetailsById(id);
    }
}
