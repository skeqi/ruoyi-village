package com.ruoyi.patrol.service;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 巡检任务详情Service接口
 * 
 * @author ruoyi
 * @date 2023-03-25
 */
public interface IDbTaskdetailsService 
{
    /**
     * 查询巡检任务详情
     * 
     * @param id 巡检任务详情主键
     * @return 巡检任务详情
     */
    public DbTaskdetails selectDbTaskdetailsById(Long id);

    /**
     * 查询巡检任务详情列表
     * 
     * @param dbTaskdetails 巡检任务详情
     * @return 巡检任务详情集合
     */
    public List<DbTaskdetails> selectDbTaskdetailsList(DbTaskdetails dbTaskdetails);

    /**
     * 新增巡检任务详情
     * 
     * @param dbTaskdetails 巡检任务详情
     * @return 结果
     */
    public int insertDbTaskdetails(DbTaskdetails dbTaskdetails);

    /**
     * 修改巡检任务详情
     * 
     * @param dbTaskdetails 巡检任务详情
     * @return 结果
     */
    public int updateDbTaskdetails(DbTaskdetails dbTaskdetails);

    /**
     * 批量删除巡检任务详情
     * 
     * @param ids 需要删除的巡检任务详情主键集合
     * @return 结果
     */
    public int deleteDbTaskdetailsByIds(Long[] ids);

    /**
     * 删除巡检任务详情信息
     * 
     * @param id 巡检任务详情主键
     * @return 结果
     */
    public int deleteDbTaskdetailsById(Long id);
}
