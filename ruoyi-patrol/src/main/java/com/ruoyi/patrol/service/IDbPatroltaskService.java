package com.ruoyi.patrol.service;

import java.util.List;

import com.ruoyi.domain.*;

/**
 * 巡检任务Service接口
 * 
 * @author ruoyi
 * @date 2023-03-09
 */
public interface IDbPatroltaskService 
{
    /**
     * 查询巡检任务
     * 
     * @param id 巡检任务主键
     * @return 巡检任务
     */
    public DbPatroltask selectDbPatroltaskById(Long id);

    /**
     * 查询巡检任务列表
     * 
     * @param dbPatroltask 巡检任务
     * @return 巡检任务集合
     */
    public List<DbPatroltask> selectDbPatroltaskList(DbPatroltask dbPatroltask);

    /**
     * 新增巡检任务
     * 
     * @param dbPatroltask 巡检任务
     * @return 结果
     */
    public int insertDbPatroltask(DbPatroltask dbPatroltask);

    /**
     * 修改巡检任务
     * 
     * @param dbPatroltask 巡检任务
     * @return 结果
     */
    public int updateDbPatroltask(DbPatroltask dbPatroltask);

    /**
     * 批量删除巡检任务
     * 
     * @param ids 需要删除的巡检任务主键集合
     * @return 结果
     */
    public int deleteDbPatroltaskByIds(Long[] ids);

    /**
     * 删除巡检任务信息
     * 
     * @param id 巡检任务主键
     * @return 结果
     */
    public int deleteDbPatroltaskById(Long id);

    public List<User> findStaffUser();
}
