package com.ruoyi.patrol.service.impl;

import java.util.List;

import com.ruoyi.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.patrol.mapper.DbPatroltaskMapper;
import com.ruoyi.patrol.service.IDbPatroltaskService;

/**
 * 巡检任务Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-09
 */
@Service
public class DbPatroltaskServiceImpl implements IDbPatroltaskService 
{
    @Autowired
    private DbPatroltaskMapper dbPatroltaskMapper;

    /**
     * 查询巡检任务
     * 
     * @param id 巡检任务主键
     * @return 巡检任务
     */
    @Override
    public DbPatroltask selectDbPatroltaskById(Long id)
    {
        return dbPatroltaskMapper.selectDbPatroltaskById(id);
    }

    /**
     * 查询巡检任务列表
     * 
     * @param dbPatroltask 巡检任务
     * @return 巡检任务
     */
    @Override
    public List<DbPatroltask> selectDbPatroltaskList(DbPatroltask dbPatroltask)
    {
        return dbPatroltaskMapper.selectDbPatroltaskList(dbPatroltask);
    }

    /**
     * 新增巡检任务
     * 
     * @param dbPatroltask 巡检任务
     * @return 结果
     */
    @Override
    public int insertDbPatroltask(DbPatroltask dbPatroltask)
    {
        return dbPatroltaskMapper.insertDbPatroltask(dbPatroltask);
    }

    /**
     * 修改巡检任务
     * 
     * @param dbPatroltask 巡检任务
     * @return 结果
     */
    @Override
    public int updateDbPatroltask(DbPatroltask dbPatroltask)
    {
        return dbPatroltaskMapper.updateDbPatroltask(dbPatroltask);
    }

    /**
     * 批量删除巡检任务
     * 
     * @param ids 需要删除的巡检任务主键
     * @return 结果
     */
    @Override
    public int deleteDbPatroltaskByIds(Long[] ids)
    {
        return dbPatroltaskMapper.deleteDbPatroltaskByIds(ids);
    }

    /**
     * 删除巡检任务信息
     * 
     * @param id 巡检任务主键
     * @return 结果
     */
    @Override
    public int deleteDbPatroltaskById(Long id)
    {
        return dbPatroltaskMapper.deleteDbPatroltaskById(id);
    }

    @Override
    public List<User> findStaffUser() {
        return dbPatroltaskMapper.findStaffUser();
    }
}
