package com.ruoyi.member.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbOwner;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbMember;
import com.ruoyi.member.service.IDbMemberService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主家庭成员Controller
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@RestController
@RequestMapping("/member/member")
public class DbMemberController extends BaseController
{
    @Autowired
    private IDbMemberService dbMemberService;

    /**
     * 查询业主家庭成员列表
     */
    @PreAuthorize("@ss.hasPermi('member:member:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbMember dbMember)
    {
        startPage();
        List<DbMember> list = dbMemberService.selectDbMemberList(dbMember);
        return getDataTable(list);
    }

    /**
     * 导出业主家庭成员列表
     */
    @PreAuthorize("@ss.hasPermi('member:member:export')")
    @Log(title = "业主家庭成员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbMember dbMember)
    {
        List<DbMember> list = dbMemberService.selectDbMemberList(dbMember);
        ExcelUtil<DbMember> util = new ExcelUtil<DbMember>(DbMember.class);
        util.exportExcel(response, list, "业主家庭成员数据");
    }

    /**
     * 获取业主家庭成员详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:member:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbMemberService.selectDbMemberById(id));
    }

    /**
     * 新增业主家庭成员
     */
    @PreAuthorize("@ss.hasPermi('member:member:add')")
    @Log(title = "业主家庭成员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbMember dbMember)
    {
        return toAjax(dbMemberService.insertDbMember(dbMember));
    }

    /**
     * 修改业主家庭成员
     */
    @PreAuthorize("@ss.hasPermi('member:member:edit')")
    @Log(title = "业主家庭成员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbMember dbMember)
    {
        return toAjax(dbMemberService.updateDbMember(dbMember));
    }

    /**
     * 删除业主家庭成员
     */
    @PreAuthorize("@ss.hasPermi('member:member:remove')")
    @Log(title = "业主家庭成员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbMemberService.deleteDbMemberByIds(ids));
    }

    @GetMapping("/finder")
    public List<DbOwner> findner(){return dbMemberService.findner();}
}
