package com.ruoyi.member.service;

import java.util.List;
import com.ruoyi.domain.*;

/**
 * 业主家庭成员Service接口
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
public interface IDbMemberService 
{
    /**
     * 查询业主家庭成员
     * 
     * @param id 业主家庭成员主键
     * @return 业主家庭成员
     */
    public DbMember selectDbMemberById(Long id);

    /**
     * 查询业主家庭成员列表
     * 
     * @param dbMember 业主家庭成员
     * @return 业主家庭成员集合
     */
    public List<DbMember> selectDbMemberList(DbMember dbMember);

    /**
     * 新增业主家庭成员
     * 
     * @param dbMember 业主家庭成员
     * @return 结果
     */
    public int insertDbMember(DbMember dbMember);

    /**
     * 修改业主家庭成员
     * 
     * @param dbMember 业主家庭成员
     * @return 结果
     */
    public int updateDbMember(DbMember dbMember);

    /**
     * 批量删除业主家庭成员
     * 
     * @param ids 需要删除的业主家庭成员主键集合
     * @return 结果
     */
    public int deleteDbMemberByIds(Long[] ids);

    /**
     * 删除业主家庭成员信息
     * 
     * @param id 业主家庭成员主键
     * @return 结果
     */
    public int deleteDbMemberById(Long id);

    /*
    绑定业主信息
     */
    public List<DbOwner> findner();
}
