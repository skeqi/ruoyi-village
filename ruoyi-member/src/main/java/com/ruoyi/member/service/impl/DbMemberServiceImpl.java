package com.ruoyi.member.service.impl;

import java.util.List;

import com.ruoyi.domain.DbOwner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.member.mapper.DbMemberMapper;
import com.ruoyi.domain.DbMember;
import com.ruoyi.member.service.IDbMemberService;

/**
 * 业主家庭成员Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class DbMemberServiceImpl implements IDbMemberService 
{
    @Autowired
    private DbMemberMapper dbMemberMapper;

    /**
     * 查询业主家庭成员
     * 
     * @param id 业主家庭成员主键
     * @return 业主家庭成员
     */
    @Override
    public DbMember selectDbMemberById(Long id)
    {
        return dbMemberMapper.selectDbMemberById(id);
    }

    /**
     * 查询业主家庭成员列表
     * 
     * @param dbMember 业主家庭成员
     * @return 业主家庭成员
     */
    @Override
    public List<DbMember> selectDbMemberList(DbMember dbMember)
    {
        return dbMemberMapper.selectDbMemberList(dbMember);
    }

    /**
     * 新增业主家庭成员
     * 
     * @param dbMember 业主家庭成员
     * @return 结果
     */
    @Override
    public int insertDbMember(DbMember dbMember)
    {
        return dbMemberMapper.insertDbMember(dbMember);
    }

    /**
     * 修改业主家庭成员
     * 
     * @param dbMember 业主家庭成员
     * @return 结果
     */
    @Override
    public int updateDbMember(DbMember dbMember)
    {
        return dbMemberMapper.updateDbMember(dbMember);
    }

    /**
     * 批量删除业主家庭成员
     * 
     * @param ids 需要删除的业主家庭成员主键
     * @return 结果
     */
    @Override
    public int deleteDbMemberByIds(Long[] ids)
    {
        return dbMemberMapper.deleteDbMemberByIds(ids);
    }

    /**
     * 删除业主家庭成员信息
     * 
     * @param id 业主家庭成员主键
     * @return 结果
     */
    @Override
    public int deleteDbMemberById(Long id)
    {
        return dbMemberMapper.deleteDbMemberById(id);
    }

    @Override
    public List<DbOwner> findner() {
        return dbMemberMapper.findner();
    }
}
