package com.ruoyi.ygkq.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbStaffYgkq;
import com.ruoyi.ygkq.service.IDbStaffYgkqService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 员工考勤Controller
 * 
 * @author 郑东来
 * @date 2023-03-22
 */
@RestController
@RequestMapping("/ygkq/ygkq")
public class DbStaffYgkqController extends BaseController
{
    @Autowired
    private IDbStaffYgkqService dbStaffYgkqService;

    /**
     * 查询员工考勤列表
     */
    @PreAuthorize("@ss.hasPermi('ygkq:ygkq:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbStaffYgkq dbStaffYgkq)
    {
        startPage();
        List<DbStaffYgkq> list = dbStaffYgkqService.selectDbStaffYgkqList(dbStaffYgkq);
        return getDataTable(list);
    }

    /**
     * 导出员工考勤列表
     */
    @PreAuthorize("@ss.hasPermi('ygkq:ygkq:export')")
    @Log(title = "员工考勤", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbStaffYgkq dbStaffYgkq)
    {
        List<DbStaffYgkq> list = dbStaffYgkqService.selectDbStaffYgkqList(dbStaffYgkq);
        ExcelUtil<DbStaffYgkq> util = new ExcelUtil<DbStaffYgkq>(DbStaffYgkq.class);
        util.exportExcel(response, list, "员工考勤数据");
    }

    /**
     * 获取员工考勤详细信息
     */
    @PreAuthorize("@ss.hasPermi('ygkq:ygkq:query')")
    @GetMapping(value = "/{staffId}")
    public AjaxResult getInfo(@PathVariable("staffId") Long staffId)
    {
        return success(dbStaffYgkqService.selectDbStaffYgkqByStaffId(staffId));
    }

    /**
     * 新增员工考勤
     */
    @PreAuthorize("@ss.hasPermi('ygkq:ygkq:add')")
    @Log(title = "员工考勤", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbStaffYgkq dbStaffYgkq)
    {
        return toAjax(dbStaffYgkqService.insertDbStaffYgkq(dbStaffYgkq));
    }

    /**
     * 修改员工考勤
     */
    @PreAuthorize("@ss.hasPermi('ygkq:ygkq:edit')")
    @Log(title = "员工考勤", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbStaffYgkq dbStaffYgkq)
    {
        return toAjax(dbStaffYgkqService.updateDbStaffYgkq(dbStaffYgkq));
    }

    /**
     * 删除员工考勤
     */
    @PreAuthorize("@ss.hasPermi('ygkq:ygkq:remove')")
    @Log(title = "员工考勤", businessType = BusinessType.DELETE)
	@DeleteMapping("/{staffIds}")
    public AjaxResult remove(@PathVariable Long[] staffIds)
    {
        return toAjax(dbStaffYgkqService.deleteDbStaffYgkqByStaffIds(staffIds));
    }
}
