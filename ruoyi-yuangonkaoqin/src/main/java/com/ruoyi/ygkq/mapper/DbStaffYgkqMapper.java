package com.ruoyi.ygkq.mapper;

import java.util.List;
import com.ruoyi.domain.DbStaffYgkq;

/**
 * 员工考勤Mapper接口
 * 
 * @author 郑东来
 * @date 2023-03-22
 */
public interface DbStaffYgkqMapper 
{
    /**
     * 查询员工考勤
     * 
     * @param staffId 员工考勤主键
     * @return 员工考勤
     */
    public DbStaffYgkq selectDbStaffYgkqByStaffId(Long staffId);

    /**
     * 查询员工考勤列表
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 员工考勤集合
     */
    public List<DbStaffYgkq> selectDbStaffYgkqList(DbStaffYgkq dbStaffYgkq);

    /**
     * 新增员工考勤
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 结果
     */
    public int insertDbStaffYgkq(DbStaffYgkq dbStaffYgkq);

    /**
     * 修改员工考勤
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 结果
     */
    public int updateDbStaffYgkq(DbStaffYgkq dbStaffYgkq);

    /**
     * 删除员工考勤
     * 
     * @param staffId 员工考勤主键
     * @return 结果
     */
    public int deleteDbStaffYgkqByStaffId(Long staffId);

    /**
     * 批量删除员工考勤
     * 
     * @param staffIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbStaffYgkqByStaffIds(Long[] staffIds);
}
