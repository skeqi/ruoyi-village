package com.ruoyi.ygkq.service;

import java.util.List;
import com.ruoyi.domain.DbStaffYgkq;

/**
 * 员工考勤Service接口
 * 
 * @author 郑东来
 * @date 2023-03-22
 */
public interface IDbStaffYgkqService 
{
    /**
     * 查询员工考勤
     * 
     * @param staffId 员工考勤主键
     * @return 员工考勤
     */
    public DbStaffYgkq selectDbStaffYgkqByStaffId(Long staffId);

    /**
     * 查询员工考勤列表
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 员工考勤集合
     */
    public List<DbStaffYgkq> selectDbStaffYgkqList(DbStaffYgkq dbStaffYgkq);

    /**
     * 新增员工考勤
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 结果
     */
    public int insertDbStaffYgkq(DbStaffYgkq dbStaffYgkq);

    /**
     * 修改员工考勤
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 结果
     */
    public int updateDbStaffYgkq(DbStaffYgkq dbStaffYgkq);

    /**
     * 批量删除员工考勤
     * 
     * @param staffIds 需要删除的员工考勤主键集合
     * @return 结果
     */
    public int deleteDbStaffYgkqByStaffIds(Long[] staffIds);

    /**
     * 删除员工考勤信息
     * 
     * @param staffId 员工考勤主键
     * @return 结果
     */
    public int deleteDbStaffYgkqByStaffId(Long staffId);
}
