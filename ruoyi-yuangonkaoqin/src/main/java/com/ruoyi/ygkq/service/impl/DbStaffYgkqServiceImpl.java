package com.ruoyi.ygkq.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ygkq.mapper.DbStaffYgkqMapper;
import com.ruoyi.domain.DbStaffYgkq;
import com.ruoyi.ygkq.service.IDbStaffYgkqService;

/**
 * 员工考勤Service业务层处理
 * 
 * @author 郑东来
 * @date 2023-03-22
 */
@Service
public class DbStaffYgkqServiceImpl implements IDbStaffYgkqService 
{
    @Autowired
    private DbStaffYgkqMapper dbStaffYgkqMapper;

    /**
     * 查询员工考勤
     * 
     * @param staffId 员工考勤主键
     * @return 员工考勤
     */
    @Override
    public DbStaffYgkq selectDbStaffYgkqByStaffId(Long staffId)
    {
        return dbStaffYgkqMapper.selectDbStaffYgkqByStaffId(staffId);
    }

    /**
     * 查询员工考勤列表
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 员工考勤
     */
    @Override
    public List<DbStaffYgkq> selectDbStaffYgkqList(DbStaffYgkq dbStaffYgkq)
    {
        return dbStaffYgkqMapper.selectDbStaffYgkqList(dbStaffYgkq);
    }

    /**
     * 新增员工考勤
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 结果
     */
    @Override
    public int insertDbStaffYgkq(DbStaffYgkq dbStaffYgkq)
    {
        return dbStaffYgkqMapper.insertDbStaffYgkq(dbStaffYgkq);
    }

    /**
     * 修改员工考勤
     * 
     * @param dbStaffYgkq 员工考勤
     * @return 结果
     */
    @Override
    public int updateDbStaffYgkq(DbStaffYgkq dbStaffYgkq)
    {
        return dbStaffYgkqMapper.updateDbStaffYgkq(dbStaffYgkq);
    }

    /**
     * 批量删除员工考勤
     * 
     * @param staffIds 需要删除的员工考勤主键
     * @return 结果
     */
    @Override
    public int deleteDbStaffYgkqByStaffIds(Long[] staffIds)
    {
        return dbStaffYgkqMapper.deleteDbStaffYgkqByStaffIds(staffIds);
    }

    /**
     * 删除员工考勤信息
     * 
     * @param staffId 员工考勤主键
     * @return 结果
     */
    @Override
    public int deleteDbStaffYgkqByStaffId(Long staffId)
    {
        return dbStaffYgkqMapper.deleteDbStaffYgkqByStaffId(staffId);
    }
}
