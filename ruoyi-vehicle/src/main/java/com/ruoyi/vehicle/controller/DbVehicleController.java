package com.ruoyi.vehicle.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.DbOwner;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.*;import com.ruoyi.vehicle.service.IDbVehicleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车辆管理Controller
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@RestController
@RequestMapping("/vehicle/vehicle")
public class DbVehicleController extends BaseController
{
    @Autowired
    private IDbVehicleService dbVehicleService;

    /**
     * 查询车辆管理列表
     */
    @PreAuthorize("@ss.hasPermi('vehicle:vehicle:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbVehicle dbVehicle)
    {
        startPage();
        List<DbVehicle> list = dbVehicleService.selectDbVehicleList(dbVehicle);
        return getDataTable(list);
    }

    /**
     * 导出车辆管理列表
     */
    @PreAuthorize("@ss.hasPermi('vehicle:vehicle:export')")
    @Log(title = "车辆管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbVehicle dbVehicle)
    {
        List<DbVehicle> list = dbVehicleService.selectDbVehicleList(dbVehicle);
        ExcelUtil<DbVehicle> util = new ExcelUtil<DbVehicle>(DbVehicle.class);
        util.exportExcel(response, list, "车辆管理数据");
    }

    /**
     * 获取车辆管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('vehicle:vehicle:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dbVehicleService.selectDbVehicleById(id));
    }

    /**
     * 新增车辆管理
     */
    @PreAuthorize("@ss.hasPermi('vehicle:vehicle:add')")
    @Log(title = "车辆管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbVehicle dbVehicle)
    {
        return toAjax(dbVehicleService.insertDbVehicle(dbVehicle));
    }

    /**
     * 修改车辆管理
     */
    @PreAuthorize("@ss.hasPermi('vehicle:vehicle:edit')")
    @Log(title = "车辆管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbVehicle dbVehicle)
    {
        return toAjax(dbVehicleService.updateDbVehicle(dbVehicle));
    }

    /**
     * 删除车辆管理
     */
    @PreAuthorize("@ss.hasPermi('vehicle:vehicle:remove')")
    @Log(title = "车辆管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dbVehicleService.deleteDbVehicleByIds(ids));
    }

    /*
    业主姓名
     */
    @GetMapping("/findes")
    public List<DbOwner> findes(){return dbVehicleService.findeer();};
}
