package com.ruoyi.vehicle.mapper;

import java.util.List;
import com.ruoyi.domain.*;
/**
 * 车辆管理Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
public interface DbVehicleMapper 
{
    /**
     * 查询车辆管理
     * 
     * @param id 车辆管理主键
     * @return 车辆管理
     */
    public DbVehicle selectDbVehicleById(Long id);

    /**
     * 查询车辆管理列表
     * 
     * @param dbVehicle 车辆管理
     * @return 车辆管理集合
     */
    public List<DbVehicle> selectDbVehicleList(DbVehicle dbVehicle);

    /**
     * 新增车辆管理
     * 
     * @param dbVehicle 车辆管理
     * @return 结果
     */
    public int insertDbVehicle(DbVehicle dbVehicle);

    /**
     * 修改车辆管理
     * 
     * @param dbVehicle 车辆管理
     * @return 结果
     */
    public int updateDbVehicle(DbVehicle dbVehicle);

    /**
     * 删除车辆管理
     * 
     * @param id 车辆管理主键
     * @return 结果
     */
    public int deleteDbVehicleById(Long id);

    /**
     * 批量删除车辆管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbVehicleByIds(Long[] ids);

    /*
    车辆业主姓名
     */
    public List<DbOwner> findeer();
}
