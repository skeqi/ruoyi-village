package com.ruoyi.vehicle.service.impl;

import java.util.List;

import com.ruoyi.domain.DbOwner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.vehicle.mapper.DbVehicleMapper;
import com.ruoyi.domain.*;import com.ruoyi.vehicle.service.IDbVehicleService;

/**
 * 车辆管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-20
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class DbVehicleServiceImpl implements IDbVehicleService 
{
    @Autowired
    private DbVehicleMapper dbVehicleMapper;

    /**
     * 查询车辆管理
     * 
     * @param id 车辆管理主键
     * @return 车辆管理
     */
    @Override
    public DbVehicle selectDbVehicleById(Long id)
    {
        return dbVehicleMapper.selectDbVehicleById(id);
    }

    /**
     * 查询车辆管理列表
     * 
     * @param dbVehicle 车辆管理
     * @return 车辆管理
     */
    @Override
    public List<DbVehicle> selectDbVehicleList(DbVehicle dbVehicle)
    {
        return dbVehicleMapper.selectDbVehicleList(dbVehicle);
    }

    /**
     * 新增车辆管理
     * 
     * @param dbVehicle 车辆管理
     * @return 结果
     */
    @Override
    public int insertDbVehicle(DbVehicle dbVehicle)
    {
        return dbVehicleMapper.insertDbVehicle(dbVehicle);
    }

    /**
     * 修改车辆管理
     * 
     * @param dbVehicle 车辆管理
     * @return 结果
     */
    @Override
    public int updateDbVehicle(DbVehicle dbVehicle)
    {
        return dbVehicleMapper.updateDbVehicle(dbVehicle);
    }

    /**
     * 批量删除车辆管理
     * 
     * @param ids 需要删除的车辆管理主键
     * @return 结果
     */
    @Override
    public int deleteDbVehicleByIds(Long[] ids)
    {
        return dbVehicleMapper.deleteDbVehicleByIds(ids);
    }

    /**
     * 删除车辆管理信息
     * 
     * @param id 车辆管理主键
     * @return 结果
     */
    @Override
    public int deleteDbVehicleById(Long id)
    {
        return dbVehicleMapper.deleteDbVehicleById(id);
    }

    @Override
    public List<DbOwner> findeer() {
        return dbVehicleMapper.findeer();
    }
}
