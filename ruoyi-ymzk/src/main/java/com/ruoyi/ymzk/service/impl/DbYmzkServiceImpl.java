package com.ruoyi.ymzk.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ymzk.mapper.DbYmzkMapper;
import com.ruoyi.domain.DbYmzk;
import com.ruoyi.ymzk.service.IDbYmzkService;

/**
 * 疫苗接种状况Service业务层处理
 * 
 * @author 郑东来
 * @date 2023-03-17
 */
@Service
public class DbYmzkServiceImpl implements IDbYmzkService 
{
    @Autowired
    private DbYmzkMapper dbYmzkMapper;

    /**
     * 查询疫苗接种状况
     * 
     * @param ymzkId 疫苗接种状况主键
     * @return 疫苗接种状况
     */
    @Override
    public DbYmzk selectDbYmzkByYmzkId(Long ymzkId)
    {
        return dbYmzkMapper.selectDbYmzkByYmzkId(ymzkId);
    }

    /**
     * 查询疫苗接种状况列表
     * 
     * @param dbYmzk 疫苗接种状况
     * @return 疫苗接种状况
     */
    @Override
    public List<DbYmzk> selectDbYmzkList(DbYmzk dbYmzk)
    {
        return dbYmzkMapper.selectDbYmzkList(dbYmzk);
    }

    /**
     * 新增疫苗接种状况
     * 
     * @param dbYmzk 疫苗接种状况
     * @return 结果
     */
    @Override
    public int insertDbYmzk(DbYmzk dbYmzk)
    {
        return dbYmzkMapper.insertDbYmzk(dbYmzk);
    }

    /**
     * 修改疫苗接种状况
     * 
     * @param dbYmzk 疫苗接种状况
     * @return 结果
     */
    @Override
    public int updateDbYmzk(DbYmzk dbYmzk)
    {
        return dbYmzkMapper.updateDbYmzk(dbYmzk);
    }

    /**
     * 批量删除疫苗接种状况
     * 
     * @param ymzkIds 需要删除的疫苗接种状况主键
     * @return 结果
     */
    @Override
    public int deleteDbYmzkByYmzkIds(Long[] ymzkIds)
    {
        return dbYmzkMapper.deleteDbYmzkByYmzkIds(ymzkIds);
    }

    /**
     * 删除疫苗接种状况信息
     * 
     * @param ymzkId 疫苗接种状况主键
     * @return 结果
     */
    @Override
    public int deleteDbYmzkByYmzkId(Long ymzkId)
    {
        return dbYmzkMapper.deleteDbYmzkByYmzkId(ymzkId);
    }
}
