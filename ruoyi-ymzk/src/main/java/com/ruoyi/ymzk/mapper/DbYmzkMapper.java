package com.ruoyi.ymzk.mapper;

import java.util.List;
import com.ruoyi.domain.DbYmzk;

/**
 * 疫苗接种状况Mapper接口
 * 
 * @author 郑东来
 * @date 2023-03-17
 */
public interface DbYmzkMapper 
{
    /**
     * 查询疫苗接种状况
     * 
     * @param ymzkId 疫苗接种状况主键
     * @return 疫苗接种状况
     */
    public DbYmzk selectDbYmzkByYmzkId(Long ymzkId);

    /**
     * 查询疫苗接种状况列表
     * 
     * @param dbYmzk 疫苗接种状况
     * @return 疫苗接种状况集合
     */
    public List<DbYmzk> selectDbYmzkList(DbYmzk dbYmzk);

    /**
     * 新增疫苗接种状况
     * 
     * @param dbYmzk 疫苗接种状况
     * @return 结果
     */
    public int insertDbYmzk(DbYmzk dbYmzk);

    /**
     * 修改疫苗接种状况
     * 
     * @param dbYmzk 疫苗接种状况
     * @return 结果
     */
    public int updateDbYmzk(DbYmzk dbYmzk);

    /**
     * 删除疫苗接种状况
     * 
     * @param ymzkId 疫苗接种状况主键
     * @return 结果
     */
    public int deleteDbYmzkByYmzkId(Long ymzkId);

    /**
     * 批量删除疫苗接种状况
     * 
     * @param ymzkIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDbYmzkByYmzkIds(Long[] ymzkIds);
}
