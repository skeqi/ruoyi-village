package com.ruoyi.ymzk.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.domain.DbYmzk;
import com.ruoyi.ymzk.service.IDbYmzkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 疫苗接种状况Controller
 * 
 * @author 郑东来
 * @date 2023-03-17
 */
@RestController
@RequestMapping("/ymzk/ymzk")
public class DbYmzkController extends BaseController
{
    @Autowired
    private IDbYmzkService dbYmzkService;

    /**
     * 查询疫苗接种状况列表
     */
    @PreAuthorize("@ss.hasPermi('ymzk:ymzk:list')")
    @GetMapping("/list")
    public TableDataInfo list(DbYmzk dbYmzk)
    {
        startPage();
        List<DbYmzk> list = dbYmzkService.selectDbYmzkList(dbYmzk);
        return getDataTable(list);
    }

    /**
     * 导出疫苗接种状况列表
     */
    @PreAuthorize("@ss.hasPermi('ymzk:ymzk:export')")
    @Log(title = "疫苗接种状况", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DbYmzk dbYmzk)
    {
        List<DbYmzk> list = dbYmzkService.selectDbYmzkList(dbYmzk);
        ExcelUtil<DbYmzk> util = new ExcelUtil<DbYmzk>(DbYmzk.class);
        util.exportExcel(response, list, "疫苗接种状况数据");
    }

    /**
     * 获取疫苗接种状况详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymzk:ymzk:query')")
    @GetMapping(value = "/{ymzkId}")
    public AjaxResult getInfo(@PathVariable("ymzkId") Long ymzkId)
    {
        return success(dbYmzkService.selectDbYmzkByYmzkId(ymzkId));
    }

    /**
     * 新增疫苗接种状况
     */
    @PreAuthorize("@ss.hasPermi('ymzk:ymzk:add')")
    @Log(title = "疫苗接种状况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DbYmzk dbYmzk)
    {
        return toAjax(dbYmzkService.insertDbYmzk(dbYmzk));
    }

    /**
     * 修改疫苗接种状况
     */
    @PreAuthorize("@ss.hasPermi('ymzk:ymzk:edit')")
    @Log(title = "疫苗接种状况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DbYmzk dbYmzk)
    {
        return toAjax(dbYmzkService.updateDbYmzk(dbYmzk));
    }

    /**
     * 删除疫苗接种状况
     */
    @PreAuthorize("@ss.hasPermi('ymzk:ymzk:remove')")
    @Log(title = "疫苗接种状况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ymzkIds}")
    public AjaxResult remove(@PathVariable Long[] ymzkIds)
    {
        return toAjax(dbYmzkService.deleteDbYmzkByYmzkIds(ymzkIds));
    }
}
